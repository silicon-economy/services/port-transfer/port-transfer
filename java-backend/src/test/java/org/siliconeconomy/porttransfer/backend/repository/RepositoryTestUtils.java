// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RepositoryTestUtils {

    /**
     * The given repository-mocks will be stubbed so that any call to
     * {@link BaseRepository#save(Object) save(BaseEntity)} returns the passed entity
     *
     * @param baseRepositoryMocks mocks of BaseRepository or it's subclasses
     */
    public static void returnEntityOnSave(BaseRepository<?>... baseRepositoryMocks) {
        for (BaseRepository<?> baseRepository : baseRepositoryMocks) {
            when(baseRepository.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
        }
    }

}
