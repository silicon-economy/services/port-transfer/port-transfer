// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service.container;

import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.repository.RepositoryTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.master.ContainerRepository;

@ExtendWith(MockitoExtension.class)
@UnitTest
class ContainerServiceTest {

    @Mock
    ContainerRepository containerRepository;

    /* Class under test */
    ContainerService containerService;

    /* Helper constants */
    String bic = "BICU 123456 7";
    ContainerType type = ContainerType.ISO;
    ContainerLength length = ContainerLength.ISO_LENGTH_2;
    int weight = 1;
    Container example = new Container(bic, type, length, weight);
    static final String INCOMPLETE_INPUTS = """
            Type, Length      , TaraWeight
            ##############################
            ISO ,             ,
                , ISO_LENGTH_2,
                ,             , 7
                , ISO_LENGTH_2, 7
            ISO ,             , 7
            ISO , ISO_LENGTH_2,
            """;

    @BeforeEach
    void beforeEach() {
        containerService = new ContainerService(containerRepository);
    }

    @Test
    void throwsIf_AllInputsAreNull() {
        // Assert
        assertThatThrownBy(() -> containerService.findOrCreateContainer(null, null, null, null))
                .isInstanceOf(IncompleteContainerRequestException.class);
    }

    @ParameterizedTest
    @CsvSource(useHeadersInDisplayName = true, textBlock = INCOMPLETE_INPUTS)
    void throwsIf_InputsAreIncomplete_BicIsNull(ContainerType typeOrNull,
                                                ContainerLength lengthOrNull,
                                                Integer weightOrNull) {
        // Assert
        assertThatThrownBy(() -> containerService.findOrCreateContainer(null,
                                                                        typeOrNull,
                                                                        lengthOrNull,
                                                                        weightOrNull))
                .isInstanceOf(IncompleteContainerRequestException.class);
    }

    @ParameterizedTest
    @CsvSource(useHeadersInDisplayName = true, textBlock = INCOMPLETE_INPUTS)
    void throwsIf_InputsAreIncomplete_BicIsPresent(ContainerType typeOrNull,
                                                   ContainerLength lengthOrNull,
                                                   Integer weightOrNull) {
        // Assert
        /* single value */
        assertThatThrownBy(() -> containerService.findOrCreateContainer(bic,
                                                                        typeOrNull,
                                                                        lengthOrNull,
                                                                        weightOrNull))
                .isInstanceOf(IncompleteContainerRequestException.class);
    }

    @Test
    void throwsIf_NoContainerWithBic_NoInformationForNewContainer() {
        // Assert
        assertThatThrownBy(() -> containerService.findOrCreateContainer(bic, null, null, null))
                .isInstanceOf(NoContainerException.class);
    }

    @Test
    void findsExistingContainerByBicOnly() throws ContainerException {
        // Arrange
        when(containerRepository.findByBicCode(bic)).thenReturn(Optional.of(example));
        // Act
        var container = containerService.findOrCreateContainer(bic, null, null, null);
        // Assert
        assertThat(container)
                .isEqualTo(example);
        verify(containerRepository, never()).save(any());
    }

    @Test
    void createsContainerWithoutBic() throws ContainerException {
        // Arrange
        RepositoryTestUtils.returnEntityOnSave(containerRepository);
        // Act
        var container = containerService.findOrCreateContainer(null, type, length, weight);
        // Assert
        assertThat(container)
                .returns(null, Container::getBicCode)
                .returns(type, Container::getType)
                .returns(length, Container::getLength)
                .returns(weight, Container::getTaraWeight);
        verify(containerRepository).save(container);
    }

    @Test
    void findsAndVerifiesExistingContainer() throws ContainerException {
        // Arrange
        when(containerRepository.findByBicCode(bic)).thenReturn(Optional.of(example));
        // Act
        var container = containerService.findOrCreateContainer(bic, type, length, weight);
        // Assert
        assertThat(container)
                .isEqualTo(example);
        verify(containerRepository, never()).save(any());
    }

    @Test
    void createsContainerWithBic() throws ContainerException {
        // Arrange
        RepositoryTestUtils.returnEntityOnSave(containerRepository);
        // Act
        var container = containerService.findOrCreateContainer(bic, type, length, weight);
        // Assert
        assertThat(container)
                .returns(bic, Container::getBicCode)
                .returns(type, Container::getType)
                .returns(length, Container::getLength)
                .returns(weight, Container::getTaraWeight);
        verify(containerRepository).save(container);
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            FLAT,             ,
                , ISO_LENGTH_4,
                ,             , 2
            """)
    void throwsIf_verificationFail(ContainerType typeOverride,
                                   ContainerLength lengthOverride,
                                   Integer weightOverride) {
        // Arrange
        when(containerRepository.findByBicCode(bic)).thenReturn(Optional.of(example));
        var possiblyWrongType = Objects.requireNonNullElse(typeOverride, type);
        var possiblyWrongLength = Objects.requireNonNullElse(lengthOverride, length);
        var possiblyWrongWeight = Objects.requireNonNullElse(weightOverride, weight);
        // Assert
        assertThatThrownBy(() -> containerService.findOrCreateContainer(bic,
                                                                        possiblyWrongType,
                                                                        possiblyWrongLength,
                                                                        possiblyWrongWeight))
                .isInstanceOf(InvalidContainerRequestException.class);
    }

}
