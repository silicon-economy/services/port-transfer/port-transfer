/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export type Theme =
  { name: 'blue', label: 'Blue' } |
  { name: 'green', label: 'Green' } |
  { name: 'lightblue', label: 'Light Blue' } |
  { name: 'darkblue', label: 'Dark Blue' } |
  { name: 'lightgreen', label: 'Light Green' } |
  { name: 'darkgreen', label: 'Dark Green' } |
  { name: 'yellow', label: 'Yellow' } |
  { name: 'red', label: 'Red' } |
  { name: 'blockchain', label: 'Blockchain' };

export type ThemeName = 'blue' | 'green' | 'lightblue' | 'darkblue' | 'lightgreen'
  | 'darkgreen' | 'yellow' | 'red' | 'blockchain';

export type ThemeLabel = 'Blue' | 'Green' | 'Light Blue' | 'Dark Blue' | 'Light Green'
  | 'Dark Green' | 'Yellow' | 'Red' | 'Blockchain'
