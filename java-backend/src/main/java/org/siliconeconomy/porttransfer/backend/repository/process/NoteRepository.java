// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository.process;

import org.siliconeconomy.porttransfer.backend.data.process.Note;
import org.siliconeconomy.porttransfer.backend.repository.BaseRepository;

/**
 * Repository for Notes
 *
 * @author F. König
 */
public interface NoteRepository extends BaseRepository<Note> {

}
