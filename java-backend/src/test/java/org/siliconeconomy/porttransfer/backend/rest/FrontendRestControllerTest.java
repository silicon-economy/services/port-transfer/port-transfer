// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.IntegrationTest;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity_;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.repository.RepositoryTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.LoadingUnitRepository;
import org.siliconeconomy.porttransfer.backend.rest.dto.DtoTestUtils;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewCapacityDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewDemandDto;
import org.siliconeconomy.porttransfer.backend.rest.mapper.CapacityMapper;
import org.siliconeconomy.porttransfer.backend.rest.mapper.DemandMapper;
import org.siliconeconomy.porttransfer.backend.security.SecurityConfiguration;
import org.siliconeconomy.porttransfer.backend.service.DemandMatchingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(value = FrontendRestController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
@Import(SecurityConfiguration.class)
@ActiveProfiles("test")
@IntegrationTest
class FrontendRestControllerTest {

    /* Constants */
    private static final String URL_BASE_PATH = "/" + FrontendRestController.VERSION;

    /* Dependencies */
    @MockBean
    AddressRepository addressRepository;
    @MockBean
    CapacityRepository capacityRepository;
    @MockBean
    CompanyRepository companyRepository;
    @MockBean
    DemandRepository demandRepository;
    @MockBean
    LoadingUnitRepository loadingUnitRepository;

    @MockBean
    DemandMatchingService demandMatchingService;

    @MockBean
    DemandMapper demandMapper;
    @MockBean
    CapacityMapper capacityMapper;

    /* Test utilities */
    @Autowired
    private MockMvc mvc;

    private static final ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();

    @Test
    void getDemands() throws Exception {
        // Arrange
        var demand = fakeDemand(1337L);
        when(demandRepository.findAll()).thenReturn(List.of(demand));
        when(demandMapper.map(demand)).thenReturn(DtoTestUtils.createDemandSummaryDto());
        // Act
        mvc.perform(get(URL_BASE_PATH + "/demands").contentType(MediaType.APPLICATION_JSON))
           // Assert
           .andExpect(status().isOk())
           .andExpect(jsonPath("$", isA(List.class)))
           .andExpect(jsonPath("$", is(not(empty()))))
           .andExpect(jsonPath("$[0].id", is(equalTo(1337))));
    }

    @Test
    void getCapacities() throws Exception {
        // Arrange
        var capacity = fakeCapacity(1337L);
        when(capacityRepository.findAll()).thenReturn(List.of(capacity));
        when(capacityMapper.map(capacity)).thenReturn(DtoTestUtils.createCapacitySummaryDto());
        // Act
        mvc.perform(get(URL_BASE_PATH + "/capacities")
                            .contentType(MediaType.APPLICATION_JSON))
           // Assert
           .andExpect(status().isOk())
           .andExpect(jsonPath("$", isA(List.class)))
           .andExpect(jsonPath("$", is(not(empty()))))
           .andExpect(jsonPath("$[0].id", is(equalTo(1337))));
    }

    @Test
    void matchDemand_withMatches() throws Exception {
        // Arrange
        var demand = fakeDemand(1337L);
        var capacity = fakeCapacity(1337L);
        when(demandRepository.findById(1337L)).thenReturn(Optional.of(demand));
        when(demandMatchingService.findMatching(demand)).thenReturn(List.of(capacity));
        when(capacityMapper.map(capacity)).thenReturn(DtoTestUtils.createCapacitySummaryDto());
        // Act
        mvc.perform(get(URL_BASE_PATH + "/demands/1337/match")
                            .contentType(MediaType.APPLICATION_JSON))
           // Assert
           .andExpect(status().isOk())
           .andExpect(jsonPath("$", isA(List.class)))
           .andExpect(jsonPath("$", is(not(empty()))))
           .andExpect(jsonPath("$[0].id", is(equalTo(1337))));
    }

    @Test
    void matchDemand_withoutMatches() throws Exception {
        // Arrange
        var demand = fakeDemand(1337L);
        when(demandRepository.findById(1337L)).thenReturn(Optional.of(demand));
        when(demandMatchingService.findMatching(demand)).thenReturn(Collections.emptyList());
        // Act
        mvc.perform(get(URL_BASE_PATH + "/demands/1337/match")
                            .contentType(MediaType.APPLICATION_JSON))
           // Assert
           .andExpect(status().isOk())
           .andExpect(jsonPath("$", isA(List.class)))
           .andExpect(jsonPath("$", is(empty())));
    }

    @Test
    void matchDemand_wrongId() throws Exception {
        // Arrange
        var demand = fakeDemand(1337L);
        when(demandRepository.findById(1337L)).thenReturn(Optional.of(demand));
        // Act
        mvc.perform(get(URL_BASE_PATH + "/demands/7331/match")
                            .contentType(MediaType.APPLICATION_JSON))
           // Assert
           .andExpect(status().isNotFound());
    }

    @Test
    void putDemand_success() throws Exception {
        // Arrange
        var requestBody = new String(new ClassPathResource("dto/newDemand.json").getInputStream()
                                                                                .readAllBytes(),
                                     StandardCharsets.UTF_8);
        when(demandMapper.map(any(NewDemandDto.class))).thenReturn(fakeDemand(1337L));
        when(demandMapper.map(any(Demand.class))).thenReturn(DtoTestUtils.createDemandSummaryDto());
        RepositoryTestUtils.returnEntityOnSave(loadingUnitRepository, demandRepository);
        // Act
        mvc.perform(put(URL_BASE_PATH + "/demands")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
           // Assert
           .andExpect(status().isCreated())
           .andExpect(jsonPath("$.id", is(1337)));
    }

    @Test
    void putDemand_fail() throws Exception {
        // Arrange
        var requestBody = mapper.writeValueAsString(DtoTestUtils.createNewDemandDto());
        when(demandMapper.map(any(NewDemandDto.class)))
                .thenThrow(new EntityCreationFailedException(List.of("TestFail")));
        // Act
        mvc.perform(put(URL_BASE_PATH + "/demands")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
           // Assert
           .andExpect(status().isBadRequest())
           .andExpect(result ->
                              assertThat(result.getResponse().getErrorMessage())
                                      .isEqualTo("TestFail"));
    }

    @Test
    void putCapacity_success() throws Exception {
        // Arrange
        var requestBody = new String(new ClassPathResource("dto/newCapacity.json").getInputStream()
                                                                                  .readAllBytes(),
                                     StandardCharsets.UTF_8);
        when(capacityMapper.map(any(NewCapacityDto.class))).thenReturn(fakeCapacity(1337L));
        when(capacityMapper.map(any(Capacity.class)))
                .thenReturn(DtoTestUtils.createCapacitySummaryDto());
        RepositoryTestUtils.returnEntityOnSave(capacityRepository);
        // Act
        mvc.perform(put(URL_BASE_PATH + "/capacities")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
           // Assert
           .andExpect(status().isCreated())
           .andExpect(jsonPath("$.id", is(1337)));
    }

    @Test
    void putCapacity_fail() throws Exception {
        // Arrange
        var requestBody = mapper.writeValueAsString(DtoTestUtils.createNewCapacityDto());
        when(capacityMapper.map(any(NewCapacityDto.class)))
                .thenThrow(new EntityCreationFailedException(List.of("TestFail")));
        // Act
        mvc.perform(put(URL_BASE_PATH + "/capacities")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(requestBody))
           // Assert
           .andExpect(status().isBadRequest())
           .andExpect(result ->
                              assertThat(result.getResponse().getErrorMessage())
                                      .isEqualTo("TestFail"));
    }

    @Test
    void getSessionInvariantData() throws Exception {
        // Arrange
        var address = fakeAddress("TheAddress");
        var company = new Company("TheCompany");
        when(addressRepository.findAll()).thenReturn(List.of(address));
        when(companyRepository.findAll()).thenReturn(List.of(company));
        // Act
        mvc.perform(get(URL_BASE_PATH + "/session-invariant-data-sync").contentType(
                   MediaType.APPLICATION_JSON))
           // Assert
           .andExpect(status().isOk())
           .andExpect(jsonPath("$.addressDesignations", is(List.of("TheAddress"))))
           .andExpect(jsonPath("$.companyDesignations", is(List.of("TheCompany"))));
    }

    /* Helper methods */

    @SuppressWarnings("SameParameterValue") /* intentionally mentioned in test classes */
    private static Demand fakeDemand(long id) {
        var demand = DataTestUtils.fakeDemand();
        ReflectionTestUtils.setField(demand, BaseEntity_.ID, id);
        return demand;
    }

    @SuppressWarnings("SameParameterValue") /* intentionally mentioned in test classes */
    private static Capacity fakeCapacity(long id) {
        var capacity = DataTestUtils.fakeCapacity();
        ReflectionTestUtils.setField(capacity, BaseEntity_.ID, id);
        return capacity;
    }

    @SuppressWarnings("SameParameterValue") /* intentionally mentioned in test classes */
    private static Address fakeAddress(String designation) {
        return new Address(designation,
                           "Street",
                           "House",
                           "City",
                           12345,
                           "Country");
    }

}
