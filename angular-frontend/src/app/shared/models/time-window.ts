/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * A type that represents a time window.
 *
 * Used by the timeWindow pipe
 *
 * @author F. König, C. Schlüter
 */
export type TimeWindow = {
  from: Date,
  to: Date
}
