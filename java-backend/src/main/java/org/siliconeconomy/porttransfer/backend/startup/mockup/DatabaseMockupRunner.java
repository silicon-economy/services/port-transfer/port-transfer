// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.mockup;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * This class loads example data from .csv-files and adds it to the database
 *
 * @author F. König
 */
@Slf4j
@Component
@RequiredArgsConstructor
class DatabaseMockupRunner implements ApplicationRunner {

    private final MockupConfiguration configuration;
    private final MockupEntityFactory factory;

    @Override
    @Transactional
    public void run(ApplicationArguments args) {
        if (!configuration.isEnabled()) {
            log.debug("Mockup not enabled");
            return;
        }
        fakeDemand("BICU 123456 7",
                   ContainerLength.ISO_LENGTH_4,
                   "Cyberdyne Systems",
                   "IMl",
                   "ISST");
        fakeDemand("Blue Container", ContainerLength.ISO_LENGTH_L, "Umbrella Corp.", "ISST", "IML");
        fakeDemand("Top Secret", ContainerLength.ISO_LENGTH_2, "Wayne Enterprises", "IMl", "IML");
        fakeCapacity("Metacortex", "KITT", true, "IML", "IML");
        fakeCapacity("Queen Industries", "KARR", false, "ISST", "ISST");
        fakeCapacity("Monsters, Inc.", "Thomas", false, "IML", "ISST");
    }

    private void fakeDemand(String designation,
                            ContainerLength length,
                            String dispatcher,
                            String source,
                            String sink) {
        var loadingUnit = factory.createLoadingUnit(factory.createContainer(length));
        var dispatcherCompany = factory.findOrCreateCompany(dispatcher);
        var connection = new Connection(factory.findOrCreateAddress(source),
                                        factory.findOrCreateAddress(sink));
        factory.createDemand(designation, loadingUnit, dispatcherCompany, connection);
    }

    private void fakeCapacity(String companyName,
                              String vehicleName,
                              boolean extraLong,
                              String source,
                              String sink) {
        var vehicle =
                factory.findOrCreateVehicle(
                        factory.findOrCreateCompany(companyName),
                        vehicleName,
                        extraLong);
        var connection = new Connection(factory.findOrCreateAddress(source),
                                        factory.findOrCreateAddress(sink));
        factory.createCapacity(vehicle, connection);
    }

}
