/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Initializer } from "./initializer";
import { CapacitySummary } from "../../models/http/backend/capacity-summary";

/**
 * An initializer for a Capacity
 *
 * @author F. König, C. Schlüter
 */
export class CapacityInitializer extends Initializer<CapacitySummary> {

  protected dates(): string[] {
    return ["Capacity_Earliest", "Capacity_Latest"];
  }

}
