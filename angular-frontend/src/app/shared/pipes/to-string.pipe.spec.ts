/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ToStringPipe } from './to-string.pipe';

describe('ToStringPipe', () => {
  it('create an instance', () => {
    const pipe = new ToStringPipe();
    expect(pipe).toBeTruthy();
  });
});
