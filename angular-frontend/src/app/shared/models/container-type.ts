/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * The types are modeled according to DIN-SPEC-91073, definition 2.11
 */
export enum ContainerType {
    Flat = "FLAT",
    OpenTop = "OPEN_TOP",
    HighCube = "HIGH_CUBE",
    Bulk = "BULK",
    Iso = "ISO",
    Reefer = "REEFER",
    Tank = "TANK"
}