// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

@UnitTest
class DemandBuilderTest
        implements BuilderContract<Demand>, ConnectionBuilderContract, TimeWindowBuilderContract {

    /* Class under test */
    DemandBuilder demandBuilder;

    /* Helper constants */
    Company dispatcher = DataTestUtils.fakeCompany();
    String designation = "TheDemand";
    int estimatedTime = 1;
    int nettoWeight = 0;
    boolean hazard = false;
    boolean refrigerated = false;
    Container container = DataTestUtils.fakeContainer();

    @BeforeEach
    void beforeEach() {
        demandBuilder = new DemandBuilder();
        // Set values that can not "fail*()"
        demandBuilder.setDesignation(designation);
        demandBuilder.setEstimatedTime(estimatedTime);
        demandBuilder.setNettoWeight(nettoWeight);
        demandBuilder.setHazard(hazard);
        demandBuilder.setRefrigerated(refrigerated);
    }

    @Test
    void buildsCorrectly() {
        // Arrange
        demandBuilder.setDispatcher(dispatcher);
        demandBuilder.setSource(SOURCE);
        demandBuilder.setSink(SINK);
        demandBuilder.setEarliest(EARLIEST);
        demandBuilder.setLatest(LATEST);
        demandBuilder.setContainer(container);
        // Act
        var demand = demandBuilder.buildValid();
        // Assert
        assertThat(demand)
                .returns(dispatcher, Demand::getDispatcher)
                .returns(designation, Demand::getDesignation)
                .returns(estimatedTime, Demand::getEstimatedTime)
                .returns(SOURCE, d -> d.getConnection().getSource())
                .returns(SINK, d -> d.getConnection().getSink())
                .returns(EARLIEST, d -> d.getDeliveryTimeWindow().getEarliest())
                .returns(LATEST, d -> d.getDeliveryTimeWindow().getLatest())

                .extracting(Demand::getLoadingUnit)
                .returns(nettoWeight, LoadingUnit::getNettoWeight)
                .returns(hazard, LoadingUnit::isHazard)
                .returns(refrigerated, LoadingUnit::isRefrigerated)
                .returns(container, LoadingUnit::getContainer);
    }

    @Test
    void throwsIfFailedByDispatcher() {
        // Arrange
        demandBuilder.setSource(SOURCE);
        demandBuilder.setSink(SINK);
        demandBuilder.setEarliest(EARLIEST);
        demandBuilder.setLatest(LATEST);
        demandBuilder.setContainer(container);
        // Act
        demandBuilder.failDispatcher();
        // Assert
        assertThatThrownBy(demandBuilder::build).isInstanceOf(EntityCreationFailedException.class);
    }

    @Test
    void throwsIfFailedByContainer() {
        // Arrange
        demandBuilder.setDispatcher(dispatcher);
        demandBuilder.setSource(SOURCE);
        demandBuilder.setSink(SINK);
        demandBuilder.setEarliest(EARLIEST);
        demandBuilder.setLatest(LATEST);
        // Act
        demandBuilder.failContainer();
        // Assert
        assertThatThrownBy(demandBuilder::build).isInstanceOf(EntityCreationFailedException.class);
    }

    /* Contract methods */

    @Override
    public Builder<Demand> getValidBuilder() {
        demandBuilder.setDispatcher(dispatcher);
        demandBuilder.setSource(SOURCE);
        demandBuilder.setSink(SINK);
        demandBuilder.setEarliest(EARLIEST);
        demandBuilder.setLatest(LATEST);
        demandBuilder.setContainer(container);
        return demandBuilder;
    }

    @Override
    public ConnectionBuilder<?> getValidBuilderAwaitingConnection() {
        demandBuilder.setDispatcher(dispatcher);
        demandBuilder.setEarliest(EARLIEST);
        demandBuilder.setLatest(LATEST);
        demandBuilder.setContainer(container);
        return demandBuilder;
    }

    @Override
    public TimeWindowBuilder<?> getValidBuilderAwaitingTimeWindow() {
        demandBuilder.setDispatcher(dispatcher);
        demandBuilder.setSource(SOURCE);
        demandBuilder.setSink(SINK);
        demandBuilder.setContainer(container);
        return demandBuilder;
    }

}
