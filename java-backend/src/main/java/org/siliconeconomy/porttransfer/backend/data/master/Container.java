// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Positive;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.springframework.lang.NonNull;

/**
 * Information about a physical container needed to arrange transportation
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@AllArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Container extends BaseEntity {

    /**
     * The code with wich the container is registered at the "Bureau of International Containers"
     * (BIC). Can be used to identify the container and to automatically fetch information, such as
     * the {@link #type} and {@link #length}.
     */
    @Column(unique = true, updatable = false)
    private String bicCode;

    /**
     * The container type
     * <p>
     * It is not used to determine if a {@link Vehicle} can carry this container.
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private ContainerType type;

    /**
     * The container length
     * <p>
     * This is the main criteria when determining if a {@link Vehicle} can carry a given Container
     */
    @NonNull
    @Column(updatable = false)
    private ContainerLength length;

    /**
     * The container's weight (empty) in kg
     */
    @NonNull
    @Column(updatable = false)
    @Positive
    private int taraWeight;

}
