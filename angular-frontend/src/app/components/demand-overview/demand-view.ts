/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ContainerLength, getContainerLength} from "../../shared/models/container-length";
import {ContainerType} from "../../shared/models/container-type";
import {DemandSummary} from "../../shared/models/http/backend/demand-summary";

/**
 * Implements the DemandSummary and adds a mapped size value for filtering data in a table.
 *
 * @author C. Schlüter
 */
export class DemandView implements DemandSummary {
    sourceSummary: DemandSummary;

    id: number;
    Demand_Designation?: string;
    LU_Number?: string;
    LU_Container_Type: ContainerType;
    LU_Container_Size: ContainerLength;
    LU_Weight_Tara: number;
    LU_Order_Weight_Netto: number;
    Order_PickUp_Location_Designation: string;
    Order_DropOff_Location_Designation: string;
    Order_DropOff_Earliest: Date;
    Order_DropOff_Latest: Date;

    /**
     * Container size as a number in ft.
     */
    LU_Container_Size_Mapped: number;

    constructor(demandSummary: DemandSummary) {
        this.sourceSummary = demandSummary;
        this.id = demandSummary.id;
        this.Demand_Designation = demandSummary.Demand_Designation;
        this.LU_Number = demandSummary.LU_Number;
        this.LU_Container_Type = demandSummary.LU_Container_Type;
        this.LU_Container_Size = demandSummary.LU_Container_Size;
        this.LU_Weight_Tara = demandSummary.LU_Weight_Tara;
        this.LU_Order_Weight_Netto = demandSummary.LU_Order_Weight_Netto;
        this.Order_PickUp_Location_Designation = demandSummary.Order_PickUp_Location_Designation;
        this.Order_DropOff_Location_Designation = demandSummary.Order_DropOff_Location_Designation;
        this.Order_DropOff_Earliest = demandSummary.Order_DropOff_Earliest;
        this.Order_DropOff_Latest = demandSummary.Order_DropOff_Latest;
        this.LU_Container_Size_Mapped = getContainerLength(demandSummary.LU_Container_Size);
    }
}
