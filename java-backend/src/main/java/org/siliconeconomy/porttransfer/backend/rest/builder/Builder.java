// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

/**
 * A Builder that remembers if the build process has {@link #fail(String) failed}
 *
 * @param <Entity> the entity to be build
 * @author F. König
 */
public interface Builder<Entity> {

    /**
     * Add an error message
     * <p>
     * If this method is called at least once, calling {@link #build()} will throw an exception
     *
     * @param message the message
     */
    void fail(String message);

    /**
     * Builds if no failures have happened
     *
     * @return the expected entity
     * @throws EntityCreationFailedException an exception containing all messages passed to
     *                                       {@link #fail(String)}
     */
    Entity build() throws EntityCreationFailedException;

}
