// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.model;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.springframework.lang.NonNull;

/**
 * Embeddable data class containing two {@link Address}
 *
 * @author F. König
 */
@Embeddable
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Setter(AccessLevel.PROTECTED)
public class Connection {

    /**
     * The start of the connection
     */
    @NonNull
    @ManyToOne
    private Address source;

    /**
     * The end of the connection
     */
    @NonNull
    @ManyToOne
    private Address sink;

}
