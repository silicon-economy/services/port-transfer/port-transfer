// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.rest.builder.ConnectionBuilder;
import org.siliconeconomy.porttransfer.backend.rest.dto.DtoWithConnection;

/**
 * Extends a mapper with the ability to map a {@link org.siliconeconomy.porttransfer.backend.model.Connection}
 *
 * @author F. König
 */
public interface ConnectionMapper {

    /**
     * Maps the connection.
     * <p>
     * Will fail the builder if the addressRepository does not contain the necessary entities
     *
     * @param builder           the builder
     * @param addressRepository the addressRepository
     * @param dto               the incoming dto
     * @see org.siliconeconomy.porttransfer.backend.rest.builder.Builder#fail(String)
     */
    default void mapConnection(ConnectionBuilder<?> builder,
                               AddressRepository addressRepository,
                               DtoWithConnection dto) {
        addressRepository.findByDesignation(dto.getSource())
                         .ifPresentOrElse(builder::setSource, builder::failSource);
        addressRepository.findByDesignation(dto.getSink())
                         .ifPresentOrElse(builder::setSink, builder::failSink);
    }

}
