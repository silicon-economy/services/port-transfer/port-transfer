/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { EnumTableFilter } from "./enum-table-filter";

enum TestEnum {
  A = 'A',
  B = 'B'
}

describe('EnumTableFilter', () => {

  // Object under test
  let filter: EnumTableFilter<TestEnum>;

  beforeEach(() => {
    filter = new EnumTableFilter("foo", TestEnum);
  });

  it('should create', () => {
    // Assert
    expect(filter).toBeTruthy();
    expect(filter.options['00_all']).toBeNull();
    expect(filter.options['A']).toEqual(TestEnum.A);
  });

  it('should match', () => {
    // Arrange
    filter.currentValue = TestEnum.B;
    // Act
    let result = filter.matches(TestEnum.B);
    // Assert
    expect(result).toBeTrue();
  });

  it('should NOT match', () => {
    // Arrange
    filter.currentValue = TestEnum.B;
    // Act
    let result = filter.matches(TestEnum.A);
    // Assert
    expect(result).toBeFalse();
  });

  it('should null match always', () => {
    // Arrange
    filter.currentValue = null;
    // Act
    let result = filter.matches(TestEnum.A);
    let result2 = filter.matches(TestEnum.B)
    // Assert
    expect(result).toBeTrue();
    expect(result2).toBeTrue();
  });

  it('should be active', () => {
    // Arrange
    filter.currentValue = TestEnum.A;
    // Act
    let result = filter.isActive();
    // Assert
    expect(result).toBeTrue();
  });

  it('should NOT be active', () => {
    // Arrange
    filter.currentValue = null;
    // Act
    let result = filter.isActive();
    // Assert
    expect(result).toBeFalse();
  });
});