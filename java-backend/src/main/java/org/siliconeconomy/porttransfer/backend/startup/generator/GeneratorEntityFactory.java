// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.ContainerRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.LoadingUnitRepository;
import org.springframework.stereotype.Component;

/**
 * This factory creates entities for the generator
 *
 * @author F. König
 */
@Slf4j
@Component
@AllArgsConstructor
class GeneratorEntityFactory {

    private static final int WEIGHT_EMPTY_CONTAINER = 1000;
    private static final int WEIGHT_CONTAINER_LOAD = 8000;
    private static final int MAX_VEHICLE_WEIGHT = 9001;

    private final AddressRepository addressRepository;
    private final CompanyRepository companyRepository;
    private final ContainerRepository containerRepository;
    private final VehicleRepository vehicleRepository;
    private final LoadingUnitRepository loadingUnitRepository;
    private final CapacityRepository capacityRepository;
    private final DemandRepository demandRepository;

    /**
     * Creates a new Company with a new Address
     *
     * @param designation the designation of company and address, and the street name of the address
     * @param houseNumber the house number of the address
     * @return the new Company as it now exists in the database
     */
    public Company createCompany(String designation, String houseNumber) {
        var company = new Company(designation);
        var address = createAddress(designation, houseNumber);
        company.setAddress(address);
        return companyRepository.save(company);
    }

    private Address createAddress(String designation, String houseNumber) {
        var address = new Address(designation,
                                  designation + " Street",
                                  houseNumber,
                                  "Mos Eisley",
                                  25869,
                                  "Tatooine");
        return addressRepository.save(address);
    }

    /**
     * Creates a new Vehicle
     *
     * @param owner       the owning company
     * @param designation the designation of the vehicle
     * @param extraLong   see {@link Vehicle#isFortyFiveFootCapable()}
     * @return the new Vehicle as it now exists in the database
     */
    public Vehicle createVehicle(Company owner, String designation, boolean extraLong) {
        var vehicle = new Vehicle(owner,
                                  designation,
                                  MAX_VEHICLE_WEIGHT,
                                  extraLong,
                                  Modality.ROAD);
        return vehicleRepository.save(vehicle);
    }

    private LoadingUnit createLoadingUnit(ContainerType type,
                                          ContainerLength length,
                                          boolean isEmpty) {
        var loadingUnit = new LoadingUnit(createContainer(type, length),
                                          getLoadWeight(isEmpty),
                                          false,
                                          false);
        return loadingUnitRepository.save(loadingUnit);
    }

    private Container createContainer(ContainerType type, ContainerLength length) {
        var container = new Container(type, length, WEIGHT_EMPTY_CONTAINER);
        return containerRepository.save(container);
    }

    private int getLoadWeight(boolean isEmpty) {
        if (isEmpty) {
            return 0;
        } else {
            return WEIGHT_CONTAINER_LOAD;
        }
    }

    /**
     * Creates a new Demand
     *
     * @param type               the type of the container
     * @param length             the length of the container
     * @param isEmpty            if the container is empty, its
     *                           {@link LoadingUnit#getNettoWeight() weight} will be 0
     * @param dispatcher         the company creating this demand. Its
     *                           {@link Company#getAddress() Address} will be the Demand's
     *                           {@link Connection#getSink() source}
     * @param target             the Demand's {@link Connection#getSink() sink}
     * @param deliveryTimeWindow the delivery time window
     * @param estimatedTime      the estimated time
     */
    public void createDemand(ContainerType type,
                             ContainerLength length,
                             boolean isEmpty,
                             Company dispatcher,
                             Address target,
                             TimeWindow deliveryTimeWindow,
                             int estimatedTime) {
        String designation = dispatcher.getDesignation() + " -> " + target.getDesignation();
        var demand = new Demand(createLoadingUnit(type, length, isEmpty),
                                dispatcher,
                                new Connection(dispatcher.getAddress(), target),
                                deliveryTimeWindow,
                                designation,
                                estimatedTime);
        demandRepository.save(demand);
    }

    /**
     * Creates a new Capacity
     *
     * @param vehicle    the vehicle
     * @param timeWindow the timeWindow
     * @param connection the connection
     */
    public void createCapacity(Vehicle vehicle, TimeWindow timeWindow, Connection connection) {
        var capacity = new Capacity(vehicle,
                                    timeWindow,
                                    connection,
                                    false,
                                    false);
        capacityRepository.save(capacity);
    }

}
