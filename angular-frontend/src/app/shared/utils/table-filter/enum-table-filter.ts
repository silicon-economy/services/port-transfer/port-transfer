/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TableFilter } from "./table-filter";

export class EnumTableFilter<T> implements TableFilter<T> {
    options: { [key: string]: T; };
    key: string;
    currentValue: T; 
       
    /**
     * Creates an EnumTableFilter with given key and key-value-type plus 'all' as options. 
     * @param key the Key of the filter
     * @param options The given options (should be an enum or another key-value-type)
     */
    constructor(key: string, options: any) {
        this.key = key;
        this.options = options;
        this.options['00_all'] = null; // add an "all"-option to the given enum.
        this.currentValue = null;
    }

    matches(value: T): boolean {
        return (this.currentValue == null) || (value == this.currentValue);
    }

    isActive(): boolean {
        return this.currentValue != null;
    }

    reset() {
        this.currentValue = null;
    }
}