// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository.master;

import java.util.Optional;

import org.siliconeconomy.porttransfer.backend.data.master.User;
import org.siliconeconomy.porttransfer.backend.repository.BaseRepository;

/**
 * Repository for User
 *
 * @author F. König
 */
public interface UserRepository extends BaseRepository<User> {

    Optional<User> findByUsername(String username);

}
