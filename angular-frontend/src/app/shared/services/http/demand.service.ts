/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment as env } from 'src/environments/environment';
import { DemandInitializer } from "../initialize/demand-initializer";
import { DemandSummary } from '../../models/http/backend/demand-summary';
import { NewDemand } from '../../models/http/backend/new-demand';
import { SnackbarService } from '../state/snackbar.service';

/**
 * This service provides demands and has the functionality to add
 * a demand.
 *
 * @author C. Schlüter, F. König, S. Jankowski
 */
@Injectable({
  providedIn: 'root'
})
export class DemandService {

  private readonly demandsUrl: string = env.apiHost + env.api + env.endPoints.demands;
  private readonly httpClient: HttpClient;
  private readonly initializer: DemandInitializer;

  private readonly demands: BehaviorSubject<DemandSummary[]>;

  constructor(httpClient: HttpClient, public snackService: SnackbarService) {
    this.httpClient = httpClient;
    this.demands = new BehaviorSubject<DemandSummary[]>([]);
    this.initializer = new DemandInitializer();
    this.fetchDemands();
  }

  /**
   * Get current demands as Observable
   *
   * @return an {@link Observable} for the {@link DemandSummary[]} values
   */
  public getDemands(): Observable<DemandSummary[]> {
    return this.demands.asObservable();
  }

  private fetchDemands(): void {
    this.httpClient.get<DemandSummary[]>(this.demandsUrl)
      .pipe(this.initializer.initializeList())
      .subscribe({
        next: (response) => this.demands.next(response),
        error: (error) => this.snackService.handleDefaultErrorsWithText('error.could_not_load_demands', error.error)
      });
  }

  public putDemand(demand: NewDemand): void {
    console.log("Posting the given demand " + JSON.stringify(demand));
    this.httpClient.put<NewDemand>(this.demandsUrl, demand).subscribe({
      next: (response) => {
        console.log(response)
        this.fetchDemands();
        this.snackService.handleSuccessfulCreation(response.Demand_Designation, "demand.demand");
      },
      error: (error) => {
        if (error.status === 400) {
          this.snackService.handleCreateError('error.could_not_put_demand', error.error, "demand.interface.");
        } else {
          this.snackService.handleDefaultErrorsWithText('error.could_not_put_demand', error.error);
        }
        return error;
      }
    })
  }
}
