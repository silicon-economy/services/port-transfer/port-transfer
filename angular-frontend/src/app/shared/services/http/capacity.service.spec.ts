/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { environment as env } from 'src/environments/environment';
import { CAPACITIES } from '../../test/capacities';
import { SnackbarService } from '../state/snackbar.service';
import { CapacityService } from './capacity.service';

describe('CapacityService', () => {
  let service: CapacityService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CapacityService,
        {provide: SnackbarService, useValue: {pushNewStatus: () => ({})}},
      ],
      imports: [
        HttpClientTestingModule,
        TranslateTestingModule.withTranslations('de', {})
      ]
    });
    service = TestBed.inject(CapacityService);
    httpController = TestBed.inject(HttpTestingController);

    httpController.expectOne(env.apiHost + env.api + env.endPoints.capacities)
      .flush(CAPACITIES);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get Capacities', fakeAsync(() => {
    service.getCapacities().subscribe(r => {
      expect(r).toEqual(CAPACITIES);
      expect(r[0].id).toBe(CAPACITIES[0].id);
    });
    flush();
  }));
});
