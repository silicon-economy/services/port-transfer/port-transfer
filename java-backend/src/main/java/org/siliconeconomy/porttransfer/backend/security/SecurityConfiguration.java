// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.security;

import lombok.Data;
import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * Configuration class for security-related properties
 *
 * @author F. König
 */
@ConfigurationProperties(prefix = "application.security")
@Data
public class SecurityConfiguration {

    /**
     * Origin url of incoming requests, used to configure CORS
     * <p>
     * Not optional
     */
    private String[] corsOrigin;

    /**
     * This bean makes the keycloak configurer use "application.properties" and related files as a
     * configuration source instead of the default "keycloak.json"
     *
     * @return the bean
     */
    @Bean
    public KeycloakSpringBootConfigResolver keycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

}
