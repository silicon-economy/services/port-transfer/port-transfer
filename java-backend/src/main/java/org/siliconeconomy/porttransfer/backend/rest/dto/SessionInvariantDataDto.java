// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.util.List;

import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.ADDRESS_DESIGNATION_LIST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.COMPANY_DESIGNATION_LIST;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class SessionInvariantDataDto {

    @JsonProperty(COMPANY_DESIGNATION_LIST)
    List<String> companyDesignations;

    @JsonProperty(ADDRESS_DESIGNATION_LIST)
    List<String> addressDesignations;

}
