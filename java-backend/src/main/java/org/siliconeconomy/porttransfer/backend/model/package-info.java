// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains data classes, enums and interfaces
 * <p>
 * Contrary to the {@link org.siliconeconomy.porttransfer.backend.data data} the current package's
 * contents are not meant to be stored in the database themselves, they may however contribute to
 * a database entry, e.g. by being {@link javax.persistence.Embeddable Embeddable} or
 * {@link java.io.Serializable Serializable}
 */

package org.siliconeconomy.porttransfer.backend.model;
