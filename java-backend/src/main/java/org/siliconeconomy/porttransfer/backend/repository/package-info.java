// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains {@link org.springframework.stereotype.Repository @Repository} interfaces
 * which allow interaction with the database
 * <p>
 * The package structure mirrors that of the data classes
 *
 * @author F. König
 */

package org.siliconeconomy.porttransfer.backend.repository;
