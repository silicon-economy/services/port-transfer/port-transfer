/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsPageComponent } from "./pages/settings-page/settings-page.component";
import { ErrorPageComponent } from "./pages/error-page/error-page.component";
import { ImprintLegalPageComponent } from "./pages/imprint-legal-page/imprint-legal-page.component";
import { PrivacyPageComponent } from "./pages/privacy-page/privacy-page.component";
import { DemandCapacityPageComponent } from "./pages/demand-capacity-page/demand-capacity-page.component";
import { CreateDemandComponent } from './pages/create-demand/create-demand.component';
import { LoginGuard } from "./guard/login-guard.service";

const routes: Routes = [
  {
    path: '', canActivate: [LoginGuard], children: [
      {path: '', pathMatch: 'full', redirectTo: 'demand-capacity'},
      {path: 'settings', component: SettingsPageComponent},
      {path: 'privacy', component: PrivacyPageComponent},
      {path: 'imprint-legal', component: ImprintLegalPageComponent},
      {path: 'demand-capacity', component: DemandCapacityPageComponent},
      {path: 'create-demand', component: CreateDemandComponent},
      {path: '**', component: ErrorPageComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
