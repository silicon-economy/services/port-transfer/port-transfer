// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data;

import javax.persistence.MappedSuperclass;

import org.springframework.data.jpa.domain.AbstractPersistable;

/**
 * Common superclass for all entities
 *
 * @author F. König
 */
@MappedSuperclass
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public abstract class BaseEntity extends AbstractPersistable<Long> {

}
