/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { LoginGuard } from "./login-guard.service";
import { KeycloakService } from "keycloak-angular";
import { RouterTestingModule } from "@angular/router/testing";
import { RouterStateSnapshot } from "@angular/router";

describe('LoginGuard', () => {
  let loginGuard: LoginGuard;
  let keycloakServiceMock: jasmine.SpyObj<KeycloakService>;

  beforeEach(async () => {
    const spy = jasmine.createSpyObj('KeycloakService', ['login'])
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        LoginGuard,
        {provide: KeycloakService, useValue: spy}
      ]
    });
    loginGuard = TestBed.inject(LoginGuard);
    keycloakServiceMock = TestBed.inject(KeycloakService) as jasmine.SpyObj<KeycloakService>;
    keycloakServiceMock.login.and.returnValue(Promise.resolve());
  });

  it('should be created', () => {
    expect(loginGuard).toBeTruthy();
  });

  it('should call login if not logged in', async function () {
    // Arrange
    loginGuard["authenticated"] = false;
    // // Act
    await loginGuard.isAccessAllowed(null, {url: "url"} as RouterStateSnapshot);
    // Assert
    expect(keycloakServiceMock.login).toHaveBeenCalled();
  });

  it('should skip login if already logged in', async function () {
    // Arrange
    loginGuard["authenticated"] = true;
    // // Act
    let authenticationStatus = await loginGuard.isAccessAllowed(null, {url: "url"} as RouterStateSnapshot);
    // Assert
    expect(keycloakServiceMock.login).not.toHaveBeenCalled();
    expect(authenticationStatus).toBeTrue();
  });

});
