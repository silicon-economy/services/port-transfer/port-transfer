// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.process;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.springframework.lang.NonNull;

/**
 * A capacity represents the availability of a vehicle from a source to a sink during a timeframe
 * as well as how much of that availability is already booked
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Capacity extends BaseEntity {

    /**
     * The vehicle that is available
     */
    @NonNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Vehicle vehicle;

    /**
     * A list of all orders that this capacity is part of. If this list is not empty the
     * capacity should be {@link #fixed}
     */
    @OneToMany(mappedBy = Order_.CAPACITY)
    @ToString.Exclude
    @SuppressWarnings("FieldMayBeFinal") /* Field will be set by JPA */
    private List<Order> orders = new LinkedList<>();

    /**
     * The time window where the vehicle is available
     */
    @NonNull
    @Embedded
    private TimeWindow timeWindow;

    /**
     * The start- and endpoint of the capacity
     */
    @NonNull
    @Embedded
    private Connection connection;

    /**
     * Whether the capacity has a fixed route and start/end time.
     * <p>
     * A "fixed capacity" can only pick up at the time and address that it starts, and can only drop
     * off at the time and address that it ends.
     * <p>
     * A "flexible capacity" can pick up and drop off at any address, as long as the total travel
     * time "source -> pickUp -> dropOff -> sink" fits within the time window.
     *
     * @see #timeWindow
     * @see #connection
     */
    @NonNull
    @Column(nullable = false)
    @Setter
    private boolean fixed;

    /**
     * Whether the capacity is fully booked.
     */
    @NonNull
    @Column(nullable = false)
    @Setter
    private boolean fullyBooked;

}
