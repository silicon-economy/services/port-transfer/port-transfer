// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.model.Modality;

@ExtendWith(MockitoExtension.class)
@UnitTest
class GeneratorRunnerTest {

    @Mock
    GeneratorEntityFactory factory;

    @Mock
    GeneratorContextValidator validator;

    /* Class under test */
    GeneratorRunner generatorRunner;

    /* Helper classes */
    GeneratorConfiguration configuration;

    RandomGenerationService random;

    @BeforeEach
    void beforeEach() {
        configuration = createValidConfiguration();
        random = spy(new RandomGenerationService(configuration));
        generatorRunner = new GeneratorRunner(validator, configuration, factory, random);
    }

    @Test
    void run() {
        // Arrange
        when(validator.canGenerate()).thenReturn(true);
        when(factory.createCompany(anyString(), anyString()))
                .thenAnswer(this::fakeCompanyWithAddress);
        when(factory.createVehicle(any(), any(), anyBoolean()))
                .thenAnswer(this::createVehicleRespectingOwnerAndLength);
        // Act
        generatorRunner.run(null);
        // Assert
        verify(factory, atLeastOnce()).createCompany(any(), any());
        verify(factory, atLeastOnce()).createVehicle(any(), any(), anyBoolean());
        verify(factory, atLeastOnce()).createCapacity(any(), any(), any());
        verify(factory, atLeastOnce())
                .createDemand(any(), any(), anyBoolean(), any(), any(), any(), anyInt());
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void run_containersFull_allOrNone(boolean fullContainers) {
        // Arrange
        when(random.isFullContainer()).thenReturn(fullContainers);

        when(validator.canGenerate()).thenReturn(true);
        when(factory.createCompany(anyString(), anyString()))
                .thenAnswer(this::fakeCompanyWithAddress);
        when(factory.createVehicle(any(), any(), anyBoolean()))
                .thenAnswer(this::createVehicleRespectingOwnerAndLength);
        // Act
        generatorRunner.run(null);
        // Assert
        /* readability */
        @SuppressWarnings("UnnecessaryLocalVariable")
        var containerIsEmpty = fullContainers;
        verify(factory, never())
                .createDemand(any(), any(), eq(containerIsEmpty), any(), any(), any(), anyInt());
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void run_vehiclesExtraLong_allOrNone(boolean extraLongVehicles) {
        // Arrange
        when(random.isExtraLong()).thenReturn(extraLongVehicles);

        when(validator.canGenerate()).thenReturn(true);
        when(factory.createCompany(anyString(), anyString()))
                .thenAnswer(this::fakeCompanyWithAddress);
        when(factory.createVehicle(any(), any(), anyBoolean()))
                .thenAnswer(this::createVehicleRespectingOwnerAndLength);
        // Act
        generatorRunner.run(null);
        // Assert
        var isShortVehicle = !extraLongVehicles;
        verify(factory, never()).createVehicle(any(), any(), eq(isShortVehicle));
    }

    @Test
    void run_doesNothingIfDisabled() {
        // Arrange
        configuration.setEnabled(false);
        // Act
        generatorRunner.run(null);
        // Assert
        verifyNoInteractions(factory, random);
    }

    @Test
    void run_doesNothingIfInvalid() {
        // Arrange
        when(validator.canGenerate()).thenReturn(false);
        // Act
        generatorRunner.run(null);
        // Assert
        verifyNoInteractions(factory, random);
    }

    /* Helper methods */

    private Company fakeCompanyWithAddress(InvocationOnMock invocation) {
        var company = DataTestUtils.fakeCompany();
        company.setAddress(DataTestUtils.fakeAddress());
        return company;
    }

    private Vehicle createVehicleRespectingOwnerAndLength(InvocationOnMock invocation) {
        return new Vehicle(invocation.getArgument(0),
                           "",
                           0,
                           invocation.getArgument(2),
                           Modality.ROAD);
    }

    private static GeneratorConfiguration createValidConfiguration() {
        var configuration = new GeneratorConfiguration();
        configuration.setEnabled(true);
        configuration.setTerminals(List.of("Terminal1", "Terminal2"));
        configuration.setShippingCompanies(List.of("ShippingCompany1", "ShippingCompany2"));
        configuration.setDemandMultiplier(1);
        configuration.setCapacityMultiplier(1);
        configuration.setCalendarWeek(1);
        configuration.setBeginnOfDay(8);
        configuration.setEndOfDay(20);
        configuration.setMinimumCapacityDuration(1);
        configuration.setMaximumCapacityDuration(4);
        configuration.setNonStandardLengthProbability(10);
        configuration.setFullContainerProbability(10);
        return configuration;
    }

}
