// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Provides String constants to be used as json keys
 *
 * @author F. König
 * @see com.fasterxml.jackson.annotation.JsonProperty
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyKeys {

    public static final String DISPATCHER_DESIGNATION = "Tmp_Dispatcher";

    public static final String OWNER_DESIGNATION = "Tmp_Owner";

    public static final String DEMAND_DESIGNATION = "Demand_Designation";

    public static final String VEHICLE_DESIGNATION = "Vehicle_Designation";

    public static final String DEMAND_ESTIMATED_TIME = "Demand_EstimatedTime";

    public static final String LU_NUMBER = "LU_Number";

    public static final String CONTAINER_TYPE = "LU_Container_Type";

    public static final String CONTAINER_SIZE = "LU_Container_Size";

    public static final String WEIGHT_TARA = "LU_Weight_Tara";

    public static final String WEIGHT_NETTO = "LU_Order_Weight_Netto";

    public static final String PICK_UP_DESIGNATION = "Order_PickUp_Location_Designation";

    public static final String DROP_OFF_DESIGNATION = "Order_DropOff_Location_Designation";

    public static final String DROP_OFF_EARLIEST = "Order_DropOff_Earliest";

    public static final String DROP_OFF_LATEST = "Order_DropOff_Latest";

    public static final String DANGEROUS_GOODS_INDICATION = "LU_Order_DangerousGoodsIndication";

    public static final String REEFER_INDICATION = "LU_Order_ReeferIndication";

    public static final String ID = "id";

    public static final String MODALITY = "Vehicle_Modality";

    public static final String CAN_45_FT = "Vehicle_Can_45ft";

    public static final String WEIGHT_NETTO_MAX = "Vehicle_Weight_Netto_Max";

    public static final String CAPACITY_SOURCE = "Capacity_Origin_Location_Designation";

    public static final String CAPACITY_SINK = "Capacity_Target_Location_Designation";

    public static final String CAPACITY_EARLIEST = "Capacity_Earliest";

    public static final String CAPACITY_LATEST = "Capacity_Latest";

    public static final String FIXED_ROUTE = "Capacity_FixedRoute";

    public static final String COMPANY_DESIGNATION_LIST = "companyDesignations";

    public static final String ADDRESS_DESIGNATION_LIST = "addressDesignations";

}
