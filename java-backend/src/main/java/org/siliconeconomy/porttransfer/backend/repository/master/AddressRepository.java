// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository.master;

import java.util.Optional;

import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.repository.BaseRepository;

/**
 * Repository for Address
 *
 * @author F. König
 */
public interface AddressRepository extends BaseRepository<Address> {

    Optional<Address> findByDesignation(String designation);

}
