// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

public interface BuilderContract<Entity> {

    /* Class under test */
    Builder<Entity> getValidBuilder();

    @Test
    default void buildsIfValid() {
        // Arrange
        var builder = getValidBuilder();
        // Assert
        assertThatCode(builder::build).doesNotThrowAnyException();
    }

    @Test
    default void throwsIfFailed() {
        // Arrange
        var builder = getValidBuilder();
        // Act
        builder.fail("TestFail");
        // Assert
        assertThatThrownBy(builder::build).isInstanceOf(EntityCreationFailedException.class);
    }

}
