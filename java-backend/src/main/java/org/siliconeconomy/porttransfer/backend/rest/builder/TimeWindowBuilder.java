// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import java.time.OffsetDateTime;

/**
 * A Builder that is able to build a
 * {@link org.siliconeconomy.porttransfer.backend.model.TimeWindow}
 *
 * @param <Entity> the entity to be build
 * @author F. König
 */
public interface TimeWindowBuilder<Entity> extends Builder<Entity> {

    void setEarliest(OffsetDateTime earliest);

    void failEarliest();

    void setLatest(OffsetDateTime latest);

    void failLatest();

}
