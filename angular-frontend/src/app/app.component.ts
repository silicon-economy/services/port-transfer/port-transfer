/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { animate, state, style, transition, trigger } from "@angular/animations";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { MatDrawerMode } from "@angular/material/sidenav";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslateService } from '@ngx-translate/core';
import { CookieDialogComponent } from "./shared/dialogs/cookie-dialog/cookie-dialog.component";
import { ModeIcon } from './shared/models/mode';
import { BlockchainModeService } from './shared/services/state/blockchain-mode.service';
import { CookieConsentService } from './shared/services/state/cookie-consent.service';
import { FooterService } from "./shared/services/state/footer.service";
import { SidenavService } from "./shared/services/state/sidenav.service";
import { SnackbarService } from "./shared/services/state/snackbar.service";
import { ThemeService } from "./shared/services/state/theme.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('indicatorRotation', [
      state('expanded', style({ transform: 'rotate(180deg)' })),
      state('collapsed, void', style({ transform: 'rotate(0deg)' })),
      transition('expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ]),
    /** Animation that expands and collapses the panel content. */
    trigger('listHeight', [
      state('expanded', style({ height: '*', visibility: 'visible' })),
      state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
      transition('expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ]),
  ]
})

export class AppComponent implements OnInit {
  title = 'Port Transfer';
  breakpointStateLtMd = false;
  modeIcon: ModeIcon;
  footerState: boolean;
  sidenavMode: MatDrawerMode = 'side';
  sidenavState;
  desktopSidenavState: boolean;
  exAppState = false;
  currentClassName: string;

  constructor(
    public breakpointObserver: BreakpointObserver,
    public dialog: MatDialog,
    public cookieService: CookieConsentService,
    public themeService: ThemeService,
    public footerService: FooterService,
    public sidenavService: SidenavService,
    public blockchainService: BlockchainModeService,
    public translate: TranslateService,
    public snack: SnackbarService,
    public snackBar: MatSnackBar
  ) {
    // Translate Service
    this.translate.addLangs(['en', 'de']);
    this.translate.setDefaultLang('de');
    const browserLang = this.translate.getBrowserLang();
    this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'de');

    // Sidenav State
    sidenavService.getSidenavState$().subscribe(side => {
      this.desktopSidenavState = side;
    })

    // Breakpoints
    breakpointObserver.observe([
      Breakpoints.Small,
      Breakpoints.XSmall
    ]).subscribe(result => {
      if (result.matches) {
        this.breakpointStateLtMd = true;
        this.activateHandsetLayout();
      } else {
        this.breakpointStateLtMd = false;
        this.deactivateHandsetLayout();
      }
    });

    // Cookies
    cookieService.getCookieConsent().subscribe(consent => {
      if (consent === false) {
        let cookieDialogRef = this.dialog.open(CookieDialogComponent, { disableClose: true });
        cookieDialogRef
          .afterClosed()
          .subscribe(() => {
            cookieService.setCookieConsent(true)
          });
      }
    });

    // Themes and Modes
    themeService.getModeName$().subscribe(() => {
      this.modeIcon = themeService.getNextModeIcon();
      this.applyClass(themeService.getClassName());
    })
    themeService.getThemeName$().subscribe(() => {
      this.applyClass(themeService.getClassName());
    })

    // Snackbar for Error Handling
    this.snack.getStatusMessage().subscribe(r => this.openSnackBar(r, this.snack.getNextDuration()));
  }

  private openSnackBar(message: string, duration: number): void {
    if (duration > 0) {
      this.snackBar.open(message, this.translate.instant('close'), {duration: duration});
    } else {
      this.snackBar.open(message, this.translate.instant('close'));
    } 
  }

  changeLang(language: string) {
    this.translate.use(language);
  }

  ngOnInit(): void {
    // cookie consent
    // if (localStorage.getItem('cookie-consent') !== 'true') {
    //   // reset application state to default
    //   localStorage.clear();
    //
    //   this.themeService.resetMode();
    //   this.themeService.resetTheme();
    //   document.body.classList.add(this.themeService.getAvailableThemes()[0].name + '-' +
    //     this.themeService.getAvailableModes()[0].name);
    //   this.themeModeIcon = this.themeService.getNextModeIcon();
    //
    //   this.footerService.resetFooterState();
    //
    //   this.sidenavService.resetSidenavState();
    //
    //   // get consent from user and app state from local storage
    //   let cookieDialogRef = this.dialog.open(CookieDialogComponent, {disableClose: true});
    //   cookieDialogRef
    //     .afterClosed()
    //     .subscribe(() => {
    //       // save initial application state to local storage
    //       localStorage.setItem('cookie-consent', 'true');
    //       localStorage.setItem(this.themeService.getModeStorageKey(), this.themeService.getModeStr());
    //       localStorage.setItem(this.themeService.getThemeStorageKey(), this.themeService.getThemeStr());
    //       localStorage.setItem(this.footerService.getStorageKey(), String(this.footerService.getFooterState()))
    //       this.footerService.footerState$.subscribe(x => this.footerState = x)
    //       localStorage.setItem(this.sidenavService.getStorageKey(), String(this.sidenavService.getSidenavState()));
    //     });
    // }
  }

  private activateHandsetLayout() {
    this.sidenavMode = 'over';
    this.sidenavState = false;
  }

  private deactivateHandsetLayout() {
    this.sidenavMode = 'side';
    this.sidenavState = this.desktopSidenavState;
  }

  sidenavStateToggle() {
    if (!this.breakpointStateLtMd) {
      this.sidenavService.toggleSidenavState();
    }
  }

  applyClass(className: string): void {
    if (this.currentClassName !== className) {
      document.body.classList.add(className);
      document.body.classList.remove(this.currentClassName);
      this.currentClassName = className;
    }
  }
}
