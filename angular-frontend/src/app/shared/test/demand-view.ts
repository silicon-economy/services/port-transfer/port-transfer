/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DemandView } from '../../components/demand-overview/demand-view';
import { DEMANDS } from './demands';

export const DEMANDVIEW: DemandView = new DemandView(DEMANDS[0]);