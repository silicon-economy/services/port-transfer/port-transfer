/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { ScrollRestorationService } from './scroll-restoration.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('ScrollRestorationService', () => {
  let service: ScrollRestorationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
      ],
      providers: [
        {provide: 'scrollContainerSelector', useValue: ''}
      ]
    });
    service = TestBed.inject(ScrollRestorationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
