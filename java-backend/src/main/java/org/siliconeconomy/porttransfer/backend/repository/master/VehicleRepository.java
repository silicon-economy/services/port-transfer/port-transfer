// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository.master;

import java.util.Optional;

import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.repository.BaseRepository;

/**
 * Repository for Vehicle
 *
 * @author F. König
 */
public interface VehicleRepository extends BaseRepository<Vehicle> {

    Optional<Vehicle> findByOwnerAndDesignation(Company owner, String designation);

}
