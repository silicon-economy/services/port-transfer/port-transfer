// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains functionality that runs on system startup. There should never be any
 * "outside" functionality that uses any of class or method in this package
 *
 * @author F. König
 */
package org.siliconeconomy.porttransfer.backend.startup;
