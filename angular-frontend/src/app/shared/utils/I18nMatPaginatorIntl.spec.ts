/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from "@angular/core/testing";
import { TranslateTestingModule } from "ngx-translate-testing";
import { I18nMatPaginatorIntl } from "./I18nMatPaginatorIntl";

/**
 * @author C. Schlüter
 */

const TRANSLATIONS = {
    'paginator.RANGE_PAGE_LABEL_1': "0 von {{length}}",
    'paginator.RANGE_PAGE_LABEL_2': "{{startIndex}} - {{endIndex}} von {{length}}",
};

describe('I18nMatPaginatorIntl', () => {

    let paginator: I18nMatPaginatorIntl;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            providers: [I18nMatPaginatorIntl],
            imports: [
                TranslateTestingModule.withTranslations('de', TRANSLATIONS)
            ]
        })


        paginator = TestBed.inject(I18nMatPaginatorIntl);
    });

    it('should be created', () => {
        expect(paginator).toBeTruthy();
    });

    it('should get paginator', () => {
        // act
        let result = paginator.getPaginatorIntl();
        // assert
        expect(result).toEqual(paginator);
    });

    it('should get range label with length or pagesize equal to 0', () => {
        // act
        let result = paginator["getThisRangeLabel"](0, 1, 0);
        let result2 = paginator["getThisRangeLabel"](1, 0, 1);
        // assert
        expect(result).toEqual("0 von 0");
        expect(result2).toEqual("0 von 1");
    });

    it('should get range label with length and pagesize not 0', () => {
        // act
        let result = paginator["getThisRangeLabel"](1, 3, 4);
        let result2 = paginator["getThisRangeLabel"](2, 4, 12);
        let result3 = paginator["getThisRangeLabel"](0, 4, 8);
        // assert
        expect(result).toEqual("4 - 4 von 4");
        expect(result2).toEqual("9 - 12 von 12");
        expect(result3).toEqual("1 - 4 von 8");
    });
});
