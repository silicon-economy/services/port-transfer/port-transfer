/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { DemandView } from 'src/app/components/demand-overview/demand-view';
import { ContainerLength } from 'src/app/shared/models/container-length';
import { ContainerType } from 'src/app/shared/models/container-type';
import { DemandSummary } from 'src/app/shared/models/http/backend/demand-summary';
import { DemandMatchingService } from 'src/app/shared/services/http/demand-matching.service';
import { DemandService } from 'src/app/shared/services/http/demand.service';
import { EnumTableFilter } from 'src/app/shared/utils/table-filter/enum-table-filter';
import { MatTableWithFilters } from 'src/app/shared/utils/table-filter/mat-table-with-filters';

/**
 * This component represents the table of demands and its filters and translations.
 *
 * @author C. Schlüter, F. König, S. Jankowski
 */
@Component({
  selector: 'app-demand-overview',
  styleUrls: ['./demand-overview.component.scss'],
  templateUrl: './demand-overview.component.html',
})
export class DemandOverviewComponent implements OnInit, AfterViewInit {

  public readonly displayedColumns: string[] = [
    'LU_Container_Type',
    'LU_Container_Size_Mapped',
    'LU_Weight_Tara',
    'LU_Order_Weight_Netto',
    'Order_PickUp_Location_Designation',
    'Order_DropOff_Location_Designation',
    'Order_DropOff_Earliest',
    'Order_DropOff_Latest'];
  public readonly standardColumns: string[] = [
    'LU_Container_Type',
    'LU_Container_Size_Mapped',
    'LU_Weight_Tara',
    'LU_Order_Weight_Netto',
    'Order_PickUp_Location_Designation',
    'Order_DropOff_Location_Designation'];

  @Output() selected = new EventEmitter<number>();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  origData: DemandSummary[];
  extendedData: DemandView[];
  dataSource: MatTableDataSource<DemandView> = new MatTableDataSource();

  selectedRowIndex = -1;
  selectedDemand: DemandSummary;

  // Filters
  isHidden = true;
  luContainerTypeFilter = new EnumTableFilter<ContainerType>("LU_Container_Type", ContainerType);
  luContainerSizeFilter = new EnumTableFilter<ContainerLength>("LU_Container_Size",
    { "20": ContainerLength._2, "40": ContainerLength._4, "45": ContainerLength._L });
  luWeightTaraFilter = new FormControl();
  luOrderWeightNettoFilter = new FormControl();
  orderPickUpLocationDesignationFilter = new FormControl();
  orderDropOffLocationDesignationFilter = new FormControl();
  orderDropOffEarliestFilter = new FormControl();
  orderDropOffLatestFilter = new FormControl();
  matTableFilter: MatTableWithFilters<DemandView>;

  // Services
  private demandService: DemandService;
  private matchingService: DemandMatchingService;
  private datePipe: DatePipe;

  constructor(demandService: DemandService, matchingService: DemandMatchingService, datePipe: DatePipe) {
    this.demandService = demandService;
    this.matchingService = matchingService;
    this.datePipe = datePipe;
  }


  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.setMatTableFilter();
    this.demandService.getDemands().subscribe(demands => {
      this.origData = demands;
      this.extendedData = this.origData.map(ds => new DemandView(ds));
      this.dataSource.data = this.extendedData;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  /**
   * Trigger select @EventEmitter when row is clicked, get selected @DemandSummary
   */
  select(row: DemandView) {
    this.selected.emit(row.id);
    this.selectedDemand = row.sourceSummary;
    console.log(JSON.stringify(this.selectedDemand));
    this.selectedRowIndex = row.id;
    this.matchingService.triggerMatching(Number(row.id));
  }

  private setMatTableFilter() {
    this.matTableFilter = new MatTableWithFilters(
      [
        ["LU_Weight_Tara", this.luWeightTaraFilter],
        ["LU_Order_Weight_Netto", this.luOrderWeightNettoFilter],
        ["Order_PickUp_Location_Designation", this.orderPickUpLocationDesignationFilter],
        ["Order_DropOff_Location_Designation", this.orderDropOffLocationDesignationFilter]
      ],
      [
        ["Order_DropOff_Earliest", this.orderDropOffEarliestFilter],
        ["Order_DropOff_Latest", this.orderDropOffLatestFilter]
      ],
      [this.luContainerTypeFilter, this.luContainerSizeFilter],
      this.dataSource,
      this.datePipe);
  }
}
