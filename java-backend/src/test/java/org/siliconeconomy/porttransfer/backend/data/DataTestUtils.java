// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.model.ModelTestUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DataTestUtils {

    private static final AtomicInteger COUNTER = new AtomicInteger();

    /* Master data */

    public static Address fakeAddress() {
        return new Address("Address_" + COUNTER.getAndIncrement(),
                           "Street",
                           "House",
                           "City",
                           25869,
                           "Country");
    }

    public static Company fakeCompany() {
        return new Company("Company_" + COUNTER.getAndIncrement());
    }

    public static Container fakeContainer() {
        var bic = "BICU %d 7".formatted(100_000 + COUNTER.getAndIncrement());
        return new Container(bic, ContainerType.ISO, ContainerLength.ISO_LENGTH_2, 1_000);
    }

    public static Vehicle fakeVehicle() {
        return new Vehicle(DataTestUtils.fakeCompany(),
                           "Vehicle_" + COUNTER.getAndIncrement(),
                           9_000,
                           false,
                           Modality.ROAD);
    }

    /* Process data */

    public static Capacity fakeCapacity() {
        return new Capacity(fakeVehicle(),
                            ModelTestUtils.fakeTimeWindow(),
                            ModelTestUtils.fakeConnection(),
                            false,
                            false);
    }

    public static Demand fakeDemand() {
        return new Demand(fakeLoadingUnit(),
                          fakeCompany(),
                          ModelTestUtils.fakeConnection(),
                          ModelTestUtils.fakeTimeWindow(),
                          "Demand_" + COUNTER.getAndIncrement(),
                          1);
    }

    public static LoadingUnit fakeLoadingUnit() {
        return new LoadingUnit(fakeContainer(),
                               0,
                               false,
                               false);
    }

}
