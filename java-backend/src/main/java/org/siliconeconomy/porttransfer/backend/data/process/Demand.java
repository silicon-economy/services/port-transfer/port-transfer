// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.process;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.PositiveOrZero;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.springframework.lang.NonNull;

/**
 * A demand represents a loading unit that needs to be moved
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Demand extends BaseEntity {

    /**
     * The loading unit
     */
    @NonNull
    @OneToOne
    @JoinColumn(nullable = false, updatable = false)
    private LoadingUnit loadingUnit;

    /**
     * The dispatcher that is responsible for the loading unit
     */
    @NonNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Company dispatcher;

    /**
     * The order that fulfills this demand
     */
    @OneToOne(mappedBy = Order_.DEMAND)
    @Setter
    private Order order;

    /**
     * The start and end of the transport
     */
    @NonNull
    @Embedded
    private Connection connection;

    /**
     * The time window for the arrival of the loading unit at its destination. While a loading unit
     * must not arrive outside this time window, it may be picked up beforehand - but only on the
     * same day of the earliest arrival.
     */
    @NonNull
    @Embedded
    private TimeWindow deliveryTimeWindow;

    /**
     * A text that can be used by human actors to identify or recognize the Demand. The content of
     * the designation is completely up to the user, and multiple Demands may share the same
     * designation.
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    private String designation;

    /**
     * The time in minutes needed from the start- to the endpoint. May be rounded to the nearest
     * minute
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    @PositiveOrZero
    private int estimatedTime;

}
