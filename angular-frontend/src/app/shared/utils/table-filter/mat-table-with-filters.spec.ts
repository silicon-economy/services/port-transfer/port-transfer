/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DatePipe } from "@angular/common";
import { fakeAsync, flush } from "@angular/core/testing";
import { FormControl } from "@angular/forms";
import { MatTableDataSource } from "@angular/material/table";
import { EnumTableFilter } from "./enum-table-filter";
import { MatTableWithFilters } from "./mat-table-with-filters";

/**
 * @author C. Schlüter
 */

const mockData = {
  "A": "bar",
  "Earliest": new Date(1970, 0, 1, 13, 10, 13),
  "Latest": new Date(2022, 0, 1, 13, 11, 15),
}

describe('MatTabelFilter', () => {

  let mtf: MatTableWithFilters<any>;
  let formControl: FormControl;
  let formControl2: FormControl;
  let formControl3: FormControl;
  let customFilter: EnumTableFilter<boolean>;
  let dataSource: MatTableDataSource<any>;

  beforeEach(() => {
    formControl = new FormControl();
    formControl2 = new FormControl();
    formControl3 = new FormControl();
    customFilter = new EnumTableFilter("foo", {"option.yes": true, "option.no": false});
    dataSource = new MatTableDataSource();
    mtf = new MatTableWithFilters(
      [["A", formControl]],
      [["Earliest", formControl2], ["Latest", formControl3]],
      [customFilter],
      dataSource, new DatePipe('en'));
  });

  it('should apply single filter', () => {
    mtf.applyFilter("1");
    expect(mtf.globalFilter).toEqual("1");
  });

  it('should apply a custom filter', () => {
    // Arrange
    customFilter.currentValue = false;
    // Act
    mtf.applyCustomFilter(customFilter);
    // Assert
    expect(mtf.filteredValues["foo"]).toEqual(false);
  });

  it('should react to text filter input', fakeAsync(() => {
    formControl.valueChanges.subscribe(r => {
      expect(r).toEqual('');
      expect(mtf.filteredValues['A']).toEqual('');
    });
    flush();
  }));

  it('should react to date filter input', fakeAsync(() => {
    formControl2.setValue('01.01.1970 00:00');
    formControl2.valueChanges.subscribe(r => {
      expect(r).toEqual('01.01.1970 00:00');
      expect(mtf.filteredValues['Earliest']).toEqual('01.01.1970 00:00');
    });
    flush();
  }));

  it('should define filter predicate with no global filter and run successful', () => {
    // Arrange
    mtf.globalFilter = null;
    // Act
    let filterPredicate = mtf.filterPredicate();
    let isMatched = filterPredicate(mockData, "{\"A\": \"\", \"Earliest\": \"\", \"Latest\": \"\"}");
    // Assert
    expect(isMatched).toBeTruthy();
  });

  it('should run filter predicate with a global filter that matches', () => {
    // Arrange
    mtf.applyFilter("b");
    // Act
    let filterPredicate = mtf.filterPredicate();
    let isMatched = filterPredicate(mockData, "{\"A\": \"\", \"Earliest\": \"01.01.1970 13:10:13\", \"Latest\": \"01.01.2022 13:16:16\"}");
    // Assert
    expect(isMatched).toBeTruthy();
  });

  it('should run filter predicate with a global filter not matching', () => {
    // Arrange
    mtf.globalFilter = "foooo";
    // Act
    let filterPredicate = mtf.filterPredicate();
    let isMatched = filterPredicate(mockData, "{\"A\": \"\", \"Earliest\": \"\", \"Latest\": \"\"}")
    // Assert
    expect(isMatched).toBeFalsy();
  });

  it('should run filter predicate with date filter not matching', () => {
    // Arrange
    mtf.applyFilter("b");
    // Act
    let filterPredicate = mtf.filterPredicate();
    let isMatched = filterPredicate(mockData, "{\"A\": \"\", \"Earliest\": \"01.01.1970 13:10:13\", \"Latest\": \"01.01.2022 13:10:14\"}");
    // Assert
    expect(isMatched).toBeFalsy();
  });

  it('should clear all filters', () => {
    // Arrange
    mtf.globalFilter = 'foo';
    // Act
    mtf.resetFilters();
    // Assert
    expect(mtf.globalFilter).toEqual('');
    expect(formControl2.value).toEqual('');
  });
});
