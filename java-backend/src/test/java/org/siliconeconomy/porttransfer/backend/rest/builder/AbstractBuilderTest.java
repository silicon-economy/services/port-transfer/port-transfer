// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import java.util.List;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

@UnitTest
class AbstractBuilderTest implements BuilderContract<Object> {

    /* Class under test */
    AbstractBuilder<Object> builder;

    @BeforeEach
    void beforeEach() {
        builder = new AbstractBuilder<>() {
            @Override
            protected Object buildValid() {
                return new Object();
            }
        };
    }

    @Test
    void buildValid_notCalledIfFailed() {
        // Arrange
        var builderSpy = spy(builder);
        doThrow(new EntityCreationFailedException(List.of())).when(builderSpy).checkViolations();
        // Act
        try {
            builderSpy.build();
        } catch (EntityCreationFailedException e) {
            // ignore
        }
        // Assert
        verify(builderSpy, never()).buildValid();
    }

    /* Contract methods */

    @Override
    public Builder<Object> getValidBuilder() {
        return builder;
    }

}
