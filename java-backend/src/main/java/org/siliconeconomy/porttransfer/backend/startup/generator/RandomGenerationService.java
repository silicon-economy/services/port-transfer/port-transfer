// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.util.List;
import java.util.Random;

import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.springframework.stereotype.Component;

/**
 * A specialized random value generator
 */
@Component
public class RandomGenerationService {

    private static final int TIME_WINDOW_STEP = 15;
    private static final int DRIVE_TIME_STEP = 10;

    private final GeneratorConfiguration configuration;

    private final Random random;

    public RandomGenerationService(GeneratorConfiguration configuration) {
        this.configuration = configuration;
        random = new Random();
    }

    /**
     * Creates a random duration as a multiple of {@value #TIME_WINDOW_STEP}
     * <p>
     * Tends to be bigger than {@link #getDriveTimeDurationInMinutes()}
     *
     * @return the duration
     */
    public int getTimeWindowDurationInMinutes() {
        return randomDuration() * TIME_WINDOW_STEP;
    }

    /**
     * Creates a random duration as a multiple of {@value #DRIVE_TIME_STEP}
     * <p>
     * Tends to be smaller than {@link #getTimeWindowDurationInMinutes()}
     *
     * @return the duration
     */
    public int getDriveTimeDurationInMinutes() {
        return randomDuration() * DRIVE_TIME_STEP;
    }

    private int randomDuration() {
        return random.nextInt(configuration.getMinimumCapacityDuration(),
                              configuration.getMaximumCapacityDuration() + 1);
    }

    /**
     * Chooses a random full minute between the configured
     * {@link GeneratorConfiguration#getBeginnOfDay() beginnOfDay} and
     * {@link GeneratorConfiguration#getEndOfDay() endOfDay}
     *
     * @return the minute as an offset from the beginnOfDay
     */
    public int minuteInDay() {
        return random.nextInt((configuration.getEndOfDay() - configuration.getBeginnOfDay()) * 60);
    }

    /**
     * Decides randomly whether a container is full
     *
     * @return the result
     */
    public boolean isFullContainer() {
        return random.nextInt(1, 101) <= configuration.getFullContainerProbability();
    }

    /**
     * Chooses a container type at random
     *
     * @return the selected type
     */
    public ContainerType getType() {
        return ContainerType.values()[random.nextInt(ContainerType.values().length)];
    }

    /**
     * Decides whether a vehicle should be "extra long" ( = capable of 45ft-containers)
     *
     * @return the result
     */
    public boolean isExtraLong() {
        var randomInt = random.nextInt(1, 101);
        return randomInt <= configuration.getNonStandardLengthProbability();
    }

    /**
     * Chooses a length for a container, with equal probability for 40ft and 45ft and a different
     * (higher) probability for the standard 20ft
     *
     * @return the result
     */
    public ContainerLength getLength() {
        var randomInt = random.nextInt(1, 101);
        if (randomInt <= configuration.getNonStandardLengthProbability()) {
            return ContainerLength.ISO_LENGTH_4;
        }
        if ((100 - randomInt) <= configuration.getNonStandardLengthProbability()) {
            return ContainerLength.ISO_LENGTH_L;
        }
        return ContainerLength.ISO_LENGTH_2;
    }

    /**
     * @return {@link Random#nextBoolean()}
     */
    public boolean nextBoolean() {
        return random.nextBoolean();
    }

    /**
     * Uses {@link Random} to choose an element from a list
     *
     * @param list the list
     * @param <T>  any type
     * @return the chosen element
     */
    public <T> T selectOne(List<T> list) {
        return list.get(random.nextInt(list.size()));
    }

}
