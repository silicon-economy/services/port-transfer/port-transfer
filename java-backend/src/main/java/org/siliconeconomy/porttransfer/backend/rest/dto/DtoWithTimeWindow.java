// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.time.OffsetDateTime;

/**
 * A DTO that contains information about a {@link org.siliconeconomy.porttransfer.backend.model.TimeWindow}
 *
 * @author F. König
 */
public interface DtoWithTimeWindow {

    OffsetDateTime getEarliest();

    OffsetDateTime getLatest();

}
