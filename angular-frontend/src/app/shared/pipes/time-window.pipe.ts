/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Inject, LOCALE_ID, Pipe, PipeTransform } from '@angular/core';
import { formatDate } from "@angular/common";
import { TimeWindow } from "../models/time-window";

/**
 * The TimeWindowPipe formats a TimeWindow
 *
 * The formatting follows these rules:
 * - if both are on different dates, display them both fully: "dd.MM.yyyy HH:mm - dd.MM.yyyy HH:mm"
 * - if both are on the same day, omit the date on the second Date: "dd.MM.yyyy HH:mm - HH:mm"
 *
 * @author F. König, C. Schlüter
 */
@Pipe({
  name: 'timeWindow'
})
export class TimeWindowPipe implements PipeTransform {

  private readonly locale: string;

  constructor(@Inject(LOCALE_ID) locale: string) {
    this.locale = locale;
  }

  transform(timeWindow: TimeWindow, ...args: unknown[]): string {
    if (timeWindow.from > timeWindow.to) {
      throw new Error("Can't create a time window where 'from' is after 'to'");
    }
    return this.toString(timeWindow);
  }

  private toString = ({ from, to }: TimeWindow): string => {
    let fromDate = this.toDateString(from);
    let fromTime = this.toTimeString(from);
    let toTime = this.toTimeString(to);
    if (from.toDateString() == to.toDateString()) {
      // same day -> don't repeat the date
      return `${fromDate} ${fromTime} - ${toTime}`;
    } else {
      let toDate = this.toDateString(to);
      return `${fromDate} ${fromTime} - ${toDate} ${toTime}`;
    }
  }

  private toDateString(date: Date): string {
    return formatDate(date, "dd.MM.yyyy", this.locale);
  }

  private toTimeString(date: Date): string {
    return formatDate(date, "HH:mm", this.locale);
  }

}

