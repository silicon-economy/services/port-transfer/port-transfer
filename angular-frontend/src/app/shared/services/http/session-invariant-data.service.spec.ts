import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SessionInvariantDataService } from './session-invariant-data.service';
import { environment as e } from '../../../../environments/environment'
import { SnackbarService } from '../state/snackbar.service';

const mockDoc = {
  companyDesignations: ["foo", "bar"],
  addressDesignations: ["x", "y"]
}

describe('SessionInvariantDataService', () => {
  let service: SessionInvariantDataService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SessionInvariantDataService,
        {
          provide: SnackbarService,
          useValue: {
            handleDefaultErrorsWithText: () => { }
          }
        }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(SessionInvariantDataService);
    httpController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAddressDesignations should work', fakeAsync(() => {
    service.getAddressDesignations().subscribe(r => {
      expect(r).toEqual(mockDoc.addressDesignations);
    });
    httpController.expectOne(e.apiHost + e.api + e.endPoints.sessionInvariantData).flush(mockDoc);
    flush();
  }));
  
  it('getCompanyDesignations should work', fakeAsync(() => {
    service.getCompanyDesignations().subscribe(r => {
      expect(r).toEqual(mockDoc.companyDesignations);
    });
    httpController.expectOne(e.apiHost + e.api + e.endPoints.sessionInvariantData).flush(mockDoc);
    flush();
  }));
});
