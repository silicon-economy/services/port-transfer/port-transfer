/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const NEW_DEMAND = {
    Tmp_Dispatcher: "Alpha",
    Demand_Designation: "the demand",
    Demand_EstimatedTime: 30,
    LU_Number: "BICU 123456 7",
    LU_Container_Type: "ISO",
    LU_Container_Size: "ISO_LENGTH_L",
    LU_Weight_Tara: 33,
    LU_Order_Weight_Netto: 32,
    Order_PickUp_Location_Designation: "pick-up location",
    Order_DropOff_Location_Designation: "drop-off location",
     Order_DropOff_Earliest: "2022-02-22T20:20:20+00:00",
     Order_DropOff_Latest: "2022-02-22T22:22:22+00:00",
     LU_Order_DangerousGoodsIndication: false,
     LU_Order_ReeferIndication: false
}