/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { getDateWithTime } from "./date-time-utils";


describe('DateTimeUtils', () => {

    it('should cast into Date', () => {
      // prepare
      let expectedDate = new Date(1980, 2, 1, 20, 23);
      // act
      let result = getDateWithTime("1.3.1980 20:23");
      let result2 = getDateWithTime("1.3.1980 20:23:51");
      // assert
      expect(result).toEqual(expectedDate);
      expect(result2).toEqual(expectedDate);
    });

    it('should not cast into Date', () => {
      // act
      let result = getDateWithTime(".3.1980 20:23");
      let result2 = getDateWithTime("foo");
      // assert
      expect(result).toBeUndefined();
      expect(result2).toBeUndefined();
    });
  });
