// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * Base package for the backend.
 * <p>
 * This package-info.java contains annotations that configure global null-handling
 *
 * @author F. König
 */
@NonNullApi
@NonNullFields
package org.siliconeconomy.porttransfer.backend;

import org.springframework.lang.NonNullApi;
import org.springframework.lang.NonNullFields;
