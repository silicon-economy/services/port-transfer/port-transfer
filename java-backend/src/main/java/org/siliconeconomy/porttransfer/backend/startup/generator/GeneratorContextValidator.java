// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Function;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import lombok.extern.slf4j.Slf4j;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.springframework.stereotype.Component;

/**
 * Validator that ensures that the generated values are not too nonsensical and that no existing
 * entities will be affected
 *
 * @author F. König
 */
@Slf4j
@Component
public class GeneratorContextValidator {

    private final Validator validator;

    private final GeneratorConfiguration configuration;

    private final AddressRepository addressRepository;
    private final CompanyRepository companyRepository;

    public GeneratorContextValidator(ValidatorFactory validatorFactory,
                                     GeneratorConfiguration configuration,
                                     AddressRepository addressRepository,
                                     CompanyRepository companyRepository) {
        validator = validatorFactory.getValidator();
        this.configuration = configuration;
        this.addressRepository = addressRepository;
        this.companyRepository = companyRepository;
    }

    /**
     * Checks if a configuration is possible. Contains two checks;
     * <ul>
     *     <li>
     *         checks if the configuration is sensible, e.g. that
     *         {@link GeneratorConfiguration#getBeginnOfDay() beginnOfDay} <
     *         {@link GeneratorConfiguration#getEndOfDay() endOfDay}
     *     </li>
     *     <li>
     *         checks that the database does not contain entries that would interfere with the
     *         generation
     *     </li>
     * </ul>
     *
     * @return true if generation can go ahead
     */
    public boolean canGenerate() {
        return validateFile() && validateDatabase();
    }

    private boolean validateFile() {
        var constraintViolations = validator.validate(configuration);
        if (!constraintViolations.isEmpty()) {
            for (var violation : constraintViolations) {
                log.info("Configuration constraint validation violation: {}",
                         violation.getMessage());
            }
            log.info("Will not validate further");
            return false;
        }
        var validationTracker = new AtomicBoolean(true);
        for (String terminal : configuration.getTerminals()) {
            if (terminal.isBlank()) {
                log.info("Terminals must have a name");
                validationTracker.set(false);
            }
        }
        for (String shippingCompany : configuration.getShippingCompanies()) {
            if (shippingCompany.isBlank()) {
                log.info("Shipping companies must have a name");
                validationTracker.set(false);
            }
        }
        Set<String> companies = new HashSet<>();
        companies.addAll(configuration.getTerminals());
        companies.addAll(configuration.getShippingCompanies());
        if (companies.size() < configuration.getTerminals()
                                            .size() + configuration.getShippingCompanies().size()) {
            log.info("Names (of terminals and shipping companies) must be unique");
            validationTracker.set(false);
        }
        if (configuration.getBeginnOfDay() >= configuration.getEndOfDay()) {
            log.info("Day must begin before it ends");
            validationTracker.set(false);
        }
        if (configuration.getMinimumCapacityDuration() >= configuration.getMaximumCapacityDuration()) {
            log.info("Minimum capacity duration must be smaller than maximum capacity duration");
            validationTracker.set(false);
        }
        if (configuration.getMinimumCapacityDuration() > totalQuarterHoursInADay()) {
            log.info("Minimum capacity duration must fit into a day");
            validationTracker.set(false);
        }
        if (configuration.getMaximumCapacityDuration() > (24 - configuration.getEndOfDay()) * 4) {
            log.info("Maximum capacity duration must not be able to drive over midnight");
            validationTracker.set(false);
        }
        return validationTracker.get();
    }

    private int totalQuarterHoursInADay() {
        return (configuration.getEndOfDay() - configuration.getBeginnOfDay()) * 4;
    }

    private boolean validateDatabase() {
        var validationTracker = new AtomicBoolean(true);
        checkDesignationExistence(configuration.getTerminals(), validationTracker);
        checkDesignationExistence(configuration.getShippingCompanies(), validationTracker);
        return validationTracker.get();
    }

    private void checkDesignationExistence(Collection<String> designations,
                                           AtomicBoolean validationTracker) {
        checkAddressExistence(designations, validationTracker);
        checkCompanyExistence(designations, validationTracker);
    }

    private void checkAddressExistence(Collection<String> designations,
                                       AtomicBoolean validationTracker) {
        checkExistence(designations,
                       addressRepository::findByDesignation,
                       Address::getDesignation,
                       "Address",
                       validationTracker);
    }

    private void checkCompanyExistence(Collection<String> designations,
                                       AtomicBoolean validationTracker) {
        checkExistence(designations,
                       companyRepository::findByDesignation,
                       Company::getDesignation,
                       "Company",
                       validationTracker);
    }

    private <T> void checkExistence(Collection<String> strings,
                                    Function<String, Optional<T>> find,
                                    Function<T, String> nameExtractor,
                                    String typeName,
                                    AtomicBoolean validationTracker) {
        strings.stream()
               .map(find)
               .flatMap(Optional::stream)
               .forEach(t -> {
                   log.info("{} {} already exists", typeName, nameExtractor.apply(t));
                   validationTracker.set(false);
               });
    }

}
