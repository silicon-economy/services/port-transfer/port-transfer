// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static org.siliconeconomy.porttransfer.backend.BackendConfiguration.RFC3339_DATE_TIME_PATTERN;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CONTAINER_SIZE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CONTAINER_TYPE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DEMAND_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DROP_OFF_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DROP_OFF_EARLIEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DROP_OFF_LATEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.ID;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.LU_NUMBER;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.PICK_UP_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.WEIGHT_NETTO;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.WEIGHT_TARA;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;

/**
 * A summary of a demand that may be used in an overview
 *
 * @author F. König
 */
@Value
public class DemandSummaryDto {

    /**
     * A key that identifies this demand
     */
    @NotNull
    @JsonProperty(ID)
    long id;

    /**
     * A description of the demand
     */
    @JsonProperty(DEMAND_DESIGNATION)
    String demandDesignation;

    /**
     * The BIC (or similar) identifier of the loading unit
     */
    @Pattern(regexp = "^[A-Z]{4} \\d{6} \\d$")
    @JsonProperty(LU_NUMBER)
    String luNumber;

    /**
     * The container's type
     */
    @NotNull
    @Valid
    @JsonProperty(CONTAINER_TYPE)
    ContainerType luContainerType;

    /**
     * The container's length
     */
    @NotNull
    @Valid
    @JsonProperty(CONTAINER_SIZE)
    ContainerLength luContainerSize;

    /**
     * The tara weight of the loading unit, in kg
     */
    @NotNull
    @JsonProperty(WEIGHT_TARA)
    int luWeightTara;

    /**
     * The netto weight of the demand, in kg
     */
    @NotNull
    @JsonProperty(WEIGHT_NETTO)
    int luOrderWeightNetto;

    /**
     * The designation of the demand's pickup location
     */
    @NotNull
    @JsonProperty(PICK_UP_DESIGNATION)
    String orderPickUpLocationDesignation;

    /**
     * The designation of the demand's drop off location
     */
    @NotNull
    @JsonProperty(DROP_OFF_DESIGNATION)
    String orderDropOffLocationDesignation;

    /**
     * The earliest drop off time
     */
    @NotNull
    @Valid
    @JsonProperty(DROP_OFF_EARLIEST)
    @JsonFormat(pattern = RFC3339_DATE_TIME_PATTERN)
    OffsetDateTime orderDropOffEarliest;

    /**
     * The latest drop off time
     */
    @NotNull
    @Valid
    @JsonProperty(DROP_OFF_LATEST)
    @JsonFormat(pattern = RFC3339_DATE_TIME_PATTERN)
    OffsetDateTime orderDropOffLatest;

}

