/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { ThemeService } from './theme.service';
import { ThemeName } from '../../models/theme';
import { ModeName } from '../../models/mode';

@Injectable({
  providedIn: 'root'
})
export class BlockchainModeService {

  private readonly storageKeyMode = 'blockchain-mode';
  private readonly storageKeyPreviousTheme = 'blockchain-previous';

  private blockchainMode$: ReplaySubject<boolean> = new ReplaySubject<boolean>();
  private blockchainMode: boolean;
  private lastTheme: ThemeName;
  private lastMode: ModeName;

  constructor(private themeService: ThemeService) {
    this.blockchainMode$.asObservable().subscribe(current => {
      this.blockchainMode = current;
      this.setLocalStorageMode(current);
      this.setLocalStoragePreviousTheme({theme: this.lastTheme, mode: this.lastMode});
    });
    this.lastTheme = this.getLocalStoragePreviousTheme()?.theme;
    this.lastMode = this.getLocalStoragePreviousTheme()?.mode;
    this.blockchainMode$.next(this.getLocalStorageMode());
  }

  public getBlockchainMode$(): Observable<boolean> {
    return this.blockchainMode$.asObservable();
  }

  public toggleBlockchainMode(): void {
    if (!this.blockchainMode) {
      this.lastTheme = this.themeService.getThemeName();
      this.lastMode = this.themeService.getModeName();
      this.themeService.setTheme('blockchain');
      this.themeService.setMode('light-theme');
    } else {
      this.themeService.setTheme(this.lastTheme);
      this.themeService.setMode(this.lastMode);
    }
    this.blockchainMode$.next(!this.blockchainMode);
  }

  private getLocalStorageMode(): boolean {
    return localStorage.getItem(this.storageKeyMode) == 'true';
  }

  private getLocalStoragePreviousTheme(): { theme: ThemeName, mode: ModeName } {
    return JSON.parse(localStorage.getItem(this.storageKeyPreviousTheme));
  }

  private setLocalStorageMode(state: boolean): void {
    localStorage.setItem(this.storageKeyMode, state.toString());
  }

  private setLocalStoragePreviousTheme(previous: { theme: ThemeName, mode: ModeName }): void {
    localStorage.setItem(this.storageKeyPreviousTheme, JSON.stringify(previous));
  }
}
