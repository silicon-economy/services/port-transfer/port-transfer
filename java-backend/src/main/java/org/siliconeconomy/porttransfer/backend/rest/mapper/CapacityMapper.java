// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import java.util.Objects;

import lombok.RequiredArgsConstructor;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;
import org.siliconeconomy.porttransfer.backend.rest.builder.CapacityBuilder;
import org.siliconeconomy.porttransfer.backend.rest.dto.CapacitySummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewCapacityDto;
import org.springframework.stereotype.Component;

/**
 * This class maps a {@link Capacity} to a {@link CapacitySummaryDto}
 *
 * @author F. König
 */
@Component
@RequiredArgsConstructor
public class CapacityMapper implements ConnectionMapper, TimeWindowMapper {

    private final AddressRepository addressRepository;
    private final CompanyRepository companyRepository;
    private final VehicleRepository vehicleRepository;

    public CapacitySummaryDto map(Capacity capacity) {
        return new CapacitySummaryDto(Objects.requireNonNull(capacity.getId()),
                                      capacity.getVehicle().getModality(),
                                      capacity.getVehicle().isFortyFiveFootCapable(),
                                      capacity.getVehicle().getMaxNettoWeight(),
                                      capacity.getConnection().getSource().getDesignation(),
                                      capacity.getConnection().getSink().getDesignation(),
                                      capacity.getTimeWindow().getEarliest(),
                                      capacity.getTimeWindow().getLatest());
    }

    public Capacity map(NewCapacityDto dto) throws EntityCreationFailedException {
        var builder = new CapacityBuilder();
        companyRepository.findByDesignation(dto.getOwnerDesignation())
                         .flatMap(owner -> vehicleRepository.findByOwnerAndDesignation(owner,
                                                                                       dto.getVehicleDesignation()))
                         .ifPresentOrElse(builder::setVehicle, builder::failVehicle);
        mapConnection(builder, addressRepository, dto);
        mapTimeWindow(builder, dto);
        builder.setFixed(dto.isFixed());
        return builder.build();
    }

}
