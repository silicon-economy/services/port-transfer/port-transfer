/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ContainerLength} from '../../container-length';
import {ContainerType} from '../../container-type';

/**
 * A summary of a demand that may be used in an overview
 */
export interface DemandSummary {
  /**
   * A key that identifies this demand
   */
  id: number;
  /**
   * A description of the demand
   */
  Demand_Designation?: string;
  /**
   * The BIC (or similar) identifier of the loading unit
   */
  LU_Number?: string;
  /**
   * The container's type
   */
  LU_Container_Type: ContainerType;
  /**
   * The container's length
   */
  LU_Container_Size: ContainerLength;
  /**
   * The tara weight of the loading unit, in kg
   */
  LU_Weight_Tara: number;
  /**
   * The netto weight of the demand, in kg
   */
  LU_Order_Weight_Netto: number;
  /**
   * The designation of the demand\'s pickup location
   */
  Order_PickUp_Location_Designation: string;
  /**
   * The designation of the demand\'s drop off location
   */
  Order_DropOff_Location_Designation: string;
  /**
   * The earliest time for the drop off
   */
  Order_DropOff_Earliest: Date;
  /**
   * The latest time for the drop off
   */
  Order_DropOff_Latest: Date;
}

