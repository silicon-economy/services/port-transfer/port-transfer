// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains security/authorization
 *
 * @author F. König
 */
package org.siliconeconomy.porttransfer.backend.security;
