/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TimeWindowPipe } from './time-window.pipe';

describe('TimeWindowPipe', () => {
  let pipe: TimeWindowPipe;

  beforeEach(() => {
    pipe = new TimeWindowPipe("en_GB");
  });

  it('should show both dates if they are different', function () {
    // Arrange
    let from = new Date("2022-01-01T14:00");
    let to = new Date("2022-01-02T16:30");
    let fromHour = from.getHours();
    let toHour = to.getHours();
    // Act
    let output = pipe.transform({ from, to }).split(" - ");
    // Assert
    expect(output.length).toBe(2);
    expect(output[0]).toBe("01.01.2022 " + fromHour + ":00");
    expect(output[1]).toBe("02.01.2022 " + toHour + ":30");
  });

  it('should not show both dates if they are same', function () {
    // Arrange
    let from = new Date("2022-01-01T14:00");
    let to = new Date("2022-01-01T16:30");
    let fromHour = from.getHours();
    let toHour = to.getHours();
    // Act
    let output = pipe.transform({ from, to }).split(" - ");
    // Assert
    expect(output.length).toBe(2);
    expect(output[0]).toBe("01.01.2022 " + fromHour + ":00");
    expect(output[1]).toBe(toHour + ":30");
  });

});
