// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity_;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;
import org.siliconeconomy.porttransfer.backend.rest.dto.DemandSummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewDemandDto;
import org.siliconeconomy.porttransfer.backend.service.container.ContainerException;
import org.siliconeconomy.porttransfer.backend.service.container.ContainerService;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
@UnitTest
class DemandMapperTest implements ConnectionMapperContract, TimeWindowMapperContract {

    /* Class under test */
    DemandMapper demandMapper;

    @Mock
    CompanyRepository companyRepository;
    @Mock
    AddressRepository addressRepository;
    @Mock
    ContainerService containerService;

    /* Helper constants */
    Company dispatcher = DataTestUtils.fakeCompany();
    Container container = new Container("BIC",
                                        ContainerType.ISO,
                                        ContainerLength.ISO_LENGTH_2,
                                        1_000);

    @BeforeEach
    void beforeEach() {
        demandMapper = new DemandMapper(companyRepository, addressRepository, containerService);
    }

    @Test
    void map_Demand_To_DemandSummary() {
        // Arrange
        var demand = new Demand(new LoadingUnit(container, 0, false, false),
                                dispatcher,
                                new Connection(SOURCE, SINK),
                                new TimeWindow(EARLIEST, LATEST),
                                "TheDemand",
                                1);
        ReflectionTestUtils.setField(demand, BaseEntity_.ID, 7L);
        // Act
        var dto = demandMapper.map(demand);
        // Assert
        assertThat(dto)
                .returns(7L, DemandSummaryDto::getId)
                .returns("TheDemand", DemandSummaryDto::getDemandDesignation)
                .returns("BIC", DemandSummaryDto::getLuNumber)
                .returns(ContainerType.ISO, DemandSummaryDto::getLuContainerType)
                .returns(ContainerLength.ISO_LENGTH_2, DemandSummaryDto::getLuContainerSize)
                .returns(1_000, DemandSummaryDto::getLuWeightTara)
                .returns(0, DemandSummaryDto::getLuOrderWeightNetto)
                .returns(EARLIEST, DemandSummaryDto::getOrderDropOffEarliest)
                .returns(LATEST, DemandSummaryDto::getOrderDropOffLatest)
                .returns(SOURCE.getDesignation(),
                         DemandSummaryDto::getOrderPickUpLocationDesignation)
                .returns(SINK.getDesignation(),
                         DemandSummaryDto::getOrderDropOffLocationDesignation);
    }

    @Test
    void map_NewDemand_To_Demand_successful() {
        // Arrange
        /* Repositories/Services */
        when(companyRepository.findByDesignation(dispatcher.getDesignation()))
                .thenReturn(Optional.of(dispatcher));
        setup_mapConnection_successful();
        try {
            when(containerService.findOrCreateContainer(container.getBicCode(), null, null, null))
                    .thenReturn(container);
        } catch (ContainerException e) {
            fail("Arrange threw exception!", e);
        }
        /* Test data */
        var dto = new NewDemandDto(dispatcher.getDesignation(),
                                   "TheDesignation",
                                   1,
                                   container.getBicCode(),
                                   null,
                                   null,
                                   null,
                                   0,
                                   SOURCE.getDesignation(),
                                   SINK.getDesignation(),
                                   EARLIEST,
                                   LATEST,
                                   false,
                                   true);
        // Act
        var demand = demandMapper.map(dto);
        // Assert
        assertThat(demand)
                .returns(dispatcher, Demand::getDispatcher)
                .returns("TheDesignation", Demand::getDesignation)
                .returns(1, Demand::getEstimatedTime)
                .returns(SOURCE, d -> d.getConnection().getSource())
                .returns(SINK, d -> d.getConnection().getSink())
                .returns(EARLIEST, d -> d.getDeliveryTimeWindow().getEarliest())
                .returns(LATEST, d -> d.getDeliveryTimeWindow().getLatest())

                .extracting(Demand::getLoadingUnit)
                .returns(container, LoadingUnit::getContainer)
                .returns(0, LoadingUnit::getNettoWeight)
                .returns(false, LoadingUnit::isHazard)
                .returns(true, LoadingUnit::isRefrigerated);
    }

    @Test
    void map_NewDemand_To_Demand_fail_ContainerException() {
        // Arrange
        /* Repositories/Services */
        when(companyRepository.findByDesignation(dispatcher.getDesignation()))
                .thenReturn(Optional.of(dispatcher));
        setup_mapConnection_successful();
        try {
            when(containerService.findOrCreateContainer(anyString(), any(), any(), any()))
                    .thenThrow(new ContainerException("Expected") {
                    });
        } catch (ContainerException e) {
            fail("Arrange threw exception!", e);
        }
        /* Test data */
        var dto = new NewDemandDto(dispatcher.getDesignation(),
                                   "TheDesignation",
                                   1,
                                   container.getBicCode(),
                                   null,
                                   null,
                                   null,
                                   0,
                                   SOURCE.getDesignation(),
                                   SINK.getDesignation(),
                                   EARLIEST,
                                   LATEST,
                                   false,
                                   true);
        // Assert
        assertThatThrownBy(() -> demandMapper.map(dto))
                .isInstanceOf(EntityCreationFailedException.class);
    }

    @Test
    void map_NewDemand_To_Demand_fail_noDispatcher() {
        // Arrange
        /* Repositories/Services */
        when(companyRepository.findByDesignation(dispatcher.getDesignation()))
                .thenReturn(Optional.empty());
        setup_mapConnection_successful();
        try {
            when(containerService.findOrCreateContainer(container.getBicCode(), null, null, null))
                    .thenReturn(container);
        } catch (ContainerException e) {
            fail("Arrange threw exception!", e);
        }
        /* Test data */
        var dto = new NewDemandDto(dispatcher.getDesignation(),
                                   "TheDesignation",
                                   1,
                                   container.getBicCode(),
                                   null,
                                   null,
                                   null,
                                   0,
                                   SOURCE.getDesignation(),
                                   SINK.getDesignation(),
                                   EARLIEST,
                                   LATEST,
                                   false,
                                   true);
        // Assert
        assertThatThrownBy(() -> demandMapper.map(dto))
                .isInstanceOf(EntityCreationFailedException.class);
    }

    /* Contract methods */

    @Override
    public AddressRepository getAddressRepositoryMock() {
        return addressRepository;
    }

    @Override
    public ConnectionMapper getConnectionMapper() {
        return demandMapper;
    }

    @Override
    public TimeWindowMapper getTimeWindowMapper() {
        return demandMapper;
    }

}
