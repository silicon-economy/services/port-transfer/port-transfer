/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { throwError } from 'rxjs';
import { CapacityService } from 'src/app/shared/services/http/capacity.service';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';
import { DemandCapacityPageComponent } from './demand-capacity-page.component';

describe('DemandCapacityPageComponent', () => {
  let component: DemandCapacityPageComponent;
  let fixture: ComponentFixture<DemandCapacityPageComponent>;

  let service: CapacityService;
  let snackService: SnackbarService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandCapacityPageComponent],
      imports: [
        TranslateTestingModule.withTranslations('de', {}),
        HttpClientTestingModule
      ]
    })
      .compileComponents();
    service = TestBed.inject(CapacityService);
    snackService = TestBed.inject(SnackbarService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandCapacityPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should catch Error'), (done) => {
    const snackSpy = spyOn(snackService, 'handleDefaultErrorsWithText');
    const mockCall = spyOn(service, 'getCapacities')
    .and.returnValue(throwError({status: 0, message: "test"}));
    service.getCapacities();
    expect(snackSpy).toHaveBeenCalled();
    expect(mockCall).toHaveBeenCalled();
    done();
  };
});
