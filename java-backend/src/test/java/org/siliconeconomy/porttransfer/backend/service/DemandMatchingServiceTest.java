// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service;

import java.time.OffsetDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.TestConstants;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.data.process.Order;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;

/**
 * Test for {@link DemandMatchingService}
 * <p>
 * At the core of most tests stands a {@link CapacityRepository#findByConnectionSourceAndConnectionSinkAndVehicleFortyFiveFootCapableAndFixedAndFullyBooked(Address, Address, boolean, boolean, boolean) method}
 * with a very long name, which is why this text shall refer to it as $find. $find finds all
 * {@link Capacity Capacities} with fields matching the parameters of $find. To mock $find, use
 * {@link #mockupRepository(ContainerLength, List, List)}
 */
@ExtendWith(MockitoExtension.class)
@UnitTest
class DemandMatchingServiceTest {

    /* Class under test */
    DemandMatchingService demandMatchingService;

    @Mock
    CapacityRepository capacityRepository;

    /* Helper values */
    final Address source = DataTestUtils.fakeAddress();
    final Address sink = DataTestUtils.fakeAddress();
    final OffsetDateTime startTime = TestConstants.TIME_INSTANCE;
    final OffsetDateTime endTime = startTime.plusHours(1);
    final int containerWeight = 1_000;


    @BeforeEach
    void beforeEach() {
        demandMatchingService = new DemandMatchingService(capacityRepository);
    }

    /* Tests */

    @ParameterizedTest
    @EnumSource
    void noCapacitiesNoMatch(ContainerLength length) {
        // Arrange
        mockupRepository(length, List.of(), List.of());
        // Act
        var matches = demandMatchingService.findMatching(createDemand(length));
        // Assert
        assertThat(matches)
                .isEmpty();
    }

    @Nested
    class FlexibleCapacities {

        /* Helper constants */
        private static final String FLEXIBLE_MATCHING_COMBINATIONS = """
                ContainerLength, 45ftVehicle
                #########################
                ISO_LENGTH_2   , true
                ISO_LENGTH_2   , false
                ISO_LENGTH_4   , true
                ISO_LENGTH_4   , false
                ISO_LENGTH_L   , true
                """;


        @ParameterizedTest
        @CsvSource(textBlock = FLEXIBLE_MATCHING_COMBINATIONS, useHeadersInDisplayName = true)
        void match(ContainerLength length, boolean longVehicle) {
            // Arrange
            var capacity = createCapacity(containerWeight, longVehicle, startTime, endTime);
            mockupFlexibleCapacities(length, capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand(length));
            // Assert
            assertThat(matches)
                    .isNotEmpty()
                    .containsExactly(capacity);
        }

        @Test
        void noFlexibleMatchIfTooLong() {
            // Arrange
            var capacity = createCapacity(containerWeight, false, startTime, endTime);
            mockupFlexibleCapacities(ContainerLength.ISO_LENGTH_L, capacity);
            // Act
            var matches =
                    demandMatchingService.findMatching(createDemand(ContainerLength.ISO_LENGTH_L));
            // Assert
            assertThat(matches)
                    .isEmpty();
        }

        @ParameterizedTest
        @CsvSource(textBlock = FLEXIBLE_MATCHING_COMBINATIONS, useHeadersInDisplayName = true)
        void noMatch_intersectionTooShort(ContainerLength length, boolean longVehicle) {
            // Arrange
            var capacity = createCapacity(containerWeight,
                                          longVehicle,
                                          startTime,
                                          startTime.plusMinutes(20));
            mockupFlexibleCapacities(length, capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand(length));
            // Assert
            assertThat(matches)
                    .isEmpty();
        }

        @ParameterizedTest
        @CsvSource(textBlock = FLEXIBLE_MATCHING_COMBINATIONS, useHeadersInDisplayName = true)
        void noMatch_tooHeavy(ContainerLength length, boolean longVehicle) {
            // Arrange
            var capacity = createCapacity(containerWeight - 1,
                                          longVehicle,
                                          startTime,
                                          endTime);
            mockupFlexibleCapacities(length, capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand(length));
            // Assert
            assertThat(matches)
                    .isEmpty();
        }

        /* Helper methods */

        private Capacity createCapacity(int maxWeight,
                                        boolean fortyFiveFootCapable,
                                        OffsetDateTime earliest,
                                        OffsetDateTime latest) {
            return DemandMatchingServiceTest.this.createCapacity(maxWeight,
                                                                 fortyFiveFootCapable,
                                                                 false,
                                                                 earliest,
                                                                 latest);
        }

        private void mockupFlexibleCapacities(ContainerLength length, Capacity... capacities) {
            mockupRepository(length, List.of(capacities), List.of());
        }

    }

    @Nested
    class FixedCapacities {

        @Test
        void matchFixedCapacityWithoutPriorBooking() {
            // Arrange
            var capacity = createCapacity(containerWeight, startTime, endTime);
            mockupFixedCapacities(capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand());
            // Assert
            assertThat(matches)
                    .isNotEmpty()
                    .containsExactly(capacity);
        }

        @Test
        void matchFixedCapacityWithPriorBooking() {
            // Arrange
            var capacity = createCapacity(2 * containerWeight, startTime, endTime);
            addPriorBooking(capacity);
            mockupFixedCapacities(capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand());
            // Assert
            assertThat(matches)
                    .isNotEmpty()
                    .containsExactly(capacity);
        }

        @ParameterizedTest
        @EnumSource(names = {"ISO_LENGTH_4", "ISO_LENGTH_L"})
        void skipFixedMatchingIfTooLong(ContainerLength length) {
            // Act
            demandMatchingService.findMatching(DemandMatchingServiceTest.this.createDemand(length));
            // Assert
            verify(capacityRepository, never())
                    .findByConnectionSourceAndConnectionSinkAndVehicleFortyFiveFootCapableAndFixedAndFullyBooked(
                            any(Address.class),
                            any(Address.class),
                            anyBoolean(),
                            eq(true),
                            anyBoolean());
        }

        @ParameterizedTest
        @ValueSource(ints = {-2, 2})
        void noFixedMatchIfArrivalIsOutsideTimeWindow(int offsetInHours) {
            // Arrange
            var capacity = createCapacity(containerWeight,
                                          startTime.plusHours(offsetInHours),
                                          endTime.plusHours(offsetInHours));
            mockupFixedCapacities(capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand());
            // Assert
            assertThat(matches)
                    .isEmpty();
        }

        @Test
        void noFixedMatchIfCombinedDemandsTooHeavy() {
            // Arrange
            var capacity = createCapacity(2 * containerWeight - 1, startTime, endTime);
            addPriorBooking(capacity);
            mockupFixedCapacities(capacity);
            // Act
            var matches = demandMatchingService.findMatching(createDemand());
            // Assert
            assertThat(matches)
                    .isEmpty();
        }

        /* Helper methods */

        private Demand createDemand() {
            return DemandMatchingServiceTest.this.createDemand(ContainerLength.ISO_LENGTH_2);
        }

        private Capacity createCapacity(int maxWeight,
                                        OffsetDateTime earliest,
                                        OffsetDateTime latest) {
            return DemandMatchingServiceTest.this.createCapacity(maxWeight,
                                                                 false,
                                                                 true,
                                                                 earliest,
                                                                 latest);
        }

        private void mockupFixedCapacities(Capacity... capacities) {
            mockupRepository(ContainerLength.ISO_LENGTH_2, List.of(), List.of(capacities));
        }

        private void addPriorBooking(Capacity capacity) {
            capacity.getOrders()
                    .add(new Order(capacity, createDemand()));
        }

    }

    /* Helper methods */

    private Demand createDemand(ContainerLength length) {
        return new Demand(new LoadingUnit(new Container(ContainerType.ISO,
                                                        length,
                                                        containerWeight),
                                          0,
                                          false,
                                          false),
                          DataTestUtils.fakeCompany(),
                          new Connection(source, sink),
                          new TimeWindow(startTime, endTime),
                          "Something",
                          30);
    }

    private Capacity createCapacity(int maxWeight,
                                    boolean fortyFiveFootCapable,
                                    boolean fixed,
                                    OffsetDateTime earliest,
                                    OffsetDateTime latest) {
        return new Capacity(new Vehicle(DataTestUtils.fakeCompany(),
                                        "TheVehicle",
                                        maxWeight,
                                        fortyFiveFootCapable,
                                        Modality.ROAD),
                            new TimeWindow(earliest, latest),
                            new Connection(source, sink),
                            fixed,
                            false);
    }

    private void mockupRepository(ContainerLength length,
                                  List<Capacity> flexibleCapacities,
                                  List<Capacity> fixedCapacities) {
        mockupRepository(false, flexibleCapacities);
        if (length == ContainerLength.ISO_LENGTH_2) {
            /* Other capacities won't query the "fixed" repository */
            mockupRepository(true, fixedCapacities);
        }
    }

    private void mockupRepository(boolean fixed, List<Capacity> capacities) {
        when(capacityRepository.findByConnectionSourceAndConnectionSinkAndVehicleFortyFiveFootCapableAndFixedAndFullyBooked(
                any(Address.class),
                any(Address.class),
                anyBoolean(),
                eq(fixed),
                eq(false)))
                .thenReturn(capacities);
    }

}
