/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { OperatorFunction } from "rxjs";
import { map } from "rxjs/operators";
import SubInitializer = Initializer.SubInitializer;
import EnumProperty = Initializer.EnumProperty;

/**
 * An Initializer initializes a response of the HttpClient so that the actual types match the "expected" types, i.e.
 * those provided by the corresponding interface
 *
 * @author F. König, C. Schlüter
 */
export abstract class Initializer<Type> {

  /**
   * Return a method that initializes a list of objects
   *
   * To be used as the parameter of a .pipe()-operation
   */
  public initializeList(): OperatorFunction<Type[], Type[]> {
    return map((objects: Type[]) => {
      this._initializeList(objects);
      return objects;
    });
  }

  /**
   * Return a method that initializes a single object
   *
   * To be used as the parameter of a .pipe()-operation
   */
  public initializeObject(): OperatorFunction<Type, Type> {
    return map((object: Type) => {
      this._initializeObject(object);
      return object;
    });
  }

  /**
   * Provide a list of keys where the corresponding values should be initialized as "Date"-objects
   * @protected
   */
  protected dates(): string[] {
    return [];
  }

  /**
   * Provide a list of key-Initializer pairs where the corresponding values should be initialized as an object by that
   * initializer
   * @protected
   */
  protected objects(): SubInitializer<any>[] {
    return [];
  }

  /**
   * Provide a list of key-Initializer pairs where the corresponding values should be initialized as a list by that
   * initializer
   * @protected
   */
  protected lists(): SubInitializer<any>[] {
    return [];
  }

  /**
   * Provide a list of key-enum pairs where the corresponding values should be initialized as instances of that enum
   * @protected
   */
  protected enums(): EnumProperty<any>[] {
    return [];
  }

  private _initializeList(objects: Type[]): void {
    objects.forEach(object => this._initializeObject(object));
  }

  private _initializeObject(object: Type): void {
    this.dates().forEach(key => object[key] = new Date(object[key]));
    this.enums().forEach(({ key, enum_ }) => object[key] = enum_[object[key]]);
    this.objects().forEach(({ key, initializer }) => initializer._initializeObject(object[key]));
    this.lists().forEach(({ key, initializer }) => initializer._initializeList(object[key]));
  }

}

export namespace Initializer {
  /**
   * Type of a "string"-enum
   */
  export type StringEnum<E> = Record<keyof E, string>;
  /**
   * A pair of a key and an initializer
   */
  export type SubInitializer<T> = { key: string, initializer: Initializer<T> };
  /**
   * A pair of a key and a StringEnum
   */
  export type EnumProperty<E> = { key: string, enum_: StringEnum<E> };
}
