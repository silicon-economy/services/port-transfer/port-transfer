// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.process;

import java.time.OffsetDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.springframework.lang.NonNull;

/**
 * A Note can be attached to an order to note or exchange any information.
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Note extends BaseEntity {

    /**
     * The Order this Note is related to
     */
    @NonNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Order order;

    /**
     * The Company that left this Note
     */
    @NonNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Company owner;

    /**
     * The actual content
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    private String content;

    /**
     * The creation date and time of this Note
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    private OffsetDateTime timestamp;

    /**
     * If the Note is secret, only the {@link #owner} may read it. If not, all Companies that have
     * access to the {@link #order} can read it
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    private boolean secret;

}
