// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service.container;

import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;

/**
 * Indicates that a container request contained an invalid combination of data
 *
 * @author F. König
 */
public class InvalidContainerRequestException extends ContainerException {

    public InvalidContainerRequestException(Container container,
                                            ContainerType type,
                                            ContainerLength length,
                                            Integer taraWeight) {
        super(String.format(
                "Container %s does not match Type = '%s', Length = '%s', TaraWeight = '%d'",
                container,
                type,
                length,
                taraWeight));
    }

}
