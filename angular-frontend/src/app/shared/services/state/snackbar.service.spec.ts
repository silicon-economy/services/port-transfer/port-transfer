import { TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { SnackbarService } from './snackbar.service';

/**
 * @author S. Jankowski
 */

const TRANSLATIONS = {
  '': "",
};

describe('SnackbarService', () => {
  let service: SnackbarService;

  beforeEach((() => {
    TestBed.configureTestingModule({
      imports: [TranslateTestingModule.withTranslations('de', TRANSLATIONS)]
    })
    service = TestBed.inject(SnackbarService);
  }));

  it('should have working methods', () => {
    service.getStatusMessage();
    service.getNextDuration();
    expect(service).toBeTruthy();
  });

  it('should not fail on calling handling methods', () => {
    service.handleDefaultErrorsWithText('key', {status: 0, message: "Test"});
    service.handleCreateError('key', {status: 400, message: "Test;test2"}, 'prefix');
    service.handleSuccessfulCreation('', 'key');
    service.handleMessageWithTranslateVariables('key', {}, 0);
    service.handleMessageWithTranslateVariables('key', {});
  });
});
