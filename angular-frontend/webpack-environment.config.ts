/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import * as webPack from "webpack"

/**
 * This method is called by @angular-builders/custom-webpack during build-time.
 * It has access to the environment of the process which would not be available
 * during runtime.
 */
export default (config: webPack.Configuration) => {
    config.plugins.push(
        new webPack.DefinePlugin({
            // $ENV is declared in env.d.ts and filled with the environment variables here. 
            $ENV: JSON.stringify({
                backendUrl: process.env["API_HOST"],
                keycloakUrl: process.env["KEYCLOAK_URL"],
                keycloakRealm: process.env["KEYCLOAK_REALM"],
                keycloakClient: process.env["KEYCLOAK_CLIENT"]
            })
        })
    );
    return config;
}