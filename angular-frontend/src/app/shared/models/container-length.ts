/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * The length values are modeled according to the ISO 6346 length codes. They represent in order 20ft, 40ft and 45ft
 */
export enum ContainerLength {
    _2 = "ISO_LENGTH_2",
    _4 = "ISO_LENGTH_4",
    _L = "ISO_LENGTH_L"
}

export function getContainerLength(length: ContainerLength): number {
    switch (length) {
        case ContainerLength._2: return 20;
        case ContainerLength._4: return 40;
        case ContainerLength._L: return 45;
        default: return 0;
    }
}