/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {MatTableModule} from '@angular/material/table';
import {TranslateTestingModule} from 'ngx-translate-testing';
import {Observable, of} from 'rxjs';
import {CapacitySummary} from 'src/app/shared/models/http/backend/capacity-summary';
import {CAPACITIES} from 'src/app/shared/test/capacities';
import {CapacityOverviewComponent} from './capacity-overview.component';
import {DatePipe} from '@angular/common';
import {DemandMatchingService} from 'src/app/shared/services/http/demand-matching.service';

/**
 * @author C. Schlüter
 */

class MockCapacityMatchingService {
  public getMatches(): Observable<CapacitySummary[]> {
    return of(CAPACITIES)
  }
}

describe('CapacityOverviewComponent', () => {
  let component: CapacityOverviewComponent;
  let fixture: ComponentFixture<CapacityOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        CapacityOverviewComponent
      ],
      imports: [
        MatTableModule,
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [
        DatePipe,
        {
          provide: DemandMatchingService,
          useClass: MockCapacityMatchingService
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CapacityOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should reset filters', () => {
    // Act
    component.resetFilters();
    // Assert
    expect(component.vehicleCan45ftFilter.currentValue).toBeNull();
  });
});
