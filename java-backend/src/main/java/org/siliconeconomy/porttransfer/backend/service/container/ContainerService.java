// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service.container;

import lombok.RequiredArgsConstructor;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.repository.master.ContainerRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

/**
 * This service allows fetching a known container based on a BIC or creating a new one, while
 * ensuring that the fetched container matches the expected physical information like {@link ContainerType}
 *
 * @author F. König
 */
@Service
@RequiredArgsConstructor
public class ContainerService {

    private final ContainerRepository containerRepository;

    /**
     * Finds a container based on its BIC or creates a new one
     *
     * @param bicCode    the BIC, optional
     * @param type       the type; null when and only when length and taraWeight are null
     * @param length     the length; null when and only when type and taraWeight are null
     * @param taraWeight the tara weigth; null when and only when type and length are null
     * @return a container matching the request, guaranteed to exist in the database
     * @throws ContainerException if the request could not be fulfilled
     */
    public Container findOrCreateContainer(@Nullable String bicCode,
                                           @Nullable ContainerType type,
                                           @Nullable ContainerLength length,
                                           @Nullable Integer taraWeight)
            throws ContainerException {
        if (allPresent(type, length, taraWeight)) {
            return findOrCreateWithFullInformation(bicCode, type, length, taraWeight);
        }
        if (partialPresent(type, length, taraWeight)) {
            throw new IncompleteContainerRequestException(bicCode, type, length, taraWeight);
        }
        // here: nonePresent(type, length, taraWeight) == true
        if (bicCode == null) {
            throw new IncompleteContainerRequestException();
        }
        return containerRepository.findByBicCode(bicCode)
                                  .orElseThrow(() -> new NoContainerException(bicCode));
    }

    private Container findOrCreateWithFullInformation(@Nullable String bicCode,
                                                      ContainerType type,
                                                      ContainerLength length,
                                                      int taraWeight)
            throws InvalidContainerRequestException {
        if (bicCode != null) {
            var containerOptional = containerRepository.findByBicCode(bicCode);
            if (containerOptional.isPresent()) {
                var container = containerOptional.get();
                if (container.getType() == type &&
                    container.getLength() == length &&
                    container.getTaraWeight() == taraWeight) {
                    return container;
                } else {
                    throw new InvalidContainerRequestException(container, type, length, taraWeight);
                }
            } else {
                return containerRepository.save(new Container(bicCode,
                                                              type,
                                                              length,
                                                              taraWeight));
            }
        } else {
            return containerRepository.save(new Container(type, length, taraWeight));
        }
    }

    private boolean allPresent(ContainerType type, ContainerLength length, Integer taraWeight) {
        return type != null && length != null && taraWeight != null;
    }

    private boolean nonePresent(ContainerType type, ContainerLength length, Integer taraWeight) {
        return type == null && length == null && taraWeight == null;
    }

    private boolean partialPresent(ContainerType type, ContainerLength length, Integer taraWeight) {
        return !allPresent(type, length, taraWeight) &&
               !nonePresent(type, length, taraWeight);
    }

}
