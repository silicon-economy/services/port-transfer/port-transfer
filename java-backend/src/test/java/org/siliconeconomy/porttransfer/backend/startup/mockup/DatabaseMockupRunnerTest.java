// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.mockup;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;

@UnitTest
class DatabaseMockupRunnerTest {

    @Test
    void run() {
        // Arrange
        var configuration = new MockupConfiguration();
        configuration.setEnabled(true);
        var address = DataTestUtils.fakeAddress();
        var factory = mock(MockupEntityFactory.class);
        when(factory.findOrCreateAddress(any(String.class))).thenReturn(address);
        var databaseMockupRunner = new DatabaseMockupRunner(configuration, factory);
        // Act
        databaseMockupRunner.run(null);
        // Assert
        verify(factory, atLeastOnce()).createCapacity(any(), any());
        verify(factory, atLeastOnce()).createDemand(any(), any(), any(), any());
    }

}
