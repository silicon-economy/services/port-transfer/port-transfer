/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SidenavService {

  private readonly storageKey = 'desktop-sidenav-state';

  private desktopSidenavState: boolean;
  private desktopSidenavState$: ReplaySubject<boolean> = new ReplaySubject<boolean>();

  constructor() {
    // Initialize State
    this.desktopSidenavState$.next(this.getLocalStorage());
    this.desktopSidenavState$.asObservable().subscribe(state => {
      this.desktopSidenavState = state;
      this.setLocalStorage(state);
    });
  }

  public getSidenavState$(): Observable<boolean> {
    return this.desktopSidenavState$.asObservable();
  }

  public toggleSidenavState(): void {
    this.desktopSidenavState$.next(!this.desktopSidenavState);
  }

  private getLocalStorage(): boolean {
    if (localStorage.getItem(this.storageKey) === null) {
      return true;
    }
    return localStorage.getItem(this.storageKey) === 'true';
  }

  private setLocalStorage(state: boolean) {
    localStorage.setItem(this.storageKey, state.toString());
  }
}
