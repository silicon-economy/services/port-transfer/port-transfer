// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception that indicates {@link HttpStatus#BAD_REQUEST} caused by the request not providing the
 * correct data for creating an entity
 *
 * @author F. König
 */
public class EntityCreationFailedException extends ResponseStatusException {

    public EntityCreationFailedException(List<String> errors) {
        super(HttpStatus.BAD_REQUEST, String.join(";", errors));
    }

}
