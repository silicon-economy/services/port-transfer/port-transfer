/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DemandInitializer } from "./demand-initializer";
import { DEMANDS } from "../../test/demands";

describe('DemandInitializer', function () {
  let initializer = new class extends DemandInitializer {
    dates() {
      return super.dates();
    }
  }

  it('should know the "Date" keys of a Demand', function () {
    // Arrange
    let example = DEMANDS[0];
    let expectedDateKeys = Object.getOwnPropertyNames(example).filter(key => example[key] instanceof Date);
    // Assert
    expect(initializer.dates()).toEqual(expectedDateKeys);
  });

});
