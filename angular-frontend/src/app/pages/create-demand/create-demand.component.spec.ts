/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { of } from 'rxjs';
import { DemandService } from 'src/app/shared/services/http/demand.service';
import { SessionInvariantDataService } from 'src/app/shared/services/http/session-invariant-data.service';
import { DEMANDS } from 'src/app/shared/test/demands';
import { CreateDemandComponent } from './create-demand.component';

const DEMAND_FORM_VALUE = {
  dispatcher: "D",
  designation: "Designation",
  type: "X",
  size: "1",
  tara: "2",
  netto: "3",
  pickup: "A",
  dropoff: "B",
  earliest: new Date(2022, 1, 22, 20, 20),
  latest: new Date(2022, 2, 22, 0, 0)
}

const mockData = {
  companyDesignations: ["foo", "bar"],
  addressDesignations: ["x", "y"]
}

describe('CreateDemandComponent', () => {
  let component: CreateDemandComponent;
  let fixture: ComponentFixture<CreateDemandComponent>;
  let demandService: DemandService;

  beforeEach(async () => {
    let spyDemandService = jasmine.createSpyObj("SpyDemandService", ["putDemand", "fetchDemands"]);
    spyDemandService.putDemand.and.returnValue(of(DEMANDS[0]));
    let spyInitialDataService = jasmine.createSpyObj("SpyInitialDataService", ["getCompanyDesignations", "getAddressDesignations"]);
    spyInitialDataService.getCompanyDesignations.and.returnValue(of(mockData.companyDesignations));
    spyInitialDataService.getAddressDesignations.and.returnValue(of(mockData.addressDesignations));

    await TestBed.configureTestingModule({
      declarations: [CreateDemandComponent],
      imports: [
        TranslateTestingModule.withTranslations('de', {}),
        MatAutocompleteModule
      ],
      providers: [
        {
          provide: DemandService,
          useValue: spyDemandService
        },
        {
          provide: SessionInvariantDataService,
          useValue: spyInitialDataService
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    demandService = TestBed.inject(DemandService);
    fixture = TestBed.createComponent(CreateDemandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call putDemand of demand service', () => {
    console.log(DEMAND_FORM_VALUE.earliest.toISOString())
    component.putDemand(DEMAND_FORM_VALUE);
    expect(demandService.putDemand).toHaveBeenCalled();
  });
});
