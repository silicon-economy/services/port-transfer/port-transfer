// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;

@ExtendWith(MockitoExtension.class)
@UnitTest
class GeneratorContextValidatorTest {

    @Mock
    AddressRepository addressRepository;
    @Mock
    CompanyRepository companyRepository;
    @Mock
    Validator validator;
    @Mock
    ValidatorFactory validatorFactory;

    /* Helper classes */
    GeneratorConfiguration configuration;

    /* Class under test */
    GeneratorContextValidator generatorContextValidator;

    @BeforeEach
    void beforeEach() {
        configuration = createValidConfiguration();
        when(validatorFactory.getValidator()).thenReturn(validator);
        generatorContextValidator = new GeneratorContextValidator(validatorFactory,
                                                                  configuration,
                                                                  addressRepository,
                                                                  companyRepository);
    }

    @Test
    void validate_all_successful() {
        // Act
        var output = generatorContextValidator.canGenerate();
        // Assert
        assertThat(output).isTrue();
    }

    @Nested
    class FailBy_validateFile {

        @Test
        @SuppressWarnings("unchecked") /* Set.of casts implicitly */
        void constraints_fail() {
            // Arrange
            var constraintViolation = mock(ConstraintViolation.class);
            when(constraintViolation.getMessage()).thenReturn("FakeViolation");
            when(validator.validate(any())).thenReturn(Set.of(constraintViolation));
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void invalid_terminal() {
            // Arrange
            configuration.setTerminals(List.of(""));
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void invalid_shippingCompany() {
            // Arrange
            configuration.setShippingCompanies(List.of(""));
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void duplicate_company() {
            // Arrange
            configuration.setTerminals(List.of("Duplicate"));
            configuration.setShippingCompanies(List.of("Duplicate"));
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void day_too_short() {
            // Arrange
            configuration.setBeginnOfDay(1);
            configuration.setEndOfDay(1);
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void capacityDuration_too_short() {
            // Arrange
            configuration.setMinimumCapacityDuration(2);
            configuration.setMaximumCapacityDuration(1);
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void minimumCapacityDuration_too_long() {
            // Arrange
            configuration.setBeginnOfDay(1);
            configuration.setEndOfDay(2);
            configuration.setMinimumCapacityDuration(5);
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void last_capacity_crosses_midnight() {
            // Arrange
            configuration.setEndOfDay(23);
            configuration.setMaximumCapacityDuration(5);
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

    }

    @Nested
    class FailBy_validateDatabase {

        @Test
        void existingAddress() {
            // Arrange
            var existingAddress = DataTestUtils.fakeAddress();
            when(addressRepository.findByDesignation(anyString()))
                    .thenReturn(Optional.of(existingAddress));
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

        @Test
        void existingCompany() {
            // Arrange
            var existingCompany = DataTestUtils.fakeCompany();
            when(companyRepository.findByDesignation(anyString()))
                    .thenReturn(Optional.of(existingCompany));
            // Act
            var output = generatorContextValidator.canGenerate();
            // Assert
            assertThat(output).isFalse();
        }

    }

    /* Helper methods */

    private static GeneratorConfiguration createValidConfiguration() {
        var configuration = new GeneratorConfiguration();
        configuration.setEnabled(true);
        configuration.setTerminals(List.of("Terminal"));
        configuration.setShippingCompanies(List.of("ShippingCompany"));
        configuration.setDemandMultiplier(1);
        configuration.setCapacityMultiplier(1);
        configuration.setCalendarWeek(1);
        configuration.setBeginnOfDay(8);
        configuration.setEndOfDay(20);
        configuration.setMinimumCapacityDuration(1);
        configuration.setMaximumCapacityDuration(4);
        configuration.setNonStandardLengthProbability(10);
        configuration.setFullContainerProbability(10);
        return configuration;
    }

}
