// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.model;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThat;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.siliconeconomy.porttransfer.backend.TestConstants;
import org.siliconeconomy.porttransfer.backend.UnitTest;

@UnitTest
class TimeWindowTest {

    /* Class under test */
    TimeWindow timeWindow;

    @BeforeEach
    void beforeEach() {
        timeWindow = new TimeWindow(TimePoint.START.getDateTime(), TimePoint.END.getDateTime());
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            BEFORE  , START  , 0
            END     , AFTER  , 0
            BEFORE  , AFTER  , 60
            EARLY_IN, AFTER  , 59
            BEFORE  , LATE_IN, 59
            EARLY_IN, LATE_IN, 58
            """)
    void overlap(TimePoint otherStart, TimePoint otherEnd, int expectedMinutesOfOverlap) {
        // Arrange
        var other = new TimeWindow(otherStart.getDateTime(), otherEnd.getDateTime());
        // Act
        var overlap = timeWindow.overlap(other);
        // Assert
        assertThat(overlap)
                .isEqualTo(expectedMinutesOfOverlap);
    }

    @ParameterizedTest
    @CsvSource(textBlock = """
            BEFORE  , false
            START   , true
            EARLY_IN, true
            LATE_IN , true
            END     , true
            AFTER   , false
            """)
    void contains(TimePoint timePoint, boolean isContained) {
        // Act
        var contains = timeWindow.contains(timePoint.getDateTime());
        // Assert
        assertThat(contains)
                .isEqualTo(isContained);
    }

    @RequiredArgsConstructor
    @Getter
    private enum TimePoint {
        START(TestConstants.TIME_INSTANCE),
        END(START.getDateTime().plusHours(1)),
        BEFORE(START.getDateTime().minusMinutes(1)),
        AFTER(END.getDateTime().plusMinutes(1)),
        EARLY_IN(START.getDateTime().plusMinutes(1)),
        LATE_IN(END.getDateTime().minusMinutes(1));

        private final OffsetDateTime dateTime;
    }

}
