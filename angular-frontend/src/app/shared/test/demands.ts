/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ContainerLength} from '../models/container-length';
import {ContainerType} from '../models/container-type';
import {DemandSummary} from '../models/http/backend/demand-summary';

export const DEMANDS: DemandSummary[] = [
  {
    id: 1337,
    Demand_Designation: "This Designation",
    LU_Number: "FOO1 33742 0",
    LU_Container_Type: ContainerType.Iso,
    LU_Container_Size: ContainerLength._2,
    LU_Weight_Tara: 1,
    LU_Order_Weight_Netto: 420,
    Order_PickUp_Location_Designation: "IML",
    Order_DropOff_Location_Designation: "Hafen",
    Order_DropOff_Earliest: new Date("2111-11-11T11:15:15+00:00"),
    Order_DropOff_Latest: new Date("2111-11-11T11:15:15+00:00")
  },
  {
    id: 313,
    LU_Container_Type: ContainerType.Flat,
    LU_Container_Size: ContainerLength._L,
    LU_Weight_Tara: 2,
    LU_Order_Weight_Netto: 311,
    Order_PickUp_Location_Designation: "Hafen",
    Order_DropOff_Location_Designation: "IML",
    Order_DropOff_Earliest: new Date("2111-11-11T11:11:11+00:00"),
    Order_DropOff_Latest: new Date("2111-11-11T11:11:11+00:00")
  }
];
