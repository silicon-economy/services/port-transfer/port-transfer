// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import org.siliconeconomy.porttransfer.backend.data.master.Address;

/**
 * A Builder that is able to build a
 * {@link org.siliconeconomy.porttransfer.backend.model.Connection}
 *
 * @param <Entity> the entity to be build
 * @author F. König
 */
public interface ConnectionBuilder<Entity> extends Builder<Entity> {

    void setSource(Address address);

    void failSource();

    void setSink(Address address);

    void failSink();

}
