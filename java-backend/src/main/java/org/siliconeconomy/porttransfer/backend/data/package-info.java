// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains the entity classes that represent database tables
 * <p>
 * Names and designations may be chosen according to DIN-SPEC-91073 where applicable
 *
 * @author F. König
 */
package org.siliconeconomy.porttransfer.backend.data;
