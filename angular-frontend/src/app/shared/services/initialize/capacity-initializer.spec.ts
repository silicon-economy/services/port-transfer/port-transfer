/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CAPACITIES } from "../../test/capacities";
import { CapacityInitializer } from "./capacity-intializer";

describe('CapacityInitializer', function () {
  let initializer = new class extends CapacityInitializer {
    dates() {
      return super.dates();
    }
  }

  it('should know the "Date" keys', function () {
    // Arrange
    let example = CAPACITIES[0];
    let expectedDateKeys = Object.getOwnPropertyNames(example).filter(key => example[key] instanceof Date);
    // Assert
    expect(initializer.dates()).toEqual(expectedDateKeys);
  });

});
