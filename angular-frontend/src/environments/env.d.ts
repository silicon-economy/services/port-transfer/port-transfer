/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * This constant is filled in webpack-environment.config.ts
 */
declare const $ENV: Env;

interface Env {
    backendUrl: string;
    keycloakUrl: string;
    keycloakRealm: string;
    keycloakClient: string;
}