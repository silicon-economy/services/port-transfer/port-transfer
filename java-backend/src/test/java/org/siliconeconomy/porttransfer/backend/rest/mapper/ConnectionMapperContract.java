// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.rest.builder.ConnectionBuilder;
import org.siliconeconomy.porttransfer.backend.rest.dto.DtoWithConnection;

interface ConnectionMapperContract {

    /* Class under test */
    ConnectionMapper getConnectionMapper();

    /* Helper constants */
    Address SOURCE = DataTestUtils.fakeAddress();
    Address SINK = DataTestUtils.fakeAddress();

    /* Helper classes */
    AddressRepository getAddressRepositoryMock();

    @Test
    default void mapConnection_successful() {
        // Arrange
        /* Repositories/Services */
        var builder = mock(ConnectionBuilder.class);
        var repository = getAddressRepositoryMock();
        setup_mapConnection_successful();
        /* Test data */
        var dto = new DtoWithConnection() {
            @Override
            public String getSource() {
                return SOURCE.getDesignation();
            }

            @Override
            public String getSink() {
                return SINK.getDesignation();
            }
        };
        // Act
        getConnectionMapper().mapConnection(builder, repository, dto);
        // Assert
        verify(builder).setSource(SOURCE);
        verify(builder).setSink(SINK);

        verify(builder, never()).failSource();
        verify(builder, never()).failSink();
    }

    @Test
    default void mapConnection_fail_noSource() {
        // Arrange
        /* Repositories/Services */
        var builder = mock(ConnectionBuilder.class);
        var repository = getAddressRepositoryMock();
        when(repository.findByDesignation(SOURCE.getDesignation()))
                .thenReturn(Optional.empty());
        when(repository.findByDesignation(SINK.getDesignation()))
                .thenReturn(Optional.of(SINK));
        /* Test data */
        var dto = new DtoWithConnection() {
            @Override
            public String getSource() {
                return SOURCE.getDesignation();
            }

            @Override
            public String getSink() {
                return SINK.getDesignation();
            }
        };
        // Act
        getConnectionMapper().mapConnection(builder, repository, dto);
        // Assert
        verify(builder).failSource();
        verify(builder).setSink(SINK);

        verify(builder, never()).setSource(SOURCE);
        verify(builder, never()).failSink();
    }

    @Test
    default void mapConnection_fail_noSink() {
        // Arrange
        /* Repositories/Services */
        var builder = mock(ConnectionBuilder.class);
        var repository = getAddressRepositoryMock();
        when(repository.findByDesignation(SOURCE.getDesignation()))
                .thenReturn(Optional.of(SOURCE));
        when(repository.findByDesignation(SINK.getDesignation()))
                .thenReturn(Optional.empty());
        /* Test data */
        var dto = new DtoWithConnection() {
            @Override
            public String getSource() {
                return SOURCE.getDesignation();
            }

            @Override
            public String getSink() {
                return SINK.getDesignation();
            }
        };
        // Act
        getConnectionMapper().mapConnection(builder, repository, dto);
        // Assert
        verify(builder).setSource(SOURCE);
        verify(builder).failSink();

        verify(builder, never()).failSource();
        verify(builder, never()).setSink(SINK);
    }

    /* Helper methods */

    /**
     * Implementations can use this method to prevent their tests from failing because of the
     * {@link ConnectionMapper#mapConnection(ConnectionBuilder, AddressRepository, DtoWithConnection) mapConnection}
     * constraints
     *
     * @see #mapConnection_successful()
     */
    default void setup_mapConnection_successful() {
        when(getAddressRepositoryMock().findByDesignation(SOURCE.getDesignation()))
                .thenReturn(Optional.of(SOURCE));
        when(getAddressRepositoryMock().findByDesignation(SINK.getDesignation()))
                .thenReturn(Optional.of(SINK));
    }

}
