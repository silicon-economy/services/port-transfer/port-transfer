/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { KeycloakService } from "keycloak-angular";
import { initializeKeycloak } from "./keycloak-init.factory";

describe('initializeKeycloak-factory', () => {
  let factory: () => Promise<boolean>;
  let keycloakServiceMock: jasmine.SpyObj<KeycloakService>;

  beforeEach(async () => {
    keycloakServiceMock = jasmine.createSpyObj('KeycloakService', ['init']);
    keycloakServiceMock.init.and.returnValue(Promise.resolve(true));
    factory = initializeKeycloak(keycloakServiceMock);
  });

  it('initializes keycloak', async () => {
    // Act
    await factory();
    // Assert
    expect(keycloakServiceMock.init).toHaveBeenCalled();
  });

});
