// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.model;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import javax.persistence.Embeddable;
import javax.persistence.Transient;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.lang.NonNull;

/**
 * Embeddable data containing two time stamps
 *
 * @author F. König
 */
@Embeddable
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Setter(AccessLevel.PROTECTED)
public class TimeWindow {

    /**
     * The beginning of the time window, inclusive
     */
    @NonNull
    private OffsetDateTime earliest;

    /**
     * The end of the time window, inclusive
     */
    @NonNull
    private OffsetDateTime latest;

    /**
     * Counts the full minutes of overlap with another time window
     * <p>
     * The value will be rounded down, i.e. an overlap of 119 seconds will return 1 minute
     *
     * @param other another time window
     * @return the number of full minutes of overlap
     */
    @Transient
    public int overlap(TimeWindow other) {
        OffsetDateTime overlapStart = Collections.max(List.of(this.earliest, other.earliest));
        OffsetDateTime overlapEnd = Collections.min(List.of(this.latest, other.latest));
        if (overlapStart.isAfter(overlapEnd)) {
            return 0;
        }
        return (int) overlapStart.until(overlapEnd, ChronoUnit.MINUTES);
    }

    /**
     * Checks if a given point in time is within the time window
     *
     * @param timePoint the point in time
     * @return whether the point is within this time window
     */
    @Transient
    public boolean contains(OffsetDateTime timePoint) {
        return !earliest.isAfter(timePoint) && !latest.isBefore(timePoint);
    }

}
