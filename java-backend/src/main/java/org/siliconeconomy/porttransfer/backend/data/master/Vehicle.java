// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Positive;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.springframework.lang.NonNull;

/**
 * Information about a physical vehicle indicating what kinds of {@link Container} this vehicle can
 * transport
 *
 * @author F. König
 */
@Entity
@Table(
        uniqueConstraints = @UniqueConstraint(columnNames = {Vehicle_.OWNER, Vehicle_.DESIGNATION})
)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Vehicle extends BaseEntity {

    /**
     * The Company owning of this vehicle. Only users of that company may use the vehicle
     */
    @NonNull
    @ManyToOne
    @JoinColumn(updatable = false, nullable = false, name = Vehicle_.OWNER)
    private Company owner;

    /**
     * The designation of the vehicle. The owner can use any naming scheme, but must keep the
     * designations unique (for the owner).
     */
    @NonNull
    @Column(updatable = false, nullable = false, name = Vehicle_.DESIGNATION)
    private String designation;

    /**
     * The maximum weight that can be transported
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    @Positive
    private int maxNettoWeight;

    /**
     * Indicates if this vehicle can carry containers with a length of 45ft
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private boolean fortyFiveFootCapable;

    /**
     * The modality by which the vehicle travels.
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private Modality modality;

    /**
     * Get the full designation, that is the {@link #owner owner's} designation combined with the
     * vehicles {@link #designation}
     *
     * @return a designation of the form "company:vehicle"
     */
    public String getFullDesignation() {
        return owner.getDesignation() + ":" + getDesignation();
    }

}
