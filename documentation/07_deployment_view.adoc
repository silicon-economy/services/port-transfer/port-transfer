[[section-deployment-view]]
== Deployment View

Software does not run without hardware.
This view describes the deployment of the Port Transfer service.
It describes the technical infrastructure used to execute the Port Transfer service, with infrastructure elements like clouds, computing nodes, and net topologies as well as other infrastructure elements and the mapping/installation of compiled software building block instances to that infrastructure elements.

=== General Deployment Decisions

The Port Transfer service is part of an SE Platform Instance (i.e a K8s Cluster instance).
In such an SE Platform Instance the following number of components may be deployed:

*Components of an SE Platform*

. One or more SE services → Project overview, and SE Reference Architecture
. One or more IDS Connectors → IDS
. One Logistics Broker → Logistics Broker
. IoT Broker (optional) → IoT Broker
. Blockchain Full-Node (optional) → Blockchain Broker

*An SE platform is*

. the environment of SE services
. Location does not "matter" (e.g., enterprise IT environment, a cloud environment, ...)

=== Deployment of the Port Transfer service

The Port Transfer services sub-components are deployed separately as containers into a Kubernetes cluster used as execution environment.
Each sub component is configured via Kubernetes resources (i.e. YAML files) which are deployed alongside the sub component container.

By design, those Port Transfer service sub-component deployments may be scaled to an arbitrary number of instances to increase performance as well as redundancy.
However, true redundancy can only be gained if the underlying execution platform comprises multiple independent failure domains such as multiple machines, data centers or geographic locations and sub component deployments span multiple such failure domains.

==== Java-Backend

* Maven-build java service
* Deployed manually by operator or as a part of CI/CD-Pipeline
* Loads DBMS-schema with build (and optional initial data)
* Type of build can be controlled through `--spring.profiles.active` flag +
for Details see the link:../java-backend/README.md[README], Section "Spring Profiles"

Further parameters, e.g. to change the database, can be adjusted in application.properties file or via environment variables and runtime arguments.

==== H2-DBMS

* In-memory-DBMS used as storage for PortTransfer-Backend
* Used when corresponding profile is active
* Schema is loaded and controlled through backend
* No deployment necessary

==== postgres-DBMS

* DBMS used as storage for PortTransfer-Backend
* Preferred DBMS for a production environment
* Schema is loaded and controlled through backend
* Deployed manually by operator

==== PortTransfer-Frontend

* Angular web service
* Deployed manually by operator or as a part of CI/CD-Pipeline
* Type of build can be controlled through flag:
** --test
*** Used as default configuration
*** Outgoing requests target local instances through localhost
** --prod
*** Outgoing requests target deployed instances on cluster
** --staging
*** Outgoing requests target staging instances on cluster

