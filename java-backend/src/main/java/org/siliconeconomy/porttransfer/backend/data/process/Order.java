// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.process;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import static org.siliconeconomy.porttransfer.backend.data.process.Order.TABLE_NAME;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.springframework.lang.NonNull;

/**
 * An Order matches a Capacity to a Demand. This fulfills that Demand, and usually uses the Capacity
 * up
 *
 * @author F. König
 */
@Entity
@Table(name = TABLE_NAME)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Order extends BaseEntity {

    public static final String TABLE_NAME = "ORDERS";

    /**
     * The Capacity that is used
     */
    @NonNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Capacity capacity;

    /**
     * The Demand that is fulfilled
     */
    @NonNull
    @OneToOne
    @JoinColumn(nullable = false, updatable = false)
    private Demand demand;

}
