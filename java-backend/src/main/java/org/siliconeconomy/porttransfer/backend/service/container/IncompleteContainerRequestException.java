// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service.container;

import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.springframework.lang.Nullable;

/**
 * Indicates that a container request had incomplete data
 *
 * @author F. König
 */
public class IncompleteContainerRequestException extends ContainerException {

    public IncompleteContainerRequestException(@Nullable String bicCode,
                                               @Nullable ContainerType type,
                                               @Nullable ContainerLength length,
                                               @Nullable Integer taraWeight) {
        super(String.format(
                "Incomplete container information: BIC: '%s', Type = '%s', Length = '%s', TaraWeight = '%d'",
                bicCode,
                type,
                length,
                taraWeight));
    }

    public IncompleteContainerRequestException() {
        super("No BIC-Code and no container information");
    }

}
