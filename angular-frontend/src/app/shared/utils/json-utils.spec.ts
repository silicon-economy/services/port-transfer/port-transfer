/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { JsonUtils } from "./json-utils";


const mockDoc = {
  "0": "FOO",
  "1": false,
  "2": 12,
}

const mockDoc2 = {
  "foo": "bar"
}
const expectedMockDoc2 = {
  "foo": "bar",
  "A": "",
  "B": ""
}

const expectedMap: Map<string, any> = new Map<string, any>([["0", "FOO"], ["1", false], ["2", 12]]);
const expectedNumberMap: Map<number, any> = new Map<number, any>([[0, "FOO"], [1, false], [2, 12]]);

describe('JsonUtils', () => {
  it('should cast from Json to Map<string, any>', () => {
    // Act
    var result = JsonUtils.castJsonToMap(mockDoc);
    // Assert
    expect(result).toEqual(expectedMap);
  });

  it('should cast from Json to Map<number, any>', () => {
    // Act
    var result = JsonUtils.castJsonToNumberMap(mockDoc);
    // Assert
    expect(result).toEqual(expectedNumberMap);
  });

  it('should add keys to a given json object', () => {
    // Act
    var result = JsonUtils.addKeys(mockDoc2, ["A", "B"]);
    // Assert
    expect(result).toEqual(expectedMockDoc2);
  });

  it('should check a string match in the given json object', () => {
    // Act
    let trueResult = JsonUtils.checkStringMatch(mockDoc2, "foo", "bar");
    let falseResult = JsonUtils.checkStringMatch(mockDoc2, "bar", "bar");
    let falseResult2 = JsonUtils.checkStringMatch(mockDoc2, "foo", "foo");
    // Assert
    expect(trueResult).toBeTruthy();
    expect(falseResult).toBeFalsy();
    expect(falseResult2).toBeFalsy();
  });
});