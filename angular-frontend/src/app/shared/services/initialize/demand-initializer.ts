/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Initializer } from "./initializer";
import { DemandSummary } from "../../models/http/backend/demand-summary";

/**
 * An initializer for a Demand
 *
 * @author F. König, C. Schlüter
 */
export class DemandInitializer extends Initializer<DemandSummary> {

  protected dates(): string[] {
    return ["Order_DropOff_Earliest", "Order_DropOff_Latest"];
  }

}
