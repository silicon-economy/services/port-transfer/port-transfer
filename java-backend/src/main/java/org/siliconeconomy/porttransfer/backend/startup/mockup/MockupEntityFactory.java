// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.mockup;

import java.time.OffsetDateTime;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.ContainerRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.LoadingUnitRepository;
import org.springframework.stereotype.Component;

/**
 * This factory can create or update various entities in the database
 *
 * @author F. König
 */
@Slf4j
@Component
@AllArgsConstructor
class MockupEntityFactory {

    private final AddressRepository addressRepository;
    private final CompanyRepository companyRepository;
    private final ContainerRepository containerRepository;
    private final VehicleRepository vehicleRepository;
    private final CapacityRepository capacityRepository;
    private final DemandRepository demandRepository;
    private final LoadingUnitRepository loadingUnitRepository;

    public Address findOrCreateAddress(String designation) {
        return addressRepository.findByDesignation(designation)
                                .orElseGet(() -> createAddress(designation));
    }

    private Address createAddress(String designation) {
        var address = new Address(designation,
                                  "Joseph-von-Fraunhofer Str",
                                  "2-4",
                                  "Dortmund",
                                  44227,
                                  "Deutschland");
        return addressRepository.save(address);
    }

    public Company findOrCreateCompany(String designation) {
        return companyRepository.findByDesignation(designation)
                                .orElseGet(() -> createCompany(designation));
    }

    private Company createCompany(String designation) {
        var company = new Company(designation);
        return companyRepository.save(company);
    }

    public Container createContainer(ContainerLength length) {
        var container = new Container(ContainerType.ISO, length, 42);
        return containerRepository.save(container);
    }

    public Vehicle findOrCreateVehicle(Company owner,
                                       String designation,
                                       boolean extraLong) {
        return vehicleRepository.findByOwnerAndDesignation(owner, designation)
                                .map(vehicle -> {
                                    if (extraLong != vehicle.isFortyFiveFootCapable()) {
                                        log.info(
                                                "The found vehicle {} does not have expected extra length '{}'",
                                                vehicle,
                                                extraLong);
                                    }
                                    return vehicle;
                                }).orElseGet(() -> createVehicle(owner, designation, extraLong));
    }

    private Vehicle createVehicle(Company owner, String designation, boolean extraLong) {
        var vehicle = new Vehicle(owner,
                                  designation,
                                  9001,
                                  extraLong,
                                  Modality.ROAD);
        return vehicleRepository.save(vehicle);
    }

    public LoadingUnit createLoadingUnit(Container container) {
        var loadingUnit = new LoadingUnit(container, 1337, false, false);
        return loadingUnitRepository.save(loadingUnit);
    }

    public void createCapacity(Vehicle vehicle, Connection connection) {
        var capacity = new Capacity(vehicle,
                                    createTimeWindow(),
                                    connection,
                                    false,
                                    false);
        capacityRepository.save(capacity);
    }

    public void createDemand(String designation,
                             LoadingUnit loadingUnit,
                             Company dispatcher,
                             Connection connection) {
        var demand = new Demand(loadingUnit,
                                dispatcher,
                                connection,
                                createTimeWindow(),
                                designation,
                                1);
        demandRepository.save(demand);
    }

    private TimeWindow createTimeWindow() {
        return new TimeWindow(OffsetDateTime.now(), OffsetDateTime.now().plusHours(2));
    }

}
