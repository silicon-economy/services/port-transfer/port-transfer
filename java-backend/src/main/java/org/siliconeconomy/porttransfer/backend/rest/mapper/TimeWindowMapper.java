// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import org.siliconeconomy.porttransfer.backend.rest.builder.TimeWindowBuilder;
import org.siliconeconomy.porttransfer.backend.rest.dto.DtoWithTimeWindow;

/**
 * Extends a mapper with the ability to map a {@link org.siliconeconomy.porttransfer.backend.model.TimeWindow}
 *
 * @author F. König
 */
public interface TimeWindowMapper {

    /**
     * Maps the time window.
     * <p>
     * Will fail the builder if the time window is negative
     *
     * @param builder the builder
     * @param dto     the incoming dto
     * @see org.siliconeconomy.porttransfer.backend.rest.builder.Builder#fail(String)
     */
    default void mapTimeWindow(TimeWindowBuilder<?> builder, DtoWithTimeWindow dto) {
        if (dto.getLatest().isAfter(dto.getEarliest())) {
            builder.setEarliest(dto.getEarliest());
            builder.setLatest(dto.getLatest());
        } else {
            builder.failEarliest();
            builder.failLatest();
        }
    }

}
