/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {CapacitySummary} from '../models/http/backend/capacity-summary';
import {Modality} from '../models/modality';

export const CAPACITIES: CapacitySummary[] = [
  {
    id: 1337,
    Vehicle_Modality: Modality.Road,
    Vehicle_Can_45ft: true,
    Vehicle_Weight_Netto_Max: 32,
    Capacity_Origin_Location_Designation: "IML",
    Capacity_Target_Location_Designation: "ISST",
    Capacity_Earliest: new Date("2111-11-11T22:25:00+00:00"),
    Capacity_Latest: new Date("2111-11-11T22:25:00+00:00")
  },
  {
    id: 313,
    Vehicle_Modality: Modality.Rail,
    Vehicle_Can_45ft: false,
    Vehicle_Weight_Netto_Max: 128,
    Capacity_Origin_Location_Designation: "Entenhausen",
    Capacity_Target_Location_Designation: "Duckburg",
    Capacity_Earliest: new Date("2111-11-11T22:22+00:00"),
    Capacity_Latest: new Date("2111-11-11T22:22+00:00")
  }
];
