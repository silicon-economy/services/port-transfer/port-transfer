/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Modality} from '../../modality';

/**
 * A summary of a capacity that may be used in an overview
 */
export interface CapacitySummary {
  /**
   * A key that identifies this capacity
   */
  id: number;
  /**
   * Modality of the vehicle
   */
  Vehicle_Modality: Modality;
  /**
   * Whether the vehicle can transport oversized (45ft) containers
   */
  Vehicle_Can_45ft: boolean;
  /**
   * The maximal weight (in kg) the vehicle can be loaded with
   */
  Vehicle_Weight_Netto_Max: number;
  /**
   * The origin of the capacity
   */
  Capacity_Origin_Location_Designation: string;
  /**
   * The target of the capacity
   */
  Capacity_Target_Location_Designation: string;
  /**
   * The earliest time the capacity is available
   */
  Capacity_Earliest: Date;
  /**
   * The latest time the capacity is available
   */
  Capacity_Latest: Date;
}

