// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.ModelTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.RepositoryTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.ContainerRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.LoadingUnitRepository;

@ExtendWith(MockitoExtension.class)
@UnitTest
class GeneratorEntityFactoryTest {

    @Mock
    AddressRepository addressRepository;
    @Mock
    CompanyRepository companyRepository;
    @Mock
    ContainerRepository containerRepository;
    @Mock
    VehicleRepository vehicleRepository;
    @Mock
    LoadingUnitRepository loadingUnitRepository;
    @Mock
    CapacityRepository capacityRepository;
    @Mock
    DemandRepository demandRepository;

    /* Class under test */
    GeneratorEntityFactory generatorEntityFactory;

    @BeforeEach
    void beforeEach() {
        generatorEntityFactory = new GeneratorEntityFactory(addressRepository,
                                                            companyRepository,
                                                            containerRepository,
                                                            vehicleRepository,
                                                            loadingUnitRepository,
                                                            capacityRepository,
                                                            demandRepository);
    }

    @Test
    void createCompany() {
        // Arrange
        RepositoryTestUtils.returnEntityOnSave(companyRepository, addressRepository);
        // Act
        var company = generatorEntityFactory.createCompany("TheCompany", "TheNumber");
        // Assert
        verify(companyRepository).save(company);
        verify(addressRepository).save(company.getAddress());
        assertThat(company)
                .returns("TheCompany", Company::getDesignation)
                .extracting(Company::getAddress)
                .returns("TheCompany", Address::getDesignation)
                .returns("TheNumber", Address::getHouseNumber);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void createVehicle(boolean extraLong) {
        // Arrange
        RepositoryTestUtils.returnEntityOnSave(vehicleRepository);
        var company = DataTestUtils.fakeCompany();
        // Act
        var vehicle = generatorEntityFactory.createVehicle(company, "TheVehicle", extraLong);
        // Assert
        verify(vehicleRepository).save(vehicle);
        assertThat(vehicle)
                .returns(company, Vehicle::getOwner)
                .returns("TheVehicle", Vehicle::getDesignation)
                .returns(extraLong, Vehicle::isFortyFiveFootCapable);
    }

    @ParameterizedTest
    @MethodSource
    void createDemand(ContainerType type, ContainerLength length, boolean isEmpty) {
        // Arrange
        var dispatcher = DataTestUtils.fakeCompany();
        dispatcher.setAddress(DataTestUtils.fakeAddress());
        var target = DataTestUtils.fakeAddress();
        var timeWindow = ModelTestUtils.fakeTimeWindow();
        RepositoryTestUtils.returnEntityOnSave(containerRepository, loadingUnitRepository);
        // Act
        generatorEntityFactory.createDemand(type,
                                            length,
                                            isEmpty,
                                            dispatcher,
                                            target,
                                            timeWindow,
                                            1);
        // Assert
        var captor = ArgumentCaptor.forClass(Demand.class);
        verify(demandRepository, atLeastOnce()).save(captor.capture());
        assertThat(captor.getValue())
                .returns(dispatcher, Demand::getDispatcher)
                .returns(timeWindow, Demand::getDeliveryTimeWindow)
                .returns(1, Demand::getEstimatedTime)
                .returns(target, d -> d.getConnection().getSink())
                .extracting(Demand::getLoadingUnit)
                .returns(isEmpty, lu -> lu.getNettoWeight() == 0)
                .extracting(LoadingUnit::getContainer)
                .returns(type, Container::getType)
                .returns(length, Container::getLength);
    }

    @Test
    void createCapacity() {
        // Arrange
        var vehicle = DataTestUtils.fakeVehicle();
        var timeWindow = ModelTestUtils.fakeTimeWindow();
        var connection = ModelTestUtils.fakeConnection();
        // Act
        generatorEntityFactory.createCapacity(vehicle,
                                              timeWindow,
                                              connection);
        // Assert
        var captor = ArgumentCaptor.forClass(Capacity.class);
        verify(capacityRepository, atLeastOnce()).save(captor.capture());
        assertThat(captor.getValue())
                .returns(vehicle, Capacity::getVehicle)
                .returns(timeWindow, Capacity::getTimeWindow)
                .returns(connection, Capacity::getConnection);
    }

    /* Method sources */

    static List<Arguments> createDemand() {
        List<Arguments> arguments = new LinkedList<>();
        for (ContainerType type : ContainerType.values()) {
            for (ContainerLength length : ContainerLength.values()) {
                arguments.add(Arguments.of(type, length, true));
                arguments.add(Arguments.of(type, length, false));
            }
        }
        return arguments;
    }

}
