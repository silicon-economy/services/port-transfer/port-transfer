// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

@UnitTest
class CapacityBuilderTest
        implements BuilderContract<Capacity>, ConnectionBuilderContract, TimeWindowBuilderContract {

    /* Class under test */
    CapacityBuilder capacityBuilder;

    /* Helper constants */
    Vehicle vehicle = DataTestUtils.fakeVehicle();
    boolean fixed = false;

    @BeforeEach
    void beforeEach() {
        capacityBuilder = new CapacityBuilder();
        // Set values that can not "fail*()"
        capacityBuilder.setFixed(fixed);
    }

    @Test
    void buildsCorrectly() {
        // Arrange
        capacityBuilder.setVehicle(vehicle);
        capacityBuilder.setSource(SOURCE);
        capacityBuilder.setSink(SINK);
        capacityBuilder.setEarliest(EARLIEST);
        capacityBuilder.setLatest(LATEST);
        // Act
        var capacity = capacityBuilder.buildValid();
        // Assert
        assertThat(capacity)
                .returns(vehicle, Capacity::getVehicle)
                .returns(SOURCE, c -> c.getConnection().getSource())
                .returns(SINK, c -> c.getConnection().getSink())
                .returns(EARLIEST, c -> c.getTimeWindow().getEarliest())
                .returns(LATEST, c -> c.getTimeWindow().getLatest())
                .returns(fixed, Capacity::isFixed)
                .returns(false, Capacity::isFullyBooked);
    }

    @Test
    void throwsIfFailedByVehicle() {
        // Arrange
        capacityBuilder.setSource(SOURCE);
        capacityBuilder.setSink(SINK);
        capacityBuilder.setEarliest(EARLIEST);
        capacityBuilder.setLatest(LATEST);
        // Act
        capacityBuilder.failVehicle();
        // Assert
        assertThatThrownBy(capacityBuilder::build)
                .isInstanceOf(EntityCreationFailedException.class);
    }

    /* Contract methods */

    @Override
    public Builder<Capacity> getValidBuilder() {
        capacityBuilder.setVehicle(vehicle);
        capacityBuilder.setSource(SOURCE);
        capacityBuilder.setSink(SINK);
        capacityBuilder.setEarliest(EARLIEST);
        capacityBuilder.setLatest(LATEST);
        return capacityBuilder;
    }

    @Override
    public CapacityBuilder getValidBuilderAwaitingTimeWindow() {
        capacityBuilder.setVehicle(vehicle);
        capacityBuilder.setSource(SOURCE);
        capacityBuilder.setSink(SINK);
        return capacityBuilder;
    }

    @Override
    public ConnectionBuilder<?> getValidBuilderAwaitingConnection() {
        capacityBuilder.setVehicle(vehicle);
        capacityBuilder.setEarliest(EARLIEST);
        capacityBuilder.setLatest(LATEST);
        return capacityBuilder;
    }

}
