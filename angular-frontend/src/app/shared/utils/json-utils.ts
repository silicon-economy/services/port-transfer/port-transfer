/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * @author C. Schlüter, J. Pixberg
 */

export abstract class JsonUtils {
  /**
   * Adds the given keys to a Json object with no content (empty string).
   *
   * @param jsonObject A Json object
   * @param keys The keys that should be added to the given Json object
   */
  public static addKeys(jsonObject: Object, keys: string[]): Object {
    keys.forEach(key => {
      jsonObject[key] = '';
    });
    return jsonObject;
  }

  /**
   * Casts a Json object to a map. Depends on correct json structure for a map cast.
   *
   * @param jsonObject An Object that is Json formatted and has the structure of a map (key, value pairs)
   * @returns A map with string keys and values of type any.
   */
  public static castJsonToMap(jsonObject: Object): Map<string, any> {
    var map = new Map<string, any>();
    for (var value in jsonObject) {
      map.set(value, jsonObject[value]);
    }
    return map;
  }

  /**
   * Casts a Json object to a map. Depends on correct json structure for a map cast.
   *
   * @param jsonObject An Object that is Json formatted and has the structure of a map (key, value pairs)
   * @returns A map with number keys and values of type any.
   */
  public static castJsonToNumberMap(jsonObject: Object): Map<number, any> {
    var map = new Map<number, any>();
    for (var value in jsonObject) {
      map.set(Number.parseInt(value), jsonObject[value]);
    }
    return map;
  }

  /**
   * Checks if a data object (json) contains an entry with the given key and if that entry contains
   * a filter string.
   * @param data the data to check on
   * @param key the key of the part of the data to check the string match
   * @param filter the string filter that should apply to the data[key]
   * @returns true if the data[key] contains the filter string
   */
  public static checkStringMatch(data: Object, key: string, filter: string): boolean {
    let keyData = data[key];
    if (keyData != null)
      return keyData.toString().trim().toLowerCase().indexOf(filter.toLowerCase()) !== -1;
    return false;
  }
}
