// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity_;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;
import org.siliconeconomy.porttransfer.backend.rest.dto.CapacitySummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewCapacityDto;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
@UnitTest
class CapacityMapperTest implements ConnectionMapperContract, TimeWindowMapperContract {

    /* Class under test */
    CapacityMapper capacityMapper;

    @Mock
    AddressRepository addressRepository;
    @Mock
    CompanyRepository companyRepository;
    @Mock
    VehicleRepository vehicleRepository;

    /* Helper constants */
    Company owner = new Company("TheOwner");
    Vehicle vehicle = new Vehicle(owner, "TheVehicle", 9_000, false, Modality.ROAD);

    @BeforeEach
    void beforeEach() {
        capacityMapper = new CapacityMapper(addressRepository,
                                            companyRepository,
                                            vehicleRepository);
    }

    @Test
    void map_Capacity_To_CapacitySummary() {
        // Arrange
        var capacity = new Capacity(vehicle,
                                    new TimeWindow(EARLIEST, LATEST),
                                    new Connection(SOURCE, SINK),
                                    false,
                                    false);
        ReflectionTestUtils.setField(capacity, BaseEntity_.ID, 7L);
        // Act
        var dto = capacityMapper.map(capacity);
        // Assert
        assertThat(dto)
                .returns(7L, CapacitySummaryDto::getId)
                .returns(Modality.ROAD, CapacitySummaryDto::getVehicleModality)
                .returns(false, CapacitySummaryDto::isVehicleCan45ft)
                .returns(9_000, CapacitySummaryDto::getVehicleWeightNettoMax)
                .returns(EARLIEST, CapacitySummaryDto::getCapacityEarliest)
                .returns(LATEST, CapacitySummaryDto::getCapacityLatest)
                .returns(SOURCE.getDesignation(),
                         CapacitySummaryDto::getCapacityOriginLocationDesignation)
                .returns(SINK.getDesignation(),
                         CapacitySummaryDto::getCapacityTargetLocationDesignation);
    }

    @Test
    void map_NewCapacity_To_Capacity_successful() {
        // Arrange
        /* Repositories/Services */
        setup_mapConnection_successful();
        when(companyRepository.findByDesignation(owner.getDesignation()))
                .thenReturn(Optional.of(owner));
        when(vehicleRepository.findByOwnerAndDesignation(owner, vehicle.getDesignation()))
                .thenReturn(Optional.of(vehicle));
        /* Test data */
        var dto = new NewCapacityDto("TheOwner",
                                     "TheVehicle",
                                     SOURCE.getDesignation(),
                                     SINK.getDesignation(),
                                     EARLIEST,
                                     LATEST,
                                     true);
        // Act
        var capacity = capacityMapper.map(dto);
        // Assert
        assertThat(capacity)
                .returns(vehicle, Capacity::getVehicle)
                .returns(true, Capacity::isFixed)
                .returns(false, Capacity::isFullyBooked)
                .returns(EARLIEST, c -> c.getTimeWindow().getEarliest())
                .returns(LATEST, c -> c.getTimeWindow().getLatest())
                .returns(SOURCE, c -> c.getConnection().getSource())
                .returns(SINK, c -> c.getConnection().getSink());
    }

    @Test
    void map_newCapacity_To_Capacity_fail_noOwner() {
        // Arrange
        /* Repositories/Services */
        setup_mapConnection_successful();
        when(companyRepository.findByDesignation(owner.getDesignation()))
                .thenReturn(Optional.empty());
        // vehicleRepository needs no stubbing - will not be called
        /* Test data */
        var dto = new NewCapacityDto("TheOwner",
                                     "TheVehicle",
                                     SOURCE.getDesignation(),
                                     SINK.getDesignation(),
                                     EARLIEST,
                                     LATEST,
                                     true);
        // Assert
        assertThatThrownBy(() -> capacityMapper.map(dto))
                .isInstanceOf(EntityCreationFailedException.class);
    }

    @Test
    void map_newCapacity_To_Capacity_fail_noVehicle() {
        // Arrange
        /* Repositories/Services */
        setup_mapConnection_successful();
        when(companyRepository.findByDesignation(owner.getDesignation()))
                .thenReturn(Optional.of(owner));
        when(vehicleRepository.findByOwnerAndDesignation(owner, vehicle.getDesignation()))
                .thenReturn(Optional.empty());
        /* Test data */
        var dto = new NewCapacityDto("TheOwner",
                                     "TheVehicle",
                                     SOURCE.getDesignation(),
                                     SINK.getDesignation(),
                                     EARLIEST,
                                     LATEST,
                                     true);
        // Assert
        assertThatThrownBy(() -> capacityMapper.map(dto))
                .isInstanceOf(EntityCreationFailedException.class);
    }

    /* Contract methods */

    @Override
    public AddressRepository getAddressRepositoryMock() {
        return addressRepository;
    }

    @Override
    public ConnectionMapper getConnectionMapper() {
        return capacityMapper;
    }

    @Override
    public TimeWindowMapper getTimeWindowMapper() {
        return capacityMapper;
    }

}
