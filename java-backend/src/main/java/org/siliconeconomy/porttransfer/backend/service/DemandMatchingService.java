// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.RequiredArgsConstructor;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.data.process.Order;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * A service that finds {@link Capacity}s matching a {@link Demand}
 */
@Service
@RequiredArgsConstructor
public class DemandMatchingService {

    private final CapacityRepository capacityRepository;

    /**
     * Searches for all Capacities that match the given Demand
     *
     * @param demand the Demand
     * @return the matching Capacities
     */
    @Transactional
    public List<Capacity> findMatching(Demand demand) {
        List<Capacity> matches = new ArrayList<>();
        matches.addAll(findFixedMatches(demand));
        matches.addAll(findFlexibleMatches(demand));
        return matches;
    }

    private List<Capacity> findFlexibleMatches(Demand demand) {
        Predicate<Capacity> lengthCheck =
                capacity -> capacity.getVehicle().isFortyFiveFootCapable() ||
                            demand.getLoadingUnit()
                                  .getContainer()
                                  .getLength() != ContainerLength.ISO_LENGTH_L;
        Predicate<Capacity> driveTimeCheck =
                capacity -> demand.getDeliveryTimeWindow().overlap(capacity.getTimeWindow())
                            >= demand.getEstimatedTime();
        return findPotentialMatches(demand, false)
                .filter(lengthCheck)
                .filter(driveTimeCheck)
                .collect(Collectors.toList());
    }

    private List<Capacity> findFixedMatches(Demand demand) {
        if (demand.getLoadingUnit().getContainer().getLength() != ContainerLength.ISO_LENGTH_2) {
            // container needs an entire truck -> no fixed matches
            return Collections.emptyList();
        }
        Predicate<Capacity> arrivesInTimeCheck =
                capacity -> demand.getDeliveryTimeWindow()
                                  .contains(capacity.getTimeWindow().getLatest());
        return findPotentialMatches(demand, true)
                .filter(arrivesInTimeCheck)
                .collect(Collectors.toList());
    }

    private Stream<Capacity> findPotentialMatches(Demand demand, boolean fixed) {
        return capacityRepository.findByConnectionSourceAndConnectionSinkAndVehicleFortyFiveFootCapableAndFixedAndFullyBooked(
                                         demand.getConnection().getSource(),
                                         demand.getConnection().getSink(),
                                         needsFortyFiveFoot(demand),
                                         fixed,
                                         false
                                 ).stream()
                                 // the weight needs to be checked regardless of $fixed
                                 .filter(weightCheck(demand));
    }

    private boolean needsFortyFiveFoot(Demand demand) {
        return demand.getLoadingUnit().getContainer().getLength() == ContainerLength.ISO_LENGTH_L;
    }

    private Predicate<Capacity> weightCheck(Demand demand) {
        return capacity -> (demand.getLoadingUnit().getBruttoWeight() + currentLoadWeight(capacity))
                           <= capacity.getVehicle().getMaxNettoWeight();
    }

    private int currentLoadWeight(Capacity capacity) {
        return capacity.getOrders().stream()
                       .map(Order::getDemand)
                       .map(Demand::getLoadingUnit)
                       .mapToInt(LoadingUnit::getBruttoWeight)
                       .sum();
    }

}
