> This project has been archived. It is no longer maintained or updated. If you have any questions about the project, please contact info@openlogisticsfoundation.org.

# Port Transfer

### Project Overview

The port transfer service provides the user with an ability to match demands and capacities for transportation. The
demands are created by terminals, that need to move loading units within a timeframe to a target location, while the
capacities are provided by local forwarding agencies.
This matching should reduce the number of unnecessary empty trips by the forwarding agencies, thus reducing traffic.

The port transfer service is decomposed into two sub-services:

* The **angular-frontend** (The web-interface for the user to interact with the service),
* the **java-backend** (The logic of dispatching based on imported and user given data).

For further details on the functionalities and features of the services, see the respective README files. A full
documentation according to [arc42](https://arc42.org) can be found in
the [documentation directory](documentation/index.adoc).

### Setting up the project

* You need to set up the database (we use postgreSQL as an example implementation). If you are using another database,
  configure Spring Boot Data accordingly.
* We recommend using continuous integration, e.g. gitlab-ci. For this, set up a corresponding gitlab-ci.yml
* Configure the config files (/config/ and /helm/<service>/values.yml) in the services according to your technical
  requirements. Pay attention to the URLs to the required services and set them to your needs.

### Running the project (locally)

To run and test all services together (integration testing), please run the services in the following order:

* database (optional, see the backend's [README](java-backend/README.md#build-and-run))
* java-backend
* angular-frontend

Please follow the instructions of the services in their respective readme files.

### License

[Open Logistics Foundation License](LICENSE)

### Contact information

* Product Owner
  * Nils Gastrich ([Mail](mailto:nils.gastrich@iml.fraunhofer.de))
  * Kai Hannemann ([Mail](mailto:kai.hannemann@iml.fraunhofer.de))
* Development Team
  * Kai Hannemann
  * Sandra Jankowski
  * Fabian König
  * Jan Pixberg
  * Christoph Schlüter
