// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.siliconeconomy.porttransfer.backend.BackendConfiguration.RFC3339_DATE_TIME_PATTERN;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAN_45_FT;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_EARLIEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_LATEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_SINK;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_SOURCE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.ID;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.MODALITY;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.WEIGHT_NETTO_MAX;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.siliconeconomy.porttransfer.backend.model.Modality;

/**
 * A summary of a capacity that may be used in an overview
 *
 * @author F. König
 */
@Value
public class CapacitySummaryDto {

    /**
     * A key that identifies this capacity
     */
    @NotNull
    @JsonProperty(ID)
    long id;

    /**
     * The type of modality this capacity will be transported with
     */
    @NotNull
    @Valid
    @JsonProperty(MODALITY)
    Modality vehicleModality;

    /**
     * Whether the vehicle can transport oversized (45ft) containers
     */
    @NotNull
    @JsonProperty(CAN_45_FT)
    boolean vehicleCan45ft;

    /**
     * The maximal weight (in kg) the vehicle can be loaded with
     */
    @NotNull
    @JsonProperty(WEIGHT_NETTO_MAX)
    int vehicleWeightNettoMax;

    /**
     * The origin of the capacity
     */
    @NotNull
    @JsonProperty(CAPACITY_SOURCE)
    String capacityOriginLocationDesignation;

    /**
     * The target of the capacity
     */
    @NotNull
    @JsonProperty(CAPACITY_SINK)
    String capacityTargetLocationDesignation;

    /**
     * The earliest time the capacity is available
     */
    @NotNull
    @Valid
    @JsonProperty(CAPACITY_EARLIEST)
    @JsonFormat(pattern = RFC3339_DATE_TIME_PATTERN)
    OffsetDateTime capacityEarliest;

    /**
     * The latest time the capacity is available
     */
    @NotNull
    @Valid
    @JsonProperty(CAPACITY_LATEST)
    @JsonFormat(pattern = RFC3339_DATE_TIME_PATTERN)
    OffsetDateTime capacityLatest;

}

