/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment as env } from 'src/environments/environment';
import { CapacityInitializer } from "../initialize/capacity-intializer";
import { CapacitySummary } from '../../models/http/backend/capacity-summary';
import { SnackbarService } from '../state/snackbar.service';

/**
 * This service provides the capacities.
 *
 * @author C. Schlüter, S. Jankowski
 */
@Injectable({
  providedIn: 'root'
})
export class CapacityService {
  private readonly url: string = env.apiHost + env.api + env.endPoints.capacities;

  private readonly capacities: BehaviorSubject<CapacitySummary[]>;

  constructor(httpClient: HttpClient, public snackService: SnackbarService) {
    this.capacities = new BehaviorSubject<CapacitySummary[]>([]);
    let initializer = new CapacityInitializer();
    httpClient.get<CapacitySummary[]>(this.url)
      .pipe(initializer.initializeList())
      .subscribe({
        next: (response) => this.responseHandler(response),
        error: (e) => this.errorHandler(e)
      });
  }

  /**
  * Get current capacities as Observable
  *
  * @return an {@link Observable} for the {@link CapacitySummary[]} values
  */
  public getCapacities(): Observable<CapacitySummary[]> {
    return this.capacities.asObservable();
  }

  private responseHandler = (response: CapacitySummary[]): void => {
    this.capacities.next(response);
  }

  private errorHandler = (error: any): void => {
    this.snackService.handleDefaultErrorsWithText("error.could_not_load_capacities", error.error);
  }
}
