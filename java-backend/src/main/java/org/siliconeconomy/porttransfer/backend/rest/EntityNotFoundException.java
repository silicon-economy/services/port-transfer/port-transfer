// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest;

import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception that indicates {@link HttpStatus#NOT_FOUND} caused by the database not containing the
 * requested Entity
 */
public class EntityNotFoundException extends ResponseStatusException {

    public EntityNotFoundException(Class<? extends BaseEntity> clazz, long id) {
        super(HttpStatus.NOT_FOUND, createMessage(clazz, id));
    }

    private static String createMessage(Class<? extends BaseEntity> clazz, long id) {
        return String.format("Could not find a %s with id '%d'", clazz.getSimpleName(), id);
    }

}
