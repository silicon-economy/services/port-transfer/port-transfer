/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Inject, Injectable} from '@angular/core';
import {filter} from 'rxjs/operators';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ScrollRestorationService {

  private readonly storageKey = 'scroll-state-history';
  scrollHistory = new Map();

  constructor(router: Router,
              @Inject('scrollContainerSelector') private sel: string) {
    const localStorageMap = this.getLocalStorage();
    if (localStorageMap) {
      this.scrollHistory = localStorageMap;
    }

    // Navigation Start
    router.events
      .pipe(filter((e): e is NavigationStart => e instanceof NavigationStart && e.url !== '/'))
      .subscribe(() => {
        this.scrollHistory.set(router.url, {
          top: document.querySelector(sel).scrollTop,
          left: document.querySelector(sel).scrollLeft
        });
        this.setLocalStorage(this.scrollHistory);

      });

    router.events
      .pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd))
      .subscribe(() => {
        setTimeout(() => {
          if (this.scrollHistory.has(router.url)) {
            document.querySelector(sel).scrollTo({
              top: this.scrollHistory.get(router.url).top,
              left: this.scrollHistory.get(router.url).left,
              behavior: 'auto'
            });
          } else {
            document.querySelector(sel).scrollTo({
              top: 0,
              left: 0,
              behavior: 'auto'
            });
          }
        }, 2);
      });

  }

  private setLocalStorage(map: Map<string, {}>): void {
    localStorage.setItem(this.storageKey, JSON.stringify(ScrollRestorationService.mapToObject(map)));
  }

  private getLocalStorage(): Map<string, {}> {
    const obj = JSON.parse(localStorage.getItem(this.storageKey));
    return ScrollRestorationService.objectToMap(obj);
  }

  private static mapToObject(map: Map<string, {}>): {} {
    return Object.fromEntries(map.entries());
  }

  private static objectToMap(obj: {}): Map<string, {}> {
    if (obj) {
      return new Map(Object.entries(obj));
    }
    return undefined;
  }

}
