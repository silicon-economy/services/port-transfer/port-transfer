// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

public interface ConnectionBuilderContract {

    /* Helper constants */
    Address SOURCE = DataTestUtils.fakeAddress();
    Address SINK = DataTestUtils.fakeAddress();

    /* Class under test */
    ConnectionBuilder<?> getValidBuilderAwaitingConnection();

    @Test
    default void buildsIfValidConnection() {
        // Arrange
        var builder = getValidBuilderAwaitingConnection();
        // Act
        builder.setSource(SOURCE);
        builder.setSink(SINK);
        // Assert
        assertThatCode(builder::build).doesNotThrowAnyException();
    }

    @Test
    default void throwsIfFailedBySource() {
        // Arrange
        var builder = getValidBuilderAwaitingConnection();
        // Act
        builder.failSource();
        builder.setSink(SINK);
        // Assert
        assertThatThrownBy(builder::build)
                .isInstanceOf(EntityCreationFailedException.class);
    }

    @Test
    default void throwsIfFailedBySink() {
        // Arrange
        var builder = getValidBuilderAwaitingConnection();
        // Act
        builder.setSource(SOURCE);
        builder.failSink();
        // Assert
        assertThatThrownBy(builder::build)
                .isInstanceOf(EntityCreationFailedException.class);
    }

}
