// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.WeekFields;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * The actual generator. Creates Capacities and Demands according to the configuration
 */
@Slf4j
@RequiredArgsConstructor
@Component
class GeneratorRunner implements ApplicationRunner {

    private final GeneratorContextValidator validator;
    private final GeneratorConfiguration configuration;
    private final GeneratorEntityFactory entityFactory;
    private final RandomGenerationService random;

    private List<Company> terminals;
    private List<Vehicle> vehicles;
    private LocalDate today;
    private LocalTime startOfDay;
    private LocalTime endOfDay;

    @Override
    public void run(ApplicationArguments args) {
        if (!configuration.isEnabled()) {
            log.debug("Generator not enabled");
            return;
        }
        if (!validator.canGenerate()) {
            log.warn("Could not generate, see log");
            return;
        }
        init();
        createTerminals();
        createDemands();
        createShippingVehicles();
        createCapacities();
    }

    private void init() {
        terminals = new LinkedList<>();
        vehicles = new LinkedList<>();
        today = LocalDate.now()
                         .with(WeekFields.ISO.weekOfWeekBasedYear(),
                               configuration.getCalendarWeek())
                         .with(WeekFields.ISO.dayOfWeek(), DayOfWeek.MONDAY.getValue());
        startOfDay = LocalTime.of(configuration.getBeginnOfDay(), 0);
        endOfDay = LocalTime.of(configuration.getEndOfDay(), 0);
    }

    private void createTerminals() {
        var count = 1;
        for (String designation : configuration.getTerminals()) {
            var terminal = entityFactory.createCompany(designation, String.valueOf(count));
            terminals.add(terminal);
            count++;
        }
    }

    private void createDemands() {
        for (Company terminal : terminals) {
            for (var dayOfWeek : DayOfWeek.values()) {
                for (var i = 0; i < configuration.getDemandMultiplier(); i++) {
                    if (random.isFullContainer()) {
                        createFullDemand(terminal, dayOfWeek);
                    } else {
                        createEmptyDemand(terminal, dayOfWeek);
                    }
                }
            }
        }
    }

    private void createFullDemand(Company terminal, DayOfWeek dayOfWeek) {
        entityFactory.createDemand(random.getType(),
                                   random.getLength(),
                                   false,
                                   terminal,
                                   chooseTargetTerminal(terminal.getAddress()),
                                   createFullContainerTimeWindow(dayOfWeek),
                                   random.getDriveTimeDurationInMinutes());
    }

    private TimeWindow createFullContainerTimeWindow(DayOfWeek deliveryDay) {
        // minimum of two -> bias towards earlier times
        int minuteInDay = Math.min(random.minuteInDay(), random.minuteInDay());
        LocalTime startTime = startOfDay.plusMinutes(minuteInDay);
        LocalTime endTime = startTime.plusMinutes(random.getDriveTimeDurationInMinutes());
        LocalDate date = getDate(deliveryDay);
        return new TimeWindow(OffsetDateTime.of(date, startTime, zone()),
                              OffsetDateTime.of(date, endTime, zone()));
    }

    private void createEmptyDemand(Company terminal, DayOfWeek lastDay) {
        entityFactory.createDemand(random.getType(),
                                   random.getLength(),
                                   true,
                                   terminal,
                                   chooseTargetTerminal(terminal.getAddress()),
                                   createEmptyContainerTimeWindow(lastDay),
                                   random.getDriveTimeDurationInMinutes());
    }

    private TimeWindow createEmptyContainerTimeWindow(DayOfWeek lastDay) {
        return new TimeWindow(
                OffsetDateTime.of(today, LocalTime.MIN, zone()),
                OffsetDateTime.of(getDate(lastDay), LocalTime.MAX, zone())
        );
    }

    private void createShippingVehicles() {
        var count = 1;
        for (var designation : configuration.getShippingCompanies()) {
            var shippingCompany = entityFactory.createCompany(designation, String.valueOf(count));
            for (var i = 0; i < configuration.getCapacityMultiplier(); i++) {
                Vehicle vehicle;
                if (random.isExtraLong()) {
                    vehicle = entityFactory.createVehicle(shippingCompany, "Long " + i, true);
                } else {
                    vehicle = entityFactory.createVehicle(shippingCompany, "Standard " + i, false);
                }
                vehicles.add(vehicle);
            }
        }
    }

    private void createCapacities() {
        for (Vehicle vehicle : vehicles) {
            for (DayOfWeek dayOfWeek : DayOfWeek.values()) {
                createCapacity(vehicle, dayOfWeek);
            }
        }
    }

    private void createCapacity(Vehicle vehicle, DayOfWeek dayOfWeek) {
        var isFree = random.nextBoolean();
        LocalTime startTime = startOfDay;
        var source = vehicle.getOwner().getAddress();
        while (true) {
            LocalTime endTime = startTime.plusMinutes(random.getTimeWindowDurationInMinutes());
            Address sink = chooseTargetTerminal(source);
            if (endTime.isAfter(endOfDay)) {
                return;
            }
            if (isFree) {
                LocalDate date = getDate(dayOfWeek);
                entityFactory.createCapacity(vehicle,
                                             new TimeWindow(OffsetDateTime.of(date,
                                                                              startTime,
                                                                              zone()),
                                                            OffsetDateTime.of(date,
                                                                              endTime,
                                                                              zone())),
                                             new Connection(source, sink));
            } else {
                startTime = endTime;
                source = sink;
            }
            isFree = !isFree;
        }
    }

    private LocalDate getDate(DayOfWeek dayOfWeek) {
        return today.with(WeekFields.ISO.dayOfWeek(), dayOfWeek.getValue());
    }

    private Address chooseTargetTerminal(Address exclude) {
        List<Address> list = terminals.stream()
                                      .map(Company::getAddress)
                                      .filter(not(exclude::equals))
                                      .collect(Collectors.toList());
        return random.selectOne(list);
    }

    private static ZoneOffset zone() {
        return OffsetDateTime.now().getOffset();
    }

}
