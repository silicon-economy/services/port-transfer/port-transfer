/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterService } from "./services/state/footer.service";
import { SidenavService } from "./services/state/sidenav.service";
import { ThemeService } from "./services/state/theme.service";
import { CookieDialogComponent } from "./dialogs/cookie-dialog/cookie-dialog.component";
import { MatButtonModule } from "@angular/material/button";
import { MatDialogModule } from "@angular/material/dialog";
import { FlexModule } from "@angular/flex-layout";
import { ToStringPipe } from './pipes/to-string.pipe';
import { TimeWindowPipe } from './pipes/time-window.pipe';

@NgModule({
  declarations: [
    CookieDialogComponent,
    ToStringPipe,
    TimeWindowPipe,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatDialogModule,
    FlexModule,
  ],
  exports: [
    ToStringPipe,
    TimeWindowPipe,
  ],
  providers: [
    FooterService,
    SidenavService,
    ThemeService,
  ],
})
export class SharedModule {
}
