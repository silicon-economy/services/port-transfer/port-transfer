// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository.process;

import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.repository.BaseRepository;

/**
 * A repository for loading units
 *
 * @author F. König
 */
public interface LoadingUnitRepository extends BaseRepository<LoadingUnit> {

}
