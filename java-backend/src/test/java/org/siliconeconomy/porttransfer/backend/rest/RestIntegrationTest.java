// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.IntegrationTest;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.rest.dto.CapacitySummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.DemandSummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.DtoTestUtils;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewCapacityDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewDemandDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional(propagation = Propagation.REQUIRES_NEW)
@ImportAutoConfiguration(exclude = SecurityAutoConfiguration.class)
@ActiveProfiles("test")
@IntegrationTest
class RestIntegrationTest {

    /* System under test */
    @Autowired
    FrontendRestController frontendRestController;
    @Autowired
    CapacityRepository capacityRepository;
    @Autowired
    DemandRepository demandRepository;

    /* Helper classes */
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    VehicleRepository vehicleRepository;

    @Nested
    @DisplayName("Given a minimal DB")
    class GivenACleanDatabase {

        Address source;
        Address sink;
        Company dispatcher;
        Company forwardingAgency;
        Vehicle vehicle;

        @BeforeEach
        void beforeEach() {
            source = addressRepository.save(fakeAddress("TheSource"));
            sink = addressRepository.save(fakeAddress("TheSink"));
            dispatcher = companyRepository.save(fakeCompany("TheDispatcher"));
            forwardingAgency = companyRepository.save(fakeCompany("TheOwner"));
            vehicle = vehicleRepository.save(fakeVehicle(forwardingAgency, "TheVehicle"));
        }

        @Nested
        @DisplayName("When adding a demand")
        class WhenAddDemand {

            NewDemandDto newDemand;
            DemandSummaryDto addDemandResponse;

            @BeforeEach
            void beforeEach() {
                newDemand = DtoTestUtils.createNewDemandDto();
                addDemandResponse = frontendRestController.addDemand(newDemand);
            }

            @Test
            @DisplayName("Then the response matches the new demand")
            void responseMatchesRequest() {
                // Assert
                assertThat(addDemandResponse)
                        .isNotNull()
                        .returns(newDemand.getDemandDesignation(),
                                 DemandSummaryDto::getDemandDesignation)
                        .returns(newDemand.getLuNumber(),
                                 DemandSummaryDto::getLuNumber)
                        .returns(newDemand.getContainerType(),
                                 DemandSummaryDto::getLuContainerType)
                        .returns(newDemand.getContainerSize(),
                                 DemandSummaryDto::getLuContainerSize)
                        .returns(newDemand.getWeightTara(),
                                 DemandSummaryDto::getLuWeightTara)
                        .returns(newDemand.getWeightNetto(),
                                 DemandSummaryDto::getLuOrderWeightNetto)
                        .returns(newDemand.getSource(),
                                 DemandSummaryDto::getOrderPickUpLocationDesignation)
                        .returns(newDemand.getSink(),
                                 DemandSummaryDto::getOrderDropOffLocationDesignation)
                        .returns(newDemand.getEarliest(),
                                 DemandSummaryDto::getOrderDropOffEarliest)
                        .returns(newDemand.getLatest(),
                                 DemandSummaryDto::getOrderDropOffLatest);
            }

            @Test
            @DisplayName("Then the DB contains a matching demand")
            void db() {
                assumeThat(addDemandResponse).isNotNull();
                // Act
                var demand = demandRepository.findById(addDemandResponse.getId());
                // Assert
                assertThat(demand)
                        .isPresent()
                        .get()
                        .returns(newDemand.getDemandDesignation(),
                                 Demand::getDesignation)
                        .returns(newDemand.getSource(),
                                 d -> d.getConnection().getSource().getDesignation())
                        .returns(newDemand.getSink(),
                                 d -> d.getConnection().getSink().getDesignation())
                        .returns(newDemand.getEarliest(),
                                 d -> d.getDeliveryTimeWindow().getEarliest())
                        .returns(newDemand.getLatest(),
                                 d -> d.getDeliveryTimeWindow().getLatest())
                        .returns(newDemand.getWeightNetto(),
                                 d -> d.getLoadingUnit().getNettoWeight())
                        .returns(newDemand.getLuNumber(),
                                 d -> d.getLoadingUnit().getContainer().getBicCode())
                        .returns(newDemand.getContainerType(),
                                 d -> d.getLoadingUnit().getContainer().getType())
                        .returns(newDemand.getContainerSize(),
                                 d -> d.getLoadingUnit().getContainer().getLength())
                        .returns(newDemand.getWeightTara(),
                                 d -> d.getLoadingUnit().getContainer().getTaraWeight());
            }

            @Test
            @DisplayName("Then the demand is part of all demands")
            void containedInGetDemands() {
                assumeThat(addDemandResponse).isNotNull();
                // Act
                var demands = frontendRestController.getDemands();
                // Assert
                assertThat(demands)
                        .contains(addDemandResponse);
            }

        }

        @Nested
        @DisplayName("When adding a capacity")
        class WhenAddCapacity {

            NewCapacityDto newCapacity;
            CapacitySummaryDto addCapacityResponse;

            @BeforeEach
            void beforeEach() {
                newCapacity = DtoTestUtils.createNewCapacityDto();
                addCapacityResponse = frontendRestController.addCapacity(newCapacity);
            }

            @Test
            @DisplayName("Then the response matches the new capacity")
            void responseMatchesRequest() {
                // Assert
                assertThat(addCapacityResponse)
                        .isNotNull()
                        .returns(newCapacity.getSource(),
                                 CapacitySummaryDto::getCapacityOriginLocationDesignation)
                        .returns(newCapacity.getSink(),
                                 CapacitySummaryDto::getCapacityTargetLocationDesignation)
                        .returns(newCapacity.getEarliest(),
                                 CapacitySummaryDto::getCapacityEarliest)
                        .returns(newCapacity.getLatest(),
                                 CapacitySummaryDto::getCapacityLatest);
            }

            @Test
            @DisplayName("Then the DB contains a matching capacity")
            void db() {
                assumeThat(addCapacityResponse).isNotNull();
                // Act
                var capacity = capacityRepository.findById(addCapacityResponse.getId());
                // Assert
                assertThat(capacity)
                        .isPresent()
                        .get()
                        .returns(newCapacity.isFixed(),
                                 Capacity::isFixed)
                        .returns(newCapacity.getSource(),
                                 c -> c.getConnection().getSource().getDesignation())
                        .returns(newCapacity.getSink(),
                                 c -> c.getConnection().getSink().getDesignation())
                        .returns(newCapacity.getEarliest(),
                                 c -> c.getTimeWindow().getEarliest())
                        .returns(newCapacity.getLatest(),
                                 c -> c.getTimeWindow().getLatest())
                        .returns(newCapacity.getVehicleDesignation(),
                                 c -> c.getVehicle().getDesignation())
                        .returns(newCapacity.getOwnerDesignation(),
                                 c -> c.getVehicle().getOwner().getDesignation());
            }

            @Test
            @DisplayName("Then the capacity is part of all capacities")
            void containedInGetCapacities() {
                assumeThat(addCapacityResponse).isNotNull();
                // Act
                var capacities = frontendRestController.getCapacities();
                // Assert
                assertThat(capacities)
                        .contains(addCapacityResponse);
            }

        }

    }

    /* Helper methods */

    private static Address fakeAddress(String designation) {
        return new Address(designation,
                           "Street",
                           "House",
                           "City",
                           12345,
                           "Country");
    }

    private static Company fakeCompany(String designation) {
        return new Company(designation);
    }

    @SuppressWarnings("SameParameterValue") /* intentionally mentioned in test cases */
    private static Vehicle fakeVehicle(Company owner, String designation) {
        return new Vehicle(owner,
                           designation,
                           9000,
                           false,
                           Modality.ROAD);
    }

}
