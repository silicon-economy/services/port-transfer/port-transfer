// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import java.util.LinkedList;
import java.util.List;

import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

/**
 * Base implementation for {@link Builder}
 *
 * @param <Entity> the entity to be build
 * @author F. König
 */
abstract class AbstractBuilder<Entity> implements Builder<Entity> {

    private final List<String> violations = new LinkedList<>();

    @Override
    public void fail(String message) {
        violations.add(message);
    }

    @Override
    public Entity build() throws EntityCreationFailedException {
        checkViolations();
        return buildValid();
    }

    /**
     * Check if the build process violated any constraints, and throw
     *
     * @throws EntityCreationFailedException contains the violations
     */
    protected void checkViolations() throws EntityCreationFailedException {
        if (!violations.isEmpty()) {
            throw new EntityCreationFailedException(violations);
        }
    }

    /**
     * Actually builds the entity. Will only be called if {@link #checkViolations()} was successful
     *
     * @return the new entity
     */
    protected abstract Entity buildValid();

}
