// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import java.time.OffsetDateTime;

import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CONTAINER_SIZE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CONTAINER_TYPE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DISPATCHER_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DROP_OFF_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DROP_OFF_EARLIEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.DROP_OFF_LATEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.LU_NUMBER;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.PICK_UP_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.WEIGHT_TARA;

import lombok.Setter;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;

/**
 * A Builder for {@link Demand}
 *
 * @author F. König
 */
@Setter
public class DemandBuilder extends AbstractBuilder<Demand>
        implements ConnectionBuilder<Demand>, TimeWindowBuilder<Demand> {

    private Company dispatcher;
    private Address source;
    private Address sink;
    private OffsetDateTime earliest;
    private OffsetDateTime latest;
    private String designation;
    private int estimatedTime;
    private int nettoWeight;
    private boolean hazard;
    private boolean refrigerated;
    private Container container;

    @Override
    protected Demand buildValid() {
        var loadingUnit = new LoadingUnit(container, nettoWeight, hazard, refrigerated);
        return new Demand(loadingUnit,
                          dispatcher,
                          new Connection(source, sink),
                          new TimeWindow(earliest, latest),
                          designation,
                          estimatedTime);
    }

    @Override
    public void failSource() {
        fail(PICK_UP_DESIGNATION);
    }

    @Override
    public void failSink() {
        fail(DROP_OFF_DESIGNATION);
    }

    @Override
    public void failEarliest() {
        fail(DROP_OFF_EARLIEST);
    }

    @Override
    public void failLatest() {
        fail(DROP_OFF_LATEST);
    }

    public void failDispatcher() {
        fail(DISPATCHER_DESIGNATION);
    }

    public void failContainer() {
        fail(LU_NUMBER);
        fail(CONTAINER_TYPE);
        fail(CONTAINER_SIZE);
        fail(WEIGHT_TARA);
    }

}
