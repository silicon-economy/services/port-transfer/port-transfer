// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.service.container;

/**
 * Indicates that a container request could not find any container
 *
 * @author F. König
 */
public class NoContainerException extends ContainerException {

    public NoContainerException(String bicCode) {
        super("No container with BIC-Registration '" + bicCode + "' found");
    }

}
