// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.mockup;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.data.process.LoadingUnit;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.Modality;
import org.siliconeconomy.porttransfer.backend.model.ModelTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.RepositoryTestUtils;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.ContainerRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.VehicleRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.LoadingUnitRepository;

@ExtendWith(MockitoExtension.class)
@UnitTest
class MockupEntityFactoryTest {

    @Mock
    private AddressRepository addressRepository;
    @Mock
    private CompanyRepository companyRepository;
    @Mock
    private ContainerRepository containerRepository;
    @Mock
    private VehicleRepository vehicleRepository;
    @Mock
    private LoadingUnitRepository loadingUnitRepository;
    @Mock
    private CapacityRepository capacityRepository;
    @Mock
    private DemandRepository demandRepository;

    /* Class under test */
    private MockupEntityFactory mockupEntityFactory;

    @BeforeEach
    void beforeEach() {
        mockupEntityFactory = new MockupEntityFactory(addressRepository,
                                                      companyRepository,
                                                      containerRepository,
                                                      vehicleRepository,
                                                      capacityRepository,
                                                      demandRepository,
                                                      loadingUnitRepository);
    }

    @Test
    void findOrCreateAddress_find() {
        // Arrange
        var existingAddress = new Address("TheAddress", "", "", "", 12345, "");
        when(addressRepository.findByDesignation("TheAddress"))
                .thenReturn(Optional.of(existingAddress));
        // Act
        var foundAddress = mockupEntityFactory.findOrCreateAddress("TheAddress");
        // Assert
        verify(addressRepository, never()).save(any());
        assertThat(foundAddress)
                .isEqualTo(existingAddress);
    }

    @Test
    void findOrCreateAddress_create() {
        // Arrange
        when(addressRepository.findByDesignation(anyString())).thenReturn(Optional.empty());
        RepositoryTestUtils.returnEntityOnSave(addressRepository);
        // Act
        var address = mockupEntityFactory.findOrCreateAddress("TheAddress");
        // Assert
        verify(addressRepository).save(address);
        assertThat(address.getDesignation())
                .isEqualTo("TheAddress");
    }

    @Test
    void findOrCreateCompany_find() {
        // Arrange
        var existingCompany = new Company("TheCompany");
        when(companyRepository.findByDesignation("TheCompany"))
                .thenReturn(Optional.of(existingCompany));
        // Act
        var foundCompany = mockupEntityFactory.findOrCreateCompany("TheCompany");
        // Assert
        verify(companyRepository, never()).save(any());
        assertThat(foundCompany)
                .isEqualTo(existingCompany);
    }

    @Test
    void findOrCreateCompany_create() {
        // Arrange
        when(companyRepository.findByDesignation(anyString())).thenReturn(Optional.empty());
        RepositoryTestUtils.returnEntityOnSave(companyRepository);
        // Act
        var company = mockupEntityFactory.findOrCreateCompany("TheCompany");
        // Assert
        verify(companyRepository).save(company);
        assertThat(company.getDesignation())
                .isEqualTo("TheCompany");
    }

    @ParameterizedTest
    @EnumSource
    void createContainer(ContainerLength length) {
        // Arrange
        RepositoryTestUtils.returnEntityOnSave(containerRepository);
        // Act
        var container = mockupEntityFactory.createContainer(length);
        // Assert
        verify(containerRepository).save(container);
        assertThat(container)
                .extracting(Container::getLength)
                .isEqualTo(length);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void findOrCreateVehicle_find_correctly(boolean fortyFiveFootCapable) {
        //Arrange
        var owner = DataTestUtils.fakeCompany();
        var existingVehicle = fakeVehicle(owner, "TheVehicle", fortyFiveFootCapable);
        when(vehicleRepository.findByOwnerAndDesignation(owner, "TheVehicle"))
                .thenReturn(Optional.of(existingVehicle));
        // Act
        var foundVehicle = mockupEntityFactory.findOrCreateVehicle(owner,
                                                                   "TheVehicle",
                                                                   fortyFiveFootCapable);
        // Assert
        verify(vehicleRepository, never()).save(any());
        assertThat(foundVehicle)
                .isEqualTo(existingVehicle);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void findOrCreateVehicle_find_withWrongLength(boolean existingIsFortyFiveFootCapable) {
        //Arrange
        var owner = DataTestUtils.fakeCompany();
        var existingVehicle = fakeVehicle(owner, "TheVehicle", existingIsFortyFiveFootCapable);
        when(vehicleRepository.findByOwnerAndDesignation(owner, "TheVehicle"))
                .thenReturn(Optional.of(existingVehicle));
        var requestedIsFortyFiveFootCapable = !existingIsFortyFiveFootCapable;
        // Act
        var foundVehicle = mockupEntityFactory.findOrCreateVehicle(owner,
                                                                   "TheVehicle",
                                                                   requestedIsFortyFiveFootCapable);
        // Assert
        verify(vehicleRepository, never()).save(any());
        assertThat(foundVehicle)
                .returns(existingIsFortyFiveFootCapable, Vehicle::isFortyFiveFootCapable)
                .isEqualTo(existingVehicle);
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void findOrCreateVehicle_create(boolean fortyFiveFootCapable) {
        //Arrange
        when(vehicleRepository.findByOwnerAndDesignation(any(), anyString()))
                .thenReturn(Optional.empty());
        RepositoryTestUtils.returnEntityOnSave(vehicleRepository);
        var company = DataTestUtils.fakeCompany();
        // Act
        var vehicle = mockupEntityFactory.findOrCreateVehicle(company,
                                                              "TheDesignation",
                                                              fortyFiveFootCapable);
        // Assert
        verify(vehicleRepository).save(vehicle);
        assertThat(vehicle)
                .returns(company, Vehicle::getOwner)
                .returns("TheDesignation", Vehicle::getDesignation)
                .returns(fortyFiveFootCapable, Vehicle::isFortyFiveFootCapable);
    }

    @Test
    void createLoadingUnit() {
        // Arrange
        var container = DataTestUtils.fakeContainer();
        RepositoryTestUtils.returnEntityOnSave(loadingUnitRepository);
        // Act
        var loadingUnit = mockupEntityFactory.createLoadingUnit(container);
        // Assert
        verify(loadingUnitRepository).save(loadingUnit);
        assertThat(loadingUnit)
                .extracting(LoadingUnit::getContainer)
                .isEqualTo(container);
    }

    @Test
    void createCapacity() {
        // Arrange
        var vehicle = DataTestUtils.fakeVehicle();
        var connection = ModelTestUtils.fakeConnection();
        // Act
        mockupEntityFactory.createCapacity(vehicle, connection);
        // Assert
        var captor = ArgumentCaptor.forClass(Capacity.class);
        verify(capacityRepository).save(captor.capture());
        assertThat(captor.getValue())
                .returns(connection, Capacity::getConnection)
                .returns(vehicle, Capacity::getVehicle);
    }

    @Test
    void createDemand() {
        // Arrange
        var loadingUnit = DataTestUtils.fakeLoadingUnit();
        var dispatcher = DataTestUtils.fakeCompany();
        var connection = ModelTestUtils.fakeConnection();
        // Act
        mockupEntityFactory.createDemand("TheDemand", loadingUnit, dispatcher, connection);
        // Assert
        var captor = ArgumentCaptor.forClass(Demand.class);
        verify(demandRepository).save(captor.capture());
        assertThat(captor.getValue())
                .returns("TheDemand", Demand::getDesignation)
                .returns(loadingUnit, Demand::getLoadingUnit)
                .returns(dispatcher, Demand::getDispatcher)
                .returns(connection, Demand::getConnection);
    }

    /* Helper methods */

    @SuppressWarnings("SameParameterValue") /* intentionally mentioned in tests */
    private static Vehicle fakeVehicle(Company owner,
                                       String designation,
                                       boolean fortyFiveFootCapable) {
        return new Vehicle(owner,
                           designation,
                           9001,
                           fortyFiveFootCapable,
                           Modality.ROAD);
    }

}
