/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material/paginator';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

/**
* Paginator that is able to translate the pagination-labels of the data-table
*/
@Injectable()
export class I18nMatPaginatorIntl extends MatPaginatorIntl {

    constructor(private translate: TranslateService) {
        super();
        this.updateLabels();

        this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
            this.updateLabels();
        });
    }

    getPaginatorIntl(): MatPaginatorIntl {
        return this;
    }

    private getThisRangeLabel(page: number, pageSize: number, length: number): string {
        if (length === 0 || pageSize === 0) {
            return this.translate.instant('paginator.RANGE_PAGE_LABEL_1', { length });
        }
        length = Math.max(length, 0);
        const startIndex = page * pageSize;
        const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
        return this.translate.instant('paginator.RANGE_PAGE_LABEL_2', { startIndex: startIndex + 1, endIndex, length });
    }

    private updateLabels() {
        console.log("Changed language (paginator) to language " + this.translate.currentLang);
        this.itemsPerPageLabel = this.translate.instant('paginator.ITEMS_PER_PAGE_LABEL');
        this.nextPageLabel = this.translate.instant('paginator.NEXT_PAGE_LABEL');
        this.previousPageLabel = this.translate.instant('paginator.PREVIOUS_PAGE_LABEL');
        this.firstPageLabel = this.translate.instant('paginator.FIRST_PAGE_LABEL');
        this.lastPageLabel = this.translate.instant('paginator.LAST_PAGE_LABEL');
        this.getRangeLabel = this.getThisRangeLabel.bind(this);
        this.changes.next();
    }
}
