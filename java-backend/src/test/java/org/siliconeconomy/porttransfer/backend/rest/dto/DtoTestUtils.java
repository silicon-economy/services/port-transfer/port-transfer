// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.siliconeconomy.porttransfer.backend.model.Modality;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DtoTestUtils {

    private static final OffsetDateTime START_TIME = OffsetDateTime.of(2018,
                                                                       11,
                                                                       13,
                                                                       20,
                                                                       20,
                                                                       39,
                                                                       0,
                                                                       ZoneOffset.UTC);
    private static final OffsetDateTime END_TIME = OffsetDateTime.of(2019,
                                                                     5,
                                                                     1,
                                                                     8,
                                                                     2,
                                                                     3,
                                                                     0,
                                                                     ZoneOffset.UTC);

    public static CapacitySummaryDto createCapacitySummaryDto() {
        return new CapacitySummaryDto(1337,
                                      Modality.ROAD,
                                      true,
                                      9001,
                                      "Source",
                                      "Sink",
                                      START_TIME,
                                      END_TIME);
    }

    public static DemandSummaryDto createDemandSummaryDto() {
        return new DemandSummaryDto(1337,
                                    "TheDemand",
                                    "BIC1 23456 7",
                                    ContainerType.FLAT,
                                    ContainerLength.ISO_LENGTH_4,
                                    1000,
                                    8000,
                                    "TheSource",
                                    "TheSink",
                                    START_TIME,
                                    END_TIME);
    }

    public static NewDemandDto createNewDemandDto() {
        return new NewDemandDto("TheDispatcher",
                                "TheDemand",
                                1,
                                "BICU 123456 7",
                                ContainerType.ISO,
                                ContainerLength.ISO_LENGTH_2,
                                1_000,
                                0,
                                "TheSource",
                                "TheSink",
                                START_TIME,
                                END_TIME,
                                false,
                                true);
    }

    public static NewCapacityDto createNewCapacityDto() {
        return new NewCapacityDto("TheOwner",
                                  "TheVehicle",
                                  "TheSource",
                                  "TheSink",
                                  START_TIME,
                                  END_TIME,
                                  false);
    }

    public static SessionInvariantDataDto createSessionInvariantDataDto() {
        return new SessionInvariantDataDto(List.of("Company_A", "Company_B"),
                                           List.of("Address_A", "Address_B"));
    }

}
