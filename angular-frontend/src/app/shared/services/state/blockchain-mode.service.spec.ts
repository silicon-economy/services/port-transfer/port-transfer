/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { BlockchainModeService } from './blockchain-mode.service';

describe('BlockchainModeService', () => {
  let service: BlockchainModeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlockchainModeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should toggle blocklchain Mode', () => {
    //TODO() expect
    service['blockchainMode'] = true;
    service.toggleBlockchainMode();
    service['blockchainMode'] = false;
    service.toggleBlockchainMode();
  });
});
