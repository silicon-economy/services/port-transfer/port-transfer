// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.io.IOException;
import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThatObject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.NumericNode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.springframework.core.io.ClassPathResource;

@UnitTest
class DtoTest {

    private static final ObjectMapper mapper = new ObjectMapper().findAndRegisterModules();
    private static final Comparator<JsonNode> jsonNodeComparator = jsonNodeComparator();

    @ParameterizedTest
    @EnumSource
    void checkDtoJsonRepresentation(TestCase testCase) throws IOException {
        // Arrange
        JsonNode expectedJson = readFromFile(testCase.getExpectedJsonFile());
        // Act
        JsonNode actualJson = mapper.valueToTree(testCase.getDto());
        // Assert
        assertThatObject(actualJson)
                .usingComparator(jsonNodeComparator)
                .isEqualTo(expectedJson);
    }

    @RequiredArgsConstructor
    @Getter
    private enum TestCase {
        CAPACITY_SUMMARY("dto/capacitySummary.json",
                         DtoTestUtils.createCapacitySummaryDto()),
        DEMAND_SUMMARY("dto/demandSummary.json",
                       DtoTestUtils.createDemandSummaryDto()),
        NEW_CAPACITY("dto/newCapacity.json",
                     DtoTestUtils.createNewCapacityDto()),
        NEW_DEMAND("dto/newDemand.json",
                   DtoTestUtils.createNewDemandDto()),
        SESSION_INVARIANT_DATA("dto/sessionInvariantData.json",
                               DtoTestUtils.createSessionInvariantDataDto());

        private final String expectedJsonFile;
        private final Object dto;

    }

    /* Helper methods */

    private static JsonNode readFromFile(String fileName) throws IOException {
        return mapper.readTree(new ClassPathResource(fileName).getFile());
    }

    private static Comparator<JsonNode> jsonNodeComparator() {
        return (o1, o2) -> {
            // forward to specialized equals()-method
            if (o1.equals(numberAgnosticComparator(), o2)) {
                return 0;
            }
            return 1;
        };
    }

    private static Comparator<JsonNode> numberAgnosticComparator() {
        return (o1, o2) -> {
            if (o1.equals(o2)) {
                return 0;
            }
            if ((o1 instanceof NumericNode) && (o2 instanceof NumericNode)) {
                double d1 = o1.asDouble();
                double d2 = o2.asDouble();
                if (d1 == d2) {
                    return 0;
                }
            }
            return 1;
        };
    }

}
