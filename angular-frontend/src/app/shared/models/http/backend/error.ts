/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface ModelError {
  /**
   * The http status code number
   */
  status: number;
  /**
   * The http status code name
   */
  error: string;
  /**
   * The timestamp of the creation of the error
   */
  timestamp: string;
  /**
   * A message describing the error; defaults to the status code
   */
  message: string;
  /**
   * The path that was requested
   */
  path: string;
  /**
   * [optional] The internal stack trace of the error; only available during development
   */
  trace?: string;
}

