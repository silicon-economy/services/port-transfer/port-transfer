/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  // Check if fields are defined before reading them, they are not in e.g. headless chrome
  production: false,
  // BACKEND_URL has to include the protocol since we can not rely on servers redirecting us to https when hardcoding http
  apiHost: $ENV.backendUrl || "http://localhost:8080",
  // Keycloak env
  keycloakUrl: $ENV.keycloakUrl || "https://porttransfer-keycloak.apps.sele.iml.fraunhofer.de",
  keycloakRealm: $ENV.keycloakRealm || "PortTransfer",
  keycloakClient: $ENV.keycloakClient || "main",

  // The following environment variables are independent of deployment
  api: '/api/v1',
  endPoints: {
    capacities: "/capacities",
    demands: "/demands",
    getMatchingCapacities: "/demands/{demandId}/match",
    sessionInvariantData: "/session-invariant-data-sync"
  }
}
