// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import java.time.OffsetDateTime;

import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_EARLIEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_LATEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_SINK;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_SOURCE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.OWNER_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.VEHICLE_DESIGNATION;

import lombok.Setter;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Vehicle;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.model.Connection;
import org.siliconeconomy.porttransfer.backend.model.TimeWindow;

/**
 * A Builder for {@link Capacity}
 *
 * @author F. König
 */
@Setter
public class CapacityBuilder extends AbstractBuilder<Capacity>
        implements ConnectionBuilder<Capacity>, TimeWindowBuilder<Capacity> {

    private Vehicle vehicle;
    private Address source;
    private Address sink;
    private OffsetDateTime earliest;
    private OffsetDateTime latest;
    private boolean fixed;

    @Override
    protected Capacity buildValid() {
        return new Capacity(vehicle,
                            new TimeWindow(earliest, latest),
                            new Connection(source, sink),
                            fixed,
                            false);
    }

    @Override
    public void failSource() {
        fail(CAPACITY_SOURCE);
    }

    @Override
    public void failSink() {
        fail(CAPACITY_SINK);
    }

    @Override
    public void failEarliest() {
        fail(CAPACITY_EARLIEST);
    }

    @Override
    public void failLatest() {
        fail(CAPACITY_LATEST);
    }

    public void failVehicle() {
        fail(OWNER_DESIGNATION);
        fail(VEHICLE_DESIGNATION);
    }

}
