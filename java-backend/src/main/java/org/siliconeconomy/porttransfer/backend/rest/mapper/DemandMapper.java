// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import java.util.Objects;

import lombok.RequiredArgsConstructor;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;
import org.siliconeconomy.porttransfer.backend.rest.builder.DemandBuilder;
import org.siliconeconomy.porttransfer.backend.rest.dto.DemandSummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewDemandDto;
import org.siliconeconomy.porttransfer.backend.service.container.ContainerException;
import org.siliconeconomy.porttransfer.backend.service.container.ContainerService;
import org.springframework.stereotype.Component;

/**
 * This class maps a {@link Demand} to a {@link DemandSummaryDto}
 *
 * @author F. König
 */
@Component
@RequiredArgsConstructor
public class DemandMapper implements ConnectionMapper, TimeWindowMapper {

    private final CompanyRepository companyRepository;
    private final AddressRepository addressRepository;
    private final ContainerService containerService;

    public DemandSummaryDto map(Demand demand) {
        return new DemandSummaryDto(Objects.requireNonNull(demand.getId()),
                                    demand.getDesignation(),
                                    demand.getLoadingUnit().getContainer().getBicCode(),
                                    demand.getLoadingUnit().getContainer().getType(),
                                    demand.getLoadingUnit().getContainer().getLength(),
                                    demand.getLoadingUnit().getContainer().getTaraWeight(),
                                    demand.getLoadingUnit().getNettoWeight(),
                                    demand.getConnection().getSource().getDesignation(),
                                    demand.getConnection().getSink().getDesignation(),
                                    demand.getDeliveryTimeWindow().getEarliest(),
                                    demand.getDeliveryTimeWindow().getLatest());
    }

    public Demand map(NewDemandDto dto) throws EntityCreationFailedException {
        var builder = new DemandBuilder();
        companyRepository.findByDesignation(dto.getDispatcherDesignation())
                         .ifPresentOrElse(builder::setDispatcher, builder::failDispatcher);
        mapConnection(builder, addressRepository, dto);
        mapTimeWindow(builder, dto);
        try {
            builder.setContainer(containerService.findOrCreateContainer(dto.getLuNumber(),
                                                                        dto.getContainerType(),
                                                                        dto.getContainerSize(),
                                                                        dto.getWeightTara()));
        } catch (ContainerException e) {
            builder.failContainer();
        }
        builder.setDesignation(dto.getDemandDesignation());
        builder.setEstimatedTime(dto.getEstimatedTime());
        builder.setNettoWeight(dto.getWeightNetto());
        builder.setHazard(dto.isDangerousGoodsIndication());
        builder.setRefrigerated(dto.isReeferIndication());
        return builder.build();
    }

}
