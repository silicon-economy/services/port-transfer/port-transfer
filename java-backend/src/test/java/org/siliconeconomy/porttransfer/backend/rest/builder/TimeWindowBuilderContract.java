// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.builder;

import java.time.OffsetDateTime;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.TestConstants;
import org.siliconeconomy.porttransfer.backend.rest.EntityCreationFailedException;

public interface TimeWindowBuilderContract {

    /* Helper constants */
    OffsetDateTime EARLIEST = TestConstants.TIME_INSTANCE;
    OffsetDateTime LATEST = EARLIEST.plusMinutes(1);

    /* Class under test */
    TimeWindowBuilder<?> getValidBuilderAwaitingTimeWindow();

    @Test
    default void buildsIfValidTimeWindow() {
        // Arrange
        var builder = getValidBuilderAwaitingTimeWindow();
        // Act
        builder.setEarliest(EARLIEST);
        builder.setLatest(LATEST);
        // Assert
        assertThatCode(builder::build).doesNotThrowAnyException();
    }

    @Test
    default void throwsIfFailedByEarliest() {
        // Arrange
        var builder = getValidBuilderAwaitingTimeWindow();
        // Act
        builder.failEarliest();
        builder.setLatest(LATEST);
        // Assert
        assertThatThrownBy(builder::build).isInstanceOf(EntityCreationFailedException.class);
    }

    @Test
    default void throwsIfFailedByLatest() {
        // Arrange
        var builder = getValidBuilderAwaitingTimeWindow();
        // Act
        builder.setEarliest(EARLIEST);
        builder.failLatest();
        // Assert
        assertThatThrownBy(builder::build).isInstanceOf(EntityCreationFailedException.class);
    }

}
