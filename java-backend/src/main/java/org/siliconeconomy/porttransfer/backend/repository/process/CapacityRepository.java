// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.repository.process;

import java.util.List;

import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.process.Capacity;
import org.siliconeconomy.porttransfer.backend.repository.BaseRepository;

/**
 * Repository for capacities
 *
 * @author F. König
 */
public interface CapacityRepository extends BaseRepository<Capacity> {

    List<Capacity> findByConnectionSourceAndConnectionSinkAndVehicleFortyFiveFootCapableAndFixedAndFullyBooked(
            Address connectionSource,
            Address connectionSink,
            boolean vehicleFortyFiveFootCapable,
            boolean fixed,
            boolean fullyBooked);

}
