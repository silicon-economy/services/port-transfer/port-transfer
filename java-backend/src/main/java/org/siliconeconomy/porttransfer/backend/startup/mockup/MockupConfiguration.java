// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.mockup;

import static org.siliconeconomy.porttransfer.backend.startup.mockup.MockupConfiguration.MOCKUP_CONFIG_PREFIX;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Configuration options for the mockup suite
 *
 * @author F. König
 */
@Data
@ConfigurationProperties(MOCKUP_CONFIG_PREFIX)
public class MockupConfiguration {

    public static final String MOCKUP_CONFIG_PREFIX = "application.mockup";

    private boolean enabled = false;

}
