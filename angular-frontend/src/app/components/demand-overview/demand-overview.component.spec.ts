/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { DemandService } from 'src/app/shared/services/http/demand.service';

import { DemandOverviewComponent } from './demand-overview.component';
import { MatTableModule } from '@angular/material/table';
import { DatePipe } from '@angular/common';
import { DEMANDS } from 'src/app/shared/test/demands';
import { of } from 'rxjs';
import { DemandView } from './demand-view';
import { DemandMatchingService } from 'src/app/shared/services/http/demand-matching.service';

/**
 * @author C. Schlüter
 */

describe('DemandOverviewComponent', () => {
  let component: DemandOverviewComponent;
  let fixture: ComponentFixture<DemandOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DemandOverviewComponent
      ],
      imports: [
        MatTableModule,
        TranslateTestingModule.withTranslations('de', {}),
      ],
      providers: [
        DatePipe,
        {
          provide: DemandService,
          useValue: {
            getDemands: () => of(DEMANDS)
          }
        },
        {
          provide: DemandMatchingService,
          useValue: {
            triggerMatching: () => {}
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DemandOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should select the correct demand', () => {
    component.select(new DemandView(DEMANDS[1]));
    expect(component.selectedDemand).toBe(DEMANDS[1]);
  });
});
