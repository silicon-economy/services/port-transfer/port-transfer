// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Positive;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.springframework.lang.NonNull;

/**
 * A physical address, to be used as an endpoint of a transport's route
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Address extends BaseEntity {

    /**
     * The unique designation of the address
     */
    @NonNull
    @Column(unique = true, updatable = false, nullable = false)
    private String designation;

    /**
     * The street at which the address is located
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private String street;

    /**
     * The house number at which the address is located
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private String houseNumber;

    /**
     * The city in which the address is located
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private String city;

    /**
     * The postal code for the area in which the address is located
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    @Positive
    private int postalCode;

    /**
     * The country in which the address is located
     */
    @NonNull
    @Column(updatable = false, nullable = false)
    private String country;

}
