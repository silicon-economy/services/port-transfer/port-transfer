// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.startup.generator;

import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.siliconeconomy.porttransfer.backend.UnitTest;
import org.siliconeconomy.porttransfer.backend.model.ContainerLength;
import org.siliconeconomy.porttransfer.backend.model.ContainerType;
import org.springframework.test.util.ReflectionTestUtils;

@UnitTest
class RandomGenerationServiceTest {

    /* Methods that depend on some kind of RPG should be repeated this many times */
    private static final int REPETITIONS = 10;

    /* Helper class configuration */
    static GeneratorConfiguration configuration;

    /* Under test */
    RandomGenerationService randomGenerationService;

    @BeforeAll
    static void beforeAll() {
        configuration = createValidConfiguration();
    }

    @BeforeEach
    void beforeEach() {
        randomGenerationService = new RandomGenerationService(configuration);
    }

    @RepeatedTest(REPETITIONS)
    void getTimeWindowDurationInMinutes() {
        // Arrange
        var step = (Integer) ReflectionTestUtils.getField(randomGenerationService,
                                                          "TIME_WINDOW_STEP");
        Objects.requireNonNull(step);
        // Act
        var duration = randomGenerationService.getTimeWindowDurationInMinutes();
        // Assert
        assertThat(duration)
                .isGreaterThanOrEqualTo(configuration.getMinimumCapacityDuration() * step)
                .isLessThanOrEqualTo(configuration.getMaximumCapacityDuration() * step);
    }

    @RepeatedTest(REPETITIONS)
    void getDriveTimeDurationInMinutes() {
        // Arrange
        var step = (Integer) ReflectionTestUtils.getField(randomGenerationService,
                                                          "DRIVE_TIME_STEP");
        Objects.requireNonNull(step);
        // Act
        var duration = randomGenerationService.getDriveTimeDurationInMinutes();
        // Assert
        assertThat(duration)
                .isGreaterThanOrEqualTo(configuration.getMinimumCapacityDuration() * step)
                .isLessThanOrEqualTo(configuration.getMaximumCapacityDuration() * step);
    }

    @RepeatedTest(REPETITIONS)
    void minuteInDay() {
        // Arrange
        var maxValue = (configuration.getEndOfDay() - configuration.getBeginnOfDay()) * 60;
        // Act
        var minute = randomGenerationService.minuteInDay();
        // Assert
        assertThat(minute)
                .isNotNegative()
                .isLessThanOrEqualTo(maxValue);
    }

    @RepeatedTest(REPETITIONS)
    void call_all_remaining() {
        /* All values are random, so the test simply checks that no nonsense values are generated */
        // Assert
        assertThatCode(() -> randomGenerationService.isFullContainer())
                .doesNotThrowAnyException();
        assertThatCode(() -> randomGenerationService.isExtraLong())
                .doesNotThrowAnyException();
        assertThatCode(() -> randomGenerationService.nextBoolean())
                .doesNotThrowAnyException();
        assertThat(randomGenerationService.getType())
                .isIn((Object[]) ContainerType.values());
        assertThat(randomGenerationService.getLength())
                .isIn((Object[]) ContainerLength.values());
    }

    @RepeatedTest(REPETITIONS)
    void selectOne() {
        // Arrange
        var someStrings = List.of("One", "Two", "Three");
        // Act
        var aString = randomGenerationService.selectOne(someStrings);
        // Assert
        assertThat(aString)
                .isIn(someStrings);
    }

    /* Helper methods */

    private static GeneratorConfiguration createValidConfiguration() {
        var configuration = new GeneratorConfiguration();
        configuration.setEnabled(true);
        configuration.setTerminals(List.of("Terminal"));
        configuration.setShippingCompanies(List.of("ShippingCompany"));
        configuration.setDemandMultiplier(1);
        configuration.setCapacityMultiplier(1);
        configuration.setCalendarWeek(1);
        configuration.setBeginnOfDay(8);
        configuration.setEndOfDay(20);
        configuration.setMinimumCapacityDuration(1);
        configuration.setMaximumCapacityDuration(4);
        configuration.setNonStandardLengthProbability(10);
        configuration.setFullContainerProbability(10);
        return configuration;
    }

}
