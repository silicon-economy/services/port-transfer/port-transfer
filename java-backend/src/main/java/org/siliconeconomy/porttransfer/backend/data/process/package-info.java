// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains process data
 * <p>
 * This data that is usually only used during one or few processes and seldom relevant for others.
 * <p>
 * Process data may contain references to other process data or to master data.
 *
 * @author F. König
 */

package org.siliconeconomy.porttransfer.backend.data.process;
