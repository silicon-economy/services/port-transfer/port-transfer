/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

/**
 * This service provides a Snackbar which can be triggered from every UI component to show messages or errors.
 *
 * @author S. Jankowski, C. Schlüter
 */
export class SnackbarService {

  private statusMessage: Subject<string> = new Subject<string>();
  private nextDuration: number;

  constructor(public translateService: TranslateService) { }

  public getStatusMessage(): Observable<string> {
    return this.statusMessage.asObservable();
  }

  private pushNewStatus(message: string, duration: number = -1): void {
    this.nextDuration = duration;
    this.statusMessage.next(message);
  }

  private getDefaultErrorText(message: string, error: any): string {
    let msg = this.translateService.instant(message);
    if (error && error.status) {
      msg += ' ' + this.translateService.instant('error.error') + ' ';
      msg += error.status + ": ";
      msg += this.translateService.instant('error.' + error.status);
    }
    return msg;
  }

  public getNextDuration(): number {
    return this.nextDuration;
  }

  /**
   * Defines an error message for the snackbar, if something went wrong.
   * @param message A custom message to display
   * @param error The error, that occured (requires at least a status and a message attribute to work properly)
   */
  public handleDefaultErrorsWithText(message: string, error: any): void {
    this.pushNewStatus(this.getDefaultErrorText(message, error), 10000);
  }

  /**
   * Defines an error message for the snackbar, if the creation of an object failed.
   * @param message A custom message to display.
   * @param error The error, that occured (requires at least a status and a message attribute to work properly)
   * @param prefix A translation prefix for the error message list.
   */
  public handleCreateError(message: string, error: any, prefix: string): void {
    let msg = this.getDefaultErrorText(message, error) + "\n";
    if (error && error.message) {
      let fields = error.message.split(';').map(field => this.translateService.instant(prefix + field));
      msg += fields.join(", ")
    }
    this.pushNewStatus(msg);
  }

  /**
   * Defines a snackbar message for a successfully created object.
   * @param responseDesignation The name of the created object's class as a translatable key
   * @param objectName The designation of the precise object
   */
  public handleSuccessfulCreation(responseDesignation: string, objectName: string): void {
    let msg = this.translateService.instant(objectName);
    msg += ' "' + responseDesignation + '" ';
    msg += this.translateService.instant('addedSuccessfully');
    this.pushNewStatus(msg, 10000);
  }

  /**
   * Shows a snackbar message with variables inside filled in.
   * @param message the message that should be displayed (translate key)
   * @param varsToInsert The variables to insert into the translated message in form of { var1, var2, ...}
   * @param duration The duration to display the snackbar in milliseconds
   */
  public handleMessageWithTranslateVariables(message: string, varsToInsert: any, duration: number = 4000): void {
    this.pushNewStatus(this.translateService.instant(message, varsToInsert), duration);
  }
}
