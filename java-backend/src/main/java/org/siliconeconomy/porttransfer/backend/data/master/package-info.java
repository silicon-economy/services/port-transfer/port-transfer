// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package contains master data
 * <p>
 * This data is re-used between processes and rarely, if ever, changes.
 * <p>
 * Master data may not contain references to process data.
 *
 * @author F. König
 */

package org.siliconeconomy.porttransfer.backend.data.master;
