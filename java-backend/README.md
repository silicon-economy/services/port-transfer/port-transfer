# Java backend for PortTransfer

This is the Java application that manages the backend of the PortTransfer project.
It provides a Rest API for the frontend and manages a PostgreSQL database.

### Technology stack

* Java
* SpringBoot
* PostgreSQL

### Build and run

__All following commands should be executed in the directory `port-transfer/java-backend`.__

Run tests: `mvn test`

Build: `mvn clean package`

Run: `java -jar target\java-backend-${Version}.jar`

Run locally (for development): `java -jar target\java-backend-${Version}.jar`

Before starting the application ensure that a PostgreSQL database is running. It is further necessary to provide some environment-specific configuration. This can be done via environment variables, JVM parameters or entries in [`application.properties`](src/main/resources/application.properties).

For the database:
* __spring.datasource.url__: URL (jdbc:postgres://_ip_:_port_/_databasename_)
* __spring.datasource.username__: username
* __spring.datasource.password__: password

For CORS-configuration:
* __application.security.corsOrigin__: Base URL of the frontend

For Spring:
* __spring.profiles.active__: (optional) see below

#### Spring Profiles

Multiple profiles are provided that allow for hassle-free development. Activate those by providing a comma-seperated list.

* __dev__: provides a fresh database on application start. May be used during development, but avoided in a production environment
* __localFrontend__: configure this backend for a frontend on the same machine
* __testDB__: use a fresh in-memory database
* __local__: combines localFrontend and testDB; no further configuration is necessary when developing locally
* __test__: used during testing, manual usage should be avoided
* __generator__: creates a configurable amount of example data on application start

Examples for running version 1.2.3 with different profiles:

`java -jar target\java-backend-1.2.3.jar --spring.profiles.active=local`

`java -jar target\java-backend-1.2.3.jar --spring.profiles.active=dev,localFrontend`

### Use

The application provides a Rest API that can be used for fetching a list of known capacities and demands in the context of transfers between terminals.

### Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

* [`third-party-licenses.txt`](third-party-licenses/third-party-licenses.txt) - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.

#### Generating third-party license reports

This project uses the [license-maven-plugin](https://github.com/mojohaus/license-maven-plugin) to generate a file containing the licenses used by the third-party dependencies.
The content of the `mvn license:add-third-party` Maven goal's output (`target/generated-sources/license/THIRD-PARTY.txt`) can be copied into `third-party-licenses/third-party-licenses.txt`.

Third-party dependencies for which the licenses cannot be determined automatically by the license-maven-plugin have to be documented manually in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "Unknown license" license.

## Development

## Guidelines & Code Style

The project follows the [Java Guidelines](https://oe160.iml.fraunhofer.de/wiki/display/HOW/Java+Guidelines) (which extends the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)), with the following changes:
* annotations, even "type-use", are always followed by a new line (this new line is optional in [Google 4.8.5](https://google.github.io/styleguide/javaguide.html#s4.8.5-annotations))
* Type variable names for generic classes follow the naming scheme for classes; type variable names for generic methods should be a single capital letter (this separation extends [Google 5.2.8](https://google.github.io/styleguide/javaguide.html#s5.2.8-type-variable-names))
* In JavaDoc, the paragraph separator "\<p>" is on the (otherwise empty) line between two paragraphs, not immediately before the first word of the next paragraph (this overrides [Google 7.1.2](https://google.github.io/styleguide/javaguide.html#s7.1.2-javadoc-paragraphs))
* local variables "minimize Scope" as detailed in [Google 4.8.2.2](https://google.github.io/styleguide/javaguide.html#s4.8.2-variable-declarations) and not at the top of the scope as in Section 9.1 of the Guidelines

An `.editorconfig` file is provided to configure some IDEs in compliance with the code style.
