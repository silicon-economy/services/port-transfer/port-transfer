/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DatePipe } from '@angular/common';
import { FormControl } from "@angular/forms";
import { MatTableDataSource } from "@angular/material/table";
import { getDateWithTime } from "../date-time-utils";
import { JsonUtils } from "../json-utils";
import { TableFilter } from './table-filter';

/**
 * This class helps on filtering capacities or demands.
 * Usage:
 * - in the template register the global filter form field to globalFilter
 * - in your component create a dataSource (MatTableDataSource) and assign
 *
 * @author C. Schlüter
 */
export class MatTableWithFilters<T> {
  // The date filter is ignored if the year of the given filter date is
  // below that value. This typically happens if an incomplete date is
  // entered in the free text field.
  private readonly ignoreFilterYearsBelow = 2021;
  filteredValues = {};
  globalFilter = '';
  readonly displayedColumns: string[];

  public readonly textFilters: [string, FormControl][];
  public readonly dateFilters: [string, FormControl][];
  public readonly customFilters: TableFilter<any>[];
  private dataSource: MatTableDataSource<T>;
  private datePipe: DatePipe;

  constructor(textFilters: [string, FormControl][], dateFilters: [string, FormControl][], customFilters: TableFilter<any>[], dataSource: MatTableDataSource<T>, datePipe: DatePipe) {

    this.displayedColumns = [];
    this.textFilters = textFilters;
    this.dateFilters = dateFilters;
    this.customFilters = customFilters;
    this.dataSource = dataSource;
    this.datePipe = datePipe;

    textFilters.forEach(value => {
      this.displayedColumns.push(value[0]);
    })
    dateFilters.forEach(value => {
      this.displayedColumns.push(value[0]);
    })
    customFilters.forEach(value => {
      this.displayedColumns.push(value.key);
    })
    this.setFilters();
  }


  /**
   * Apply the given filter to the global filter.
   * @param filter filter string
   */
  public applyFilter(filter): void {
    this.globalFilter = filter;
    this.dataSource.filter = JSON.stringify(this.filteredValues);
  }

  /**
   * Apply a custom filter.
   * @param key the custom filter's key
   * @param filterValue the current value
   */
  public applyCustomFilter(filter: TableFilter<any>): void {
    if (!filter) return;
    this.filteredValues[filter.key] = filter.currentValue;
    this.dataSource.filter = JSON.stringify(this.filteredValues);
  }

  /**
   * Set up the column filters according to their types and set the filter predicate.
   */
  private setFilters(): void {
    this.filteredValues = JsonUtils.addKeys({}, this.displayedColumns)
    this.dataSource.filterPredicate = this.filterPredicate();
    this.textFilters.forEach(value => {
      this.setTextFilter(value[0], value[1]);
    });
    this.dateFilters.forEach(value => {
      this.setDateFilter(value[0], value[1]);
    })
  }

  /**
   * Filter for texts and numbers.
   */
  private setTextFilter(key: string, filter: FormControl): void {
    filter.valueChanges.subscribe((filterValue) => {
      this.filteredValues[key] = filterValue;
      this.dataSource.filter = JSON.stringify(this.filteredValues);
    });
  }

  /**
   * Filter for dates.
   */
  private setDateFilter(key: string, filter: FormControl): void {
    filter.valueChanges.subscribe((filterValue) => {
      let filter_date = '';
      if (filterValue != '') {
        let date = new Date(filterValue);
        filter_date = this.datePipe.transform(date, 'dd.MM.yyyy HH:mm');
      }
      this.filteredValues[key] = filter_date;
      this.dataSource.filter = JSON.stringify(this.filteredValues);
    });
  }

  /**
   * Resets given filters to empty strings.
   * @param forms the FormControls that should be resetted to empty strings
   */
  public resetFilters(): void {
    this.globalFilter = '';
    this.textFilters.forEach(f => {
      f[1].setValue('');
    });
    this.dateFilters.forEach(f => {
      f[1].setValue('');
    });
    this.customFilters.forEach(f => {
      f.reset();
      this.applyCustomFilter(f);
    })
  }

  /**
   * Defines a filter predicate for the mat table to filter the data.
   * @returns the predicate function.
   */
  filterPredicate() {
    return (data: T, filter: string): boolean => {
      if (!this.isGlobalMatch(data)) {
        return undefined;
      }
      let searchString = JSON.parse(filter);
      return this.textFiltersMatch(data, searchString)
        && this.dateFiltersMatch(data, searchString)
        && this.customFiltersMatch(data);
    }
  }


  /**
   * Checks the data against every registered text filter.
   * @param data The data that is filtered
   * @param searchString The filter string
   * @returns true, if all text filters match the data, else false
   */
  private textFiltersMatch(data: T, searchString: any): boolean {
    for (let tf of this.textFilters) {
      let key = tf[0];
      if (!JsonUtils.checkStringMatch(data, key, searchString[key])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks the data against every registered date filter with an Earliest
   * or Latest tag in its key.
   * @param data The data that is filtered
   * @param searchString The filter string
   * @returns true, if all date filters match the data, else false
   */
  private dateFiltersMatch(data: T, searchString: any): boolean {
    for (let df of this.dateFilters) {
      let key = df[0];
      let filterDate: Date = getDateWithTime(searchString[key].toString());
      if (!filterDate || filterDate.getFullYear() < this.ignoreFilterYearsBelow)
        continue; // ignore earlier years.
      if ((key.includes('Earliest') && filterDate > data[key]) ||
        (key.includes('Latest') && filterDate < data[key])) {
        return false;
      }
    }
    return true;
  }

  /**
   * Checks the data against every registered custom filter.
   * @param data The data that is filtered
   * @returns true, if all custom filters match the data, else false
   */
  private customFiltersMatch(data: T): boolean {    
    for (let cf of this.customFilters) {
      // all custom filters must match, else return false
      let filterValue = data[cf.key];
      if (!cf.matches(filterValue))
        return false;
    }
    return true;
  }


  /**
   * Tests if the given data is matching the global filter.
   * @param data The data to match against the global filter
   * @returns true if the match is given, else false
   */
  private isGlobalMatch(data: T): boolean {
    if (this.globalFilter) {
      let globalMatch = false;
      for (let key of this.displayedColumns) {
        if (!globalMatch)
          globalMatch = JsonUtils.checkStringMatch(data, key, this.globalFilter);
      }
      return globalMatch;
    }
    return true;
  }
}
