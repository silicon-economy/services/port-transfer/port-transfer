// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

import java.time.OffsetDateTime;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.siliconeconomy.porttransfer.backend.BackendConfiguration.RFC3339_DATE_TIME_PATTERN;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_EARLIEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_LATEST;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_SINK;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.CAPACITY_SOURCE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.FIXED_ROUTE;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.OWNER_DESIGNATION;
import static org.siliconeconomy.porttransfer.backend.rest.PropertyKeys.VEHICLE_DESIGNATION;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

/**
 * Required information to create a new capacity
 *
 * @author F. König
 */
@Value
public class NewCapacityDto implements DtoWithConnection, DtoWithTimeWindow{

    /**
     * The designation of the company that owns the used vehicle
     */
    @NotNull
    @JsonProperty(OWNER_DESIGNATION)
    String ownerDesignation;

    /**
     * The designation of the used vehicle
     */
    @NotNull
    @JsonProperty(VEHICLE_DESIGNATION)
    String vehicleDesignation;

    /**
     * The origin of the capacity
     */
    @NotNull
    @JsonProperty(CAPACITY_SOURCE)
    String source;

    /**
     * The target of the capacity
     */
    @NotNull
    @JsonProperty(CAPACITY_SINK)
    String sink;

    /**
     * The earliest time the capacity is available
     */
    @NotNull
    @Valid
    @JsonProperty(CAPACITY_EARLIEST)
    @JsonFormat(pattern = RFC3339_DATE_TIME_PATTERN)
    OffsetDateTime earliest;

    /**
     * The latest time the capacity is available
     */
    @NotNull
    @Valid
    @JsonProperty(CAPACITY_LATEST)
    @JsonFormat(pattern = RFC3339_DATE_TIME_PATTERN)
    OffsetDateTime latest;

    /**
     * Whether the capacity travels along a fixed route
     */
    @JsonProperty(FIXED_ROUTE)
    boolean fixed;

}

