/**
* Copyright Open Logistics Foundation
*
* Licensed under the Open Logistics Foundation License 1.3.
* For details on the licensing terms, see the LICENSE file.
* SPDX-License-Identifier: OLFL-1.3
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as e } from 'src/environments/environment';
import { Observable, ReplaySubject } from 'rxjs';
import { SnackbarService } from '../state/snackbar.service';

/**
 * @author Christoph Schlueter
 */

type SessionInvariantData = {
  companyDesignations: string[],
  addressDesignations: string[]
}

/**
 * This service provides session invariant data. This data
 * is requested from the backend only once the first time some 
 * other component or service fetches that from this service.
 * Because it is session invariant, it does not need to be requested 
 * another time. 
 */
@Injectable({
  providedIn: 'root'
})
export class SessionInvariantDataService {

  companyDesignations: ReplaySubject<string[]> = null;
  addressDesignations: ReplaySubject<string[]> = null;

  constructor(public httpClient: HttpClient, public snackService: SnackbarService) { }

  /**
   * Get all available address designations as an observable array.
   */
   public getAddressDesignations(): Observable<string[]> {
    if (this.addressDesignations == null) {
      this.updateData();
    }
    return this.addressDesignations.asObservable();
  }

  /**
   * Get all available company designations as an observable array.
   */
  public getCompanyDesignations(): Observable<string[]> {
    if (this.companyDesignations == null) {
      this.updateData();
    }
    return this.companyDesignations.asObservable();
  }

  private updateData(): void {
    console.log("Update Initial Data");
    this.addressDesignations = new ReplaySubject<string[]>()
    this.companyDesignations = new ReplaySubject<string[]>()

    this.httpClient.get<SessionInvariantData>(e.apiHost + e.api + e.endPoints.sessionInvariantData).subscribe({
      next: (response) => {
        this.companyDesignations.next(response.companyDesignations);
        this.addressDesignations.next(response.addressDesignations);
      },
      error: (error) => {
        this.snackService.handleDefaultErrorsWithText(error.status, "error.missingSessionInvariantData");
      }
    })
  }
}
