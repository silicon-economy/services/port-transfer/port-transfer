{
  "openapi": "3.0.3",
  "info": {
    "title": "PortTransfer backend API",
    "description": "Specification of the REST-API for the PortTransfer backend",
    "version": "1.0.0"
  },
  "servers": [
    {
      "url": "{protocol}://{host}/api/v1",
      "variables": {
        "protocol": {
          "description": "The protocol. Beware that http is insecure",
          "default": "https",
          "enum": [
            "http",
            "https"
          ]
        },
        "host": {
          "description": "The deployment host",
          "default": "localhost"
        }
      }
    }
  ],
  "paths": {
    "/demands": {
      "get": {
        "description": "Request summaries of all demands",
        "responses": {
          "200": {
            "description": "A list of demand summaries",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "dto/DemandSummary.json"
                  }
                }
              }
            }
          }
        }
      },
      "put": {
        "description": "Create a new Demand",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "dto/NewDemand.json"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Successfully created the Demand. The new Demand will be contained in the response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "dto/DemandSummary.json"
                }
              }
            }
          },
          "400": {
            "description": "The Demand could not be created because the request contained errors. The response's 'message'-property contains a ';'-separated list of the erroneous fields' keys",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    },
    "/demands/{id}/match": {
      "get": {
        "description": "Request matching capacities for a specified demand",
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "schema": {
              "type": "integer"
            },
            "required": true,
            "description": "Id of the demand to match"
          }
        ],
        "responses": {
          "200": {
            "description": "A list of matching capacity summaries",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "dto/CapacitySummary.json"
                  }
                }
              }
            }
          },
          "404": {
            "description": "No demand with the specified {id} was found",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    },
    "/capacities": {
      "get": {
        "description": "Request summaries of all capacities",
        "responses": {
          "200": {
            "description": "A list of capacity summaries",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "dto/CapacitySummary.json"
                  }
                }
              }
            }
          }
        }
      },
      "put": {
        "description": "Create a new Capacity",
        "requestBody": {
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "dto/NewCapacity.json"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Successfully created the Capacity. The new capacity will be contained in the response",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "dto/CapacitySummary.json"
                }
              }
            }
          },
          "400": {
            "description": "The Capacity could not be created because the request contained errors. The response's 'message'-property contains a ';'-separated list of the erroneous fields' keys",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Error"
                }
              }
            }
          }
        }
      }
    },
    "/session-invariant-data-sync": {
      "get": {
        "description": "Request session invariant information data.",
        "responses": {
          "200": {
            "description": "An object with session invariant information",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "dto/SessionInvariantData.json"
                }
              }
            }
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "Error": {
        "type": "object",
        "properties": {
          "status": {
            "description": "The http status code number",
            "type": "integer"
          },
          "error": {
            "description": "The http status code name",
            "type": "string"
          },
          "timestamp": {
            "description": "The timestamp of the creation of the error",
            "type": "string",
            "format": "date-time"
          },
          "message": {
            "description": "A message describing the error; defaults to the status code",
            "type": "string"
          },
          "path": {
            "description": "The path that was requested",
            "type": "string"
          },
          "trace": {
            "description": "[optional] The internal stack trace of the error; only available during development",
            "type": "string"
          }
        },
        "required": [
          "status",
          "error",
          "timestamp",
          "message",
          "path"
        ],
        "additionalProperties": false
      }
    }
  }
}
