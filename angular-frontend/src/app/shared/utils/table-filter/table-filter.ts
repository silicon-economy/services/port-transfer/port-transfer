/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * An interface for writing a custom table filter. For an example implementation
 * see {@link EnumTableFilter}. This interface is used to respect custom filters in
 * {@link MatTableWithFilters}.
 */
export interface TableFilter<T> {
    options: { [key: string]: T };
    key: string;
    currentValue: T;

    matches(value: T): boolean;

    isActive(): boolean;

    reset(): void;
}