/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CookieConsentService {

  private readonly storageKey = 'cookieConsent'

  private cookieConsent$: ReplaySubject<boolean> = new ReplaySubject<boolean>();
  private cookieConsent: boolean;

  constructor() {
    this.cookieConsent$.subscribe(consent => {
      this.cookieConsent = consent;
    });
    this.cookieConsent$.next(localStorage.getItem(this.storageKey) === 'true');
  }

  public getCookieConsent(): Observable<boolean> {
    return this.cookieConsent$.asObservable();
  }

  public setCookieConsent(consent: boolean): void {
    this.cookieConsent$.next(consent);
    localStorage.setItem(this.storageKey, consent.toString());
  }
}
