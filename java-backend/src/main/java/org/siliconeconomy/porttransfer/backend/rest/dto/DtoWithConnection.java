// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.dto;

/**
 * A DTO that contains information about a {@link org.siliconeconomy.porttransfer.backend.model.Connection}
 *
 * @author F. König
 */
public interface DtoWithConnection {

    String getSource();

    String getSink();

}
