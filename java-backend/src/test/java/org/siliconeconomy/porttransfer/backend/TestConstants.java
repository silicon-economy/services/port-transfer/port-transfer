// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestConstants {

    /**
     * This concrete instance of OffsetDateTime should be used instead of
     * {@link OffsetDateTime#now()}
     * <p>
     * If a test fails because of the concrete OffsetDateTime, the use of now() would make it almost
     * impossible to recreate the error conditions.
     */
    public static final OffsetDateTime TIME_INSTANCE =
            OffsetDateTime.of(LocalDate.of(2022, 6, 7),
                              LocalTime.of(12, 0),
                              ZoneOffset.UTC);

}
