/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { environment as env } from 'src/environments/environment';
import { DEMANDS } from '../../test/demands';
import { DemandService } from './demand.service';
import { NEW_DEMAND } from '../../test/new-demand';
import { TranslateTestingModule } from 'ngx-translate-testing';

/**
 * @author C. Schlüter, S. Jankowski
 */

describe('DemandService', () => {
  let service: DemandService;
  let httpController: HttpTestingController;
  let baseUrl = env.apiHost + env.api

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DemandService],
      imports: [
        HttpClientTestingModule,
        TranslateTestingModule.withTranslations('de', {})
      ]
    });
    service = TestBed.inject(DemandService);
    httpController = TestBed.inject(HttpTestingController);

    httpController.expectOne(baseUrl + env.endPoints.demands)
      .flush(DEMANDS)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get Demands', fakeAsync(() => {
    service.getDemands().subscribe(r => {
      expect(r).toEqual(DEMANDS);
      expect(r[0].id).toBe(DEMANDS[0].id);
    });
    flush();
  }));

  it('should add demand successful', () => {
    // Arrange
    let newDemand = NEW_DEMAND;
    // Act
    service.putDemand(newDemand);
    // Assert - the demands are requested two times!
    httpController.expectOne(baseUrl + env.endPoints.demands).event(
      new HttpResponse<{}>({ body: newDemand })
    );
    httpController.expectOne(baseUrl + env.endPoints.demands).event(
      new HttpResponse<{}>({ body: newDemand })
    );
  });

  it('should handle error on put demand', () => {
    // Arrange
    let newDemand = NEW_DEMAND;
    // Act
    service.putDemand(newDemand);
    // Assert
    httpController.expectOne(baseUrl + env.endPoints.demands).flush('error', { status: 400, statusText: "error" });
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpController.verify();
  });
});
