/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';

/**
 * This component displays the demand-overview and the capacity-overview and
 * has a horizontal slider to customize the ratio of space for each table.
 */
@Component({
  selector: 'app-demand-capacity-page',
  templateUrl: './demand-capacity-page.component.html',
  styleUrls: ['./demand-capacity-page.component.scss']
})
export class DemandCapacityPageComponent implements OnInit {

  constructor() {
    // not yet implemented
  }

  ngOnInit(): void {
    // not yet implemented
  }

}
