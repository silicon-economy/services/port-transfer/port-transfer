// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

/**
 * This package fills the database with mockup data
 */
package org.siliconeconomy.porttransfer.backend.startup.mockup;
