/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';

import { CapacitySummary } from 'src/app/shared/models/http/backend/capacity-summary';
import { Modality } from 'src/app/shared/models/modality';
import { DemandMatchingService } from 'src/app/shared/services/http/demand-matching.service';
import { EnumTableFilter } from 'src/app/shared/utils/table-filter/enum-table-filter';
import { MatTableWithFilters } from 'src/app/shared/utils/table-filter/mat-table-with-filters';

/**
 * This component represents the table of capacities and its filters and translations.
 *
 * @author C. Schlüter, F. König, S. Jankowski
 */
@Component({
  selector: 'app-capacity-overview',
  templateUrl: './capacity-overview.component.html',
  styleUrls: ['./capacity-overview.component.scss']
})
export class CapacityOverviewComponent implements OnInit, AfterViewInit {

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public readonly displayedColumns: string[] = [
    'Vehicle_Can_45ft',
    'Vehicle_Modality',
    'Vehicle_Weight_Netto_Max',
    'Capacity_Origin_Location_Designation',
    'Capacity_Target_Location_Designation',
    'Capacity_Earliest',
    'Capacity_Latest'];
  public readonly standardColumns: string[] = [
    'Vehicle_Modality',
    'Vehicle_Weight_Netto_Max',
    'Capacity_Origin_Location_Designation',
    'Capacity_Target_Location_Designation'];

  dataSource: MatTableDataSource<CapacitySummary> = new MatTableDataSource();

  isHidden = true;
  vehicleCan45ftFilter = new EnumTableFilter('Vehicle_Can_45ft', {"option.yes": true, "option.no": false});
  vehicleModalityFilter = new EnumTableFilter('Vehicle_Modality', Modality);
  vehicleWeightNettoMaxFilter = new FormControl();
  capacityOriginLocationDesignationFilter = new FormControl();
  capacityTargetLocationDesignationFilter = new FormControl();
  capacityEarliestFilter = new FormControl();
  capacityLatestFilter = new FormControl();
  matTableFilter: MatTableWithFilters<CapacitySummary>;

  private demandMatchingService: DemandMatchingService;
  private datePipe: DatePipe

  constructor(
    demandMatchingService: DemandMatchingService,
    translate: TranslateService,
    datePipe: DatePipe) {
    this.demandMatchingService = demandMatchingService;
    this.datePipe = datePipe;
  }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.setMatTableFilter();

    this.demandMatchingService.getMatches().subscribe(capacities => {
      this.resetFilters();
      this.dataSource.data = capacities;
    });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  private setMatTableFilter() {
    this.matTableFilter = new MatTableWithFilters(
      [
        ["Vehicle_Weight_Netto_Max", this.vehicleWeightNettoMaxFilter],
        ["Capacity_Origin_Location_Designation", this.capacityOriginLocationDesignationFilter],
        ["Capacity_Target_Location_Designation", this.capacityTargetLocationDesignationFilter]
      ],
      [
        ["Capacity_Earliest", this.capacityEarliestFilter],
        ["Capacity_Latest", this.capacityLatestFilter]
      ],
      [this.vehicleCan45ftFilter, this.vehicleModalityFilter],
      this.dataSource,
      this.datePipe);
  }

  resetFilters() {
    this.vehicleCan45ftFilter.currentValue = null;
    this.matTableFilter.resetFilters();
  }
}
