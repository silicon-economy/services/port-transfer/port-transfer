/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Initializer } from "./initializer";
import { BehaviorSubject } from "rxjs";

describe('Initializer', () => {
  let initializer = new class extends Initializer<any> {
    public dates() {
      return super.dates()
    }

    public enums() {
      return super.enums()
    }

    public objects() {
      return super.objects()
    }

    public lists() {
      return super.lists()
    }
  };

  it('should contain empty arrays on default implementation', function () {
    expect(initializer.dates()).toEqual([]);
    expect(initializer.enums()).toEqual([]);
    expect(initializer.objects()).toEqual([]);
    expect(initializer.lists()).toEqual([]);
  });

  it('should map without errors', function () {
    // Arrange
    let testObject = new BehaviorSubject({}).asObservable();
    let testList = new BehaviorSubject([]).asObservable();
    let objectInitializer = initializer.initializeObject();
    let listInitializer = initializer.initializeList();
    // Act
    let initializedObject = objectInitializer(testObject);
    let initializedList = listInitializer(testList);
    // Assert
    expect(initializedObject).toBeTruthy();
    expect(initializedList).toBeTruthy();
  });

})
