// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.model;

/**
 * Length of a container according to ISO 6346 length codes
 * <p>
 * This enum represents a subset of the possible values. A container with a length not included in
 * this enum can not be processed
 *
 * @author F. König
 */
public enum ContainerLength {

    /**
     * 20ft
     */
    ISO_LENGTH_2,
    /**
     * 40ft
     */
    ISO_LENGTH_4,
    /**
     * 45ft
     */
    ISO_LENGTH_L

}
