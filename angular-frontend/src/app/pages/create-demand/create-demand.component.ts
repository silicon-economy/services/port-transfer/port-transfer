/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { map, Observable, startWith } from 'rxjs';
import { ContainerLength } from 'src/app/shared/models/container-length';
import { ContainerType } from 'src/app/shared/models/container-type';
import { DemandService } from 'src/app/shared/services/http/demand.service';
import { SessionInvariantDataService } from 'src/app/shared/services/http/session-invariant-data.service';
import { getISOStringWithOffset } from 'src/app/shared/utils/date-time-utils';

/**
 * This Component handles the adding of a demand.
 * To add a demand the user needs to give valid inputs.
 *
 * @author C. Schlüter
 */
@Component({
  selector: 'app-create-demand',
  templateUrl: './create-demand.component.html',
  styleUrls: ['./create-demand.component.scss']
})
export class CreateDemandComponent implements OnInit {
  public demandForm: FormGroup;
  public showDetails: boolean = false;

  /* Type forwarding for template usage */
  eContainerType = ContainerType;
  eContainerLength = ContainerLength;

  pickup: Date = new Date();

  companyOptions: string[] = [];
  filteredCompanyOptions: Observable<string[]>;

  pickupOptions: string[] = [];
  filteredPickupOptions: Observable<string[]>;

  dropoffOptions: string[] = [];
  filteredDropoffOptions: Observable<string[]>;


  private demandService: DemandService;
  private initService: SessionInvariantDataService;

  constructor(demandService: DemandService, initService: SessionInvariantDataService) {
    this.demandService = demandService;
    this.initService = initService;
  }

  ngOnInit(): void {
    let today = new Date();
    today.setHours(0, 0, 0, 0);

    this.demandForm = new FormGroup({
      dispatcher: new FormControl('', [Validators.required, Validators.maxLength(60)]),
      designation: new FormControl('', [Validators.maxLength(60)]),
      type: new FormControl(null, [Validators.required]),
      size: new FormControl(null, [Validators.required]),
      tara: new FormControl('', [Validators.required, Validators.min(1)]),
      netto: new FormControl(0, [Validators.required, Validators.min(0), Validators.pattern("[0-9]*")]),
      pickup: new FormControl('', [Validators.required]),
      dropoff: new FormControl('', [Validators.required]),
      earliest: new FormControl(today, [Validators.required]),
      latest: new FormControl('', [Validators.required]),
      estimatedTime: new FormControl('', [Validators.required, Validators.min(1), Validators.pattern("[0-9]*")])
    });

    this.demandForm.get('earliest').valueChanges.subscribe((value) => this.pickup = value);

    this.initService.getCompanyDesignations().subscribe((response) => {
      this.companyOptions = response;
      this.filteredCompanyOptions = this._assignFilter('dispatcher', this.companyOptions);
    });
    this.initService.getAddressDesignations().subscribe((response) => {
      this.pickupOptions = this.dropoffOptions = response;
      this.filteredPickupOptions = this._assignFilter('pickup', this.pickupOptions);
      this.filteredDropoffOptions = this._assignFilter('dropoff', this.dropoffOptions);
    });
  }

  private _assignFilter(formControlName: string, options: string[]): Observable<string[]> {
    return this.demandForm.get(formControlName).valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value || '', options))
    );
  }

  private _filter(value: string, options: string[]): string[] {
    const filterValue = value.toLowerCase();

    return options.filter(option => option.toLowerCase().includes(filterValue));
  }

  /**
   * Expects a valid demandFormValue, i.e. that the user only can add a demand on a completely valid FormGroup.
   * @param demandFormValue value of the FormGroup that contains the demand FormControls.
   */
  public putDemand(demandFormValue: any): void {

    let demand = {
      Tmp_Dispatcher: demandFormValue.dispatcher,
      Demand_Designation: this.getDesignation(demandFormValue),
      Demand_EstimatedTime: demandFormValue.estimatedTime,
      LU_Container_Type: demandFormValue.type,
      LU_Container_Size: demandFormValue.size,
      LU_Weight_Tara: demandFormValue.tara,
      LU_Order_Weight_Netto: demandFormValue.netto,
      Order_PickUp_Location_Designation: demandFormValue.pickup,
      Order_DropOff_Location_Designation: demandFormValue.dropoff,
      Order_DropOff_Earliest: getISOStringWithOffset(demandFormValue.earliest),
      Order_DropOff_Latest: getISOStringWithOffset(demandFormValue.latest),
      LU_Order_DangerousGoodsIndication: false,
      LU_Order_ReeferIndication: false
    }

    this.demandService.putDemand(demand);
  }

  private getDesignation(demandFormValue: any): string {
    if (demandFormValue.designation === "") {
      return demandFormValue.pickup + " -> " + demandFormValue.dropoff;
    }
    return demandFormValue.designation;
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.demandForm.controls[controlName].hasError(errorName);
  }
}
