// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.model;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.siliconeconomy.porttransfer.backend.TestConstants;
import org.siliconeconomy.porttransfer.backend.data.DataTestUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ModelTestUtils {

    public static TimeWindow fakeTimeWindow() {
        return new TimeWindow(TestConstants.TIME_INSTANCE,
                              TestConstants.TIME_INSTANCE.plusHours(1));
    }

    public static Connection fakeConnection() {
        return new Connection(DataTestUtils.fakeAddress(),
                              DataTestUtils.fakeAddress());
    }

}
