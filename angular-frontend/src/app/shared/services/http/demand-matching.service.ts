/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment as env } from 'src/environments/environment';
import { CapacitySummary } from '../../models/http/backend/capacity-summary';
import { CapacityInitializer } from '../initialize/capacity-intializer';
import { SnackbarService } from '../state/snackbar.service';

/**
 * This service provides the capacities that match to a given
 * demand.
 *
 * @author C. Schlüter, S. Jankowski
 */
@Injectable({
  providedIn: 'root'
})
export class DemandMatchingService {
  private readonly url: string = env.apiHost + env.api;

  private httpClient: HttpClient;
  private initializer: CapacityInitializer;

  private readonly matchedCapacities: BehaviorSubject<CapacitySummary[]>;

  constructor(httpClient: HttpClient, public snackService: SnackbarService) {
    this.httpClient = httpClient;
    this.initializer = new CapacityInitializer();
    this.matchedCapacities = new BehaviorSubject<CapacitySummary[]>([]);
  }

  /**
   * Triggers the matching algorithm to find all matching capacities
   * for the given demand (via ID). The matching capacities List will be
   * updated.
   * @param id The ID of the demand to match with capacities.
   */
  public triggerMatching(id: number): void {
    this.httpClient.get<CapacitySummary[]>(this.url + env.endPoints.getMatchingCapacities.replace("{demandId}", id.toString()))
      .pipe(this.initializer.initializeList())
      .subscribe({
        next: (response) => { 
          let count = response.length;
          this.matchedCapacities.next(response);
          this.snackService.handleMessageWithTranslateVariables("snackMessage.matchedCapacities", { count }, 6000)
        },
        error: (error) => this.snackService.handleDefaultErrorsWithText("error.matchingFailed", error.error)
      });
  }

  /**
  * Get current matched Capacities as Observable
  *
  * @return an {@link Observable} for the {@link CapacitySummary[]} values
  */
  public getMatches(): Observable<CapacitySummary[]> {
    return this.matchedCapacities.asObservable();
  }
}
