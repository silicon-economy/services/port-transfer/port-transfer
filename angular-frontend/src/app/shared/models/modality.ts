/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * Modalities available for travelling or transporting in a multimodal system
 */
export enum Modality {
    Road = 'ROAD',
    Water = 'WATER',
    Rail = 'RAIL'
}