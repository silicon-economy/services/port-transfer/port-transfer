// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.model;

/**
 * Types of a container according to DIN-SPEC-91073, definition 2.11
 *
 * @author F. König
 */
public enum ContainerType {

    FLAT,
    OPEN_TOP,
    HIGH_CUBE,
    BULK,
    ISO,
    REEFER,
    TANK

}
