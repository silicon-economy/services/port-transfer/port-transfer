// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest;


import java.util.List;
import javax.validation.Valid;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.siliconeconomy.porttransfer.backend.data.master.Address;
import org.siliconeconomy.porttransfer.backend.data.master.Company;
import org.siliconeconomy.porttransfer.backend.data.process.Demand;
import org.siliconeconomy.porttransfer.backend.repository.master.AddressRepository;
import org.siliconeconomy.porttransfer.backend.repository.master.CompanyRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.CapacityRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.DemandRepository;
import org.siliconeconomy.porttransfer.backend.repository.process.LoadingUnitRepository;
import org.siliconeconomy.porttransfer.backend.rest.dto.CapacitySummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.DemandSummaryDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewCapacityDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.NewDemandDto;
import org.siliconeconomy.porttransfer.backend.rest.dto.SessionInvariantDataDto;
import org.siliconeconomy.porttransfer.backend.rest.mapper.CapacityMapper;
import org.siliconeconomy.porttransfer.backend.rest.mapper.DemandMapper;
import org.siliconeconomy.porttransfer.backend.service.DemandMatchingService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * This Controller represents version {@value #VERSION} of the REST-api
 *
 * @author J. Pixberg
 * @author F. König
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(FrontendRestController.VERSION)
class FrontendRestController {

    public static final String VERSION = "v1";

    private final AddressRepository addressRepository;
    private final CapacityRepository capacityRepository;
    private final CompanyRepository companyRepository;
    private final DemandRepository demandRepository;
    private final LoadingUnitRepository loadingUnitRepository;

    private final DemandMatchingService demandMatchingService;

    private final CapacityMapper capacityMapper;
    private final DemandMapper demandMapper;

    /**
     * Provides a list of all demands
     *
     * @return the list of demands
     */
    @GetMapping("/demands")
    public List<DemandSummaryDto> getDemands() {
        return demandRepository.findAll().stream()
                               .map(demandMapper::map)
                               .toList();
    }

    /**
     * Creates a new Demand
     *
     * @param dto the incoming data
     * @return the newly created Demand
     * @throws EntityCreationFailedException if the dto does not represent a valid Demand
     */
    @PutMapping("/demands")
    @ResponseStatus(HttpStatus.CREATED)
    public DemandSummaryDto addDemand(@RequestBody @Valid NewDemandDto dto)
            throws EntityCreationFailedException {
        var freshDemand = demandMapper.map(dto);
        loadingUnitRepository.save(freshDemand.getLoadingUnit());
        var savedDemand = demandRepository.save(freshDemand);
        return demandMapper.map(savedDemand);
    }

    /**
     * Provides a list of capacities matching the given demand
     *
     * @return the list of capacities
     */
    @GetMapping("/demands/{id}/match")
    public List<CapacitySummaryDto> matchDemand(@PathVariable long id) {
        var demand = demandRepository.findById(id)
                                     .orElseThrow(() -> new EntityNotFoundException(Demand.class,
                                                                                    id));
        return demandMatchingService.findMatching(demand).stream()
                                    .map(capacityMapper::map)
                                    .toList();
    }

    /**
     * Provides a list of all capacities
     *
     * @return the list of capacities
     */
    @GetMapping("/capacities")
    public List<CapacitySummaryDto> getCapacities() {
        return capacityRepository.findAll().stream()
                                 .map(capacityMapper::map)
                                 .toList();
    }

    /**
     * Creates a new Capacity
     *
     * @param dto the incoming data
     * @return the newly created Capacity
     * @throws EntityCreationFailedException if the dto does not represent a valid Capacity
     */
    @PutMapping("/capacities")
    @ResponseStatus(HttpStatus.CREATED)
    public CapacitySummaryDto addCapacity(@RequestBody @Valid NewCapacityDto dto)
            throws EntityCreationFailedException {
        var freshCapacity = capacityMapper.map(dto);
        var savedCapacity = capacityRepository.save(freshCapacity);
        return capacityMapper.map(savedCapacity);
    }

    /**
     * Request session invariant information data, e.g. all company and address designations.
     *
     * @return the combined data
     */
    @GetMapping("/session-invariant-data-sync")
    @ResponseStatus(HttpStatus.OK)
    public SessionInvariantDataDto getSessionInvariantData() {
        return new SessionInvariantDataDto(companyRepository.findAll().stream()
                                                            .map(Company::getDesignation)
                                                            .toList(),
                                           addressRepository.findAll().stream()
                                                            .map(Address::getDesignation)
                                                            .toList());
    }

}
