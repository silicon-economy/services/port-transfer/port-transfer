/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  NGX_MAT_DATE_FORMATS,
  NgxMatDateFormats,
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule
} from '@angular-material-components/datetime-picker';
import { CommonModule, DatePipe, registerLocaleData } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import localeDe from '@angular/common/locales/de';
import { APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete'
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorIntl, MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AngularSplitModule } from 'angular-split';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CapacityOverviewComponent } from './components/capacity-overview/capacity-overview.component';
import { DemandOverviewComponent } from './components/demand-overview/demand-overview.component';
import { FocusBlurDirective } from './directives/focus-blur.directive';
import { DemandCapacityPageComponent } from './pages/demand-capacity-page/demand-capacity-page.component';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { ImprintLegalPageComponent } from './pages/imprint-legal-page/imprint-legal-page.component';
import { PrivacyPageComponent } from './pages/privacy-page/privacy-page.component';
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';
import { ScrollRestorationService } from './shared/services/state/scroll-restoration.service';
import { SharedModule } from './shared/shared.module';
import { CreateDemandComponent } from './pages/create-demand/create-demand.component';
import { SnackbarService } from './shared/services/state/snackbar.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { I18nMatPaginatorIntl } from './shared/utils/I18nMatPaginatorIntl';
import { initializeKeycloak } from "./init/keycloak-init.factory";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";


registerLocaleData(localeDe);

export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

const INTL_DATE_INPUT_FORMAT = {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  hourCycle: 'h23',
  hour: '2-digit',
  minute: '2-digit',
};

const MAT_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: INTL_DATE_INPUT_FORMAT,
  },
  display: {
    dateInput: INTL_DATE_INPUT_FORMAT,
    monthYearLabel: { year: 'numeric', month: 'short' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  },
};

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    FocusBlurDirective,
    ImprintLegalPageComponent,
    PrivacyPageComponent,
    SettingsPageComponent,
    DemandCapacityPageComponent,
    DemandOverviewComponent,
    CapacityOverviewComponent,
    CreateDemandComponent
  ],
  imports: [
    MatAutocompleteModule,
    AngularSplitModule,
    AppRoutingModule,
    RouterModule,
    MatGridListModule,
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    SharedModule,
    FlexModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    CommonModule,
    MatButtonModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    FormsModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatChipsModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatCheckboxModule,
    NgxMatDatetimePickerModule,
    NgxMatTimepickerModule,
    NgxMatNativeDateModule,
    HttpClientModule,
    MatSnackBarModule,
    KeycloakAngularModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    }),
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    DatePipe,
    ScrollRestorationService,
    SnackbarService,
    { provide: 'scrollContainerSelector', useValue: '#sidenav-content-area' },
    {
      provide: MatPaginatorIntl,
      useClass: I18nMatPaginatorIntl
    },
    { provide: NGX_MAT_DATE_FORMATS, useValue: MAT_DATE_FORMATS},
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    }
  ]
})
export class AppModule {
}
