/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { CookieConsentService } from './cookie-consent.service';

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  private readonly storageKey = 'footer-state';

  private footerState: boolean;
  private footerState$: ReplaySubject<boolean> = new ReplaySubject<boolean>();

  constructor(private cookieSrv: CookieConsentService) {
    // Only set if consent?
    // cookieSrv.getCookieConsent().pipe(filter((x => x === true))).subscribe(() => {
    this.footerState$.asObservable().subscribe(state => {
      this.footerState = state;
      this.setLocalStorage(state);
    });
    // Initialize
    this.footerState$.next(this.getLocalStorage());
    // });
  }

  public getFooterState$(): Observable<boolean> {
    return this.footerState$.asObservable();
  }

  public toggleFooterState(): void {
    this.footerState$.next(!this.footerState);
  }

  private getLocalStorage(): boolean {
    return localStorage.getItem(this.storageKey) == 'true';
  }

  private setLocalStorage(state: boolean): void {
    localStorage.setItem(this.storageKey, state.toString());
  }
}
