// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.springframework.lang.NonNull;

/**
 * A company has ownership of various entities within the PortTransfer-platform.
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class Company extends BaseEntity {

    /**
     * A unique designation of a company that will be used by human actors to identify it
     */
    @NonNull
    @Column(updatable = false, unique = true, nullable = false)
    private String designation;

    @OneToOne
    @Setter
    private Address address;

    /**
     * An e-mail address that can be used to reach the company
     */
    @Setter
    private String emailAddress;

    /**
     * A phone number that can be used to reach the company
     */
    @Setter
    private String phoneNumber;

    /**
     * An icon that makes it easier for human actors to recognize the company
     */
    @Lob
    @Setter
    private byte[] icon;

}
