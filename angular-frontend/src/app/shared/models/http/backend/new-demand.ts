/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


/**
 * The data structure to create when the user adds a demand and 
 * the demand service puts it to the backend via REST.
 */
export interface NewDemand {
    /**
     * The designation of the company that is the dispatcher of this demand.
     * 
     * Note: Will be removed in future versions. Information will be derived during user authentication instead
     * @deprecated
     */
    Tmp_Dispatcher: string,
    /**
     * An identifier of the demand
     */
    Demand_Designation: string;
    /**
     * The time estimated for the handling of this demand.
     */
    Demand_EstimatedTime: number;
    /**
     * The BIC (or similar) identifier of the loading unit
     */
    LU_Number?: string;
    /**
     * The types are modeled according to DIN-SPEC-91073, definition 2.11
     */
    LU_Container_Type: string;
    /**
     * The length values are modeled according to the ISO 6346 length codes. They represent in order 20ft, 40ft and 45ft
     */
    LU_Container_Size: string;
    /**
     * The tara weight of the loading unit, in kg
     */
    LU_Weight_Tara: number;
    /**
     * The netto weight of the demand, in kg
     */
    LU_Order_Weight_Netto: number;
    /**
     * The designation of the demand's pickup location
     */
    Order_PickUp_Location_Designation: string;
    /**
     * The designation of the demand's drop off location
     */
    Order_DropOff_Location_Designation: string;
    /**
     * The earliest time for the drop off
     */
    Order_DropOff_Earliest: string;
    /**
     * The latest time for the drop off
     */
    Order_DropOff_Latest: string;
    /**
    * Whether the demand contains dangerous goods
    */
    LU_Order_DangerousGoodsIndication: boolean;
    /**
    * Whether this demands needs refrigeration
    * 
    * Note: May be replaced by LU_Order_SetTemperature and a precise temperature in the future
    */
    LU_Order_ReeferIndication: boolean;
}

