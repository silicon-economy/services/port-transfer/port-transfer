/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/**
 * @author C. Schlüter, F. König
 */

const pattern: RegExp = /(?<day>\d{1,2})\.(?<month>\d{1,2})\.(?<year>\d{4}) (?<hour>\d{1,2}):(?<minute>\d{2})(?::\d{2})?/;

/**
 * Get date with time according to pattern "dd.mm.yyyy hh:mm:ss".
 *
 * Seconds are ignored
 *
 * Days, months and hours may also only have one digit
 *
 * @param dateString the string to parse
 * @return the date if dateString matches the pattern, else undefined
 */
export const getDateWithTime = (dateString: string): Date | undefined => {
  let match = dateString.match(pattern);
  if (!match) {
    return undefined;
  }
  let groups: { [p: string]: number } = {};
  for (let key in match.groups) {
    groups[key] = Number.parseInt(match.groups[key]);
  }
  return new Date(groups.year, groups.month - 1, groups.day, groups.hour, groups.minute);
}

export function getISOStringWithOffset(localDate: Date): string {
    let offset = localDate.getTimezoneOffset();
    let offsetString = (offset <= 0 ? "+" : "-") + (offset/60 < 10 ? "0" : "") + (Math.abs(offset)/60) + ":00";
    let isoString = localDate.toISOString().split('.')[0] + offsetString;
    console.log("Get ISO string with offset from date: " + localDate.toISOString() + " with offset: " + offset + "\nFormatted to ISO: " + isoString);
    return isoString;
}
