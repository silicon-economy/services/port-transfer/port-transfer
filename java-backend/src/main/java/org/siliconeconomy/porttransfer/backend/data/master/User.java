// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.master;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.springframework.lang.NonNull;

/**
 * A human that acts according to the interests of a company.
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class User extends BaseEntity {

    /**
     * The company this user can represent
     */
    @NonNull
    @ManyToOne
    @JoinColumn(updatable = false, nullable = false)
    private Company company;

    /**
     * The user's unique username
     */
    @NonNull
    @Column(unique = true, updatable = false, nullable = false)
    private String username;

    /**
     * The user's password
     */
    @NonNull
    @Column(nullable = false)
    @Setter
    private String password;

    /**
     * A profile picture representing the user
     */
    @Lob
    @Setter
    private byte[] profilePic;

}
