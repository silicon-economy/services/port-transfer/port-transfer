// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.security;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * This class configures CORS for both Spring WebMvc and Spring Security
 *
 * @author F. König
 */
@RequiredArgsConstructor
@Component
public class CorsConfigurer implements WebMvcConfigurer {

    private static final String ALL_PATHS_PATTERN = "/**";

    private final SecurityConfiguration securityConfiguration;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping(ALL_PATHS_PATTERN)
                .allowedMethods(HttpMethod.GET.name(), HttpMethod.PUT.name())
                .allowedOriginPatterns(securityConfiguration.getCorsOrigin());
    }

}
