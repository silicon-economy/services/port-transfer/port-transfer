// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.rest.mapper;

import java.time.OffsetDateTime;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.siliconeconomy.porttransfer.backend.TestConstants;
import org.siliconeconomy.porttransfer.backend.rest.builder.TimeWindowBuilder;
import org.siliconeconomy.porttransfer.backend.rest.dto.DtoWithTimeWindow;

interface TimeWindowMapperContract {

    /* Class under test */
    TimeWindowMapper getTimeWindowMapper();

    /* Helper constants */
    OffsetDateTime EARLIEST = TestConstants.TIME_INSTANCE;
    OffsetDateTime LATEST = EARLIEST.plusMinutes(1);

    @Test
    default void mapTimeWindow_successful() {
        // Arrange
        var builder = mock(TimeWindowBuilder.class);
        var dto = new DtoWithTimeWindow() {
            @Override
            public OffsetDateTime getEarliest() {
                return EARLIEST;
            }

            @Override
            public OffsetDateTime getLatest() {
                return LATEST;
            }
        };
        // Act
        getTimeWindowMapper().mapTimeWindow(builder, dto);
        // Assert
        verify(builder).setEarliest(EARLIEST);
        verify(builder).setLatest(LATEST);

        verify(builder, never()).failEarliest();
        verify(builder, never()).failLatest();
    }

    @Test
    default void mapTimeWindow_fail() {
        // Arrange
        var builder = mock(TimeWindowBuilder.class);
        var dto = new DtoWithTimeWindow() {
            @Override
            public OffsetDateTime getEarliest() {
                return LATEST;
            }

            @Override
            public OffsetDateTime getLatest() {
                return EARLIEST;
            }
        };
        // Act
        getTimeWindowMapper().mapTimeWindow(builder, dto);
        // Assert
        verify(builder).failEarliest();
        verify(builder).failLatest();

        verify(builder, never()).setEarliest(EARLIEST);
        verify(builder, never()).setLatest(LATEST);
    }

}
