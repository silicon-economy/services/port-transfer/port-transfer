/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { environment as env } from 'src/environments/environment';
import { CAPACITIES } from '../../test/capacities';
import { SnackbarService } from '../state/snackbar.service';
import { DemandMatchingService } from './demand-matching.service';

describe('DemandMatchingService', () => {
  let service: DemandMatchingService;
  let httpController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DemandMatchingService,
        {
          provide: SnackbarService,
          useValue: {
            handleMessageWithTranslateVariables: () => { }
          }
        },
      ],
      imports: [
        HttpClientTestingModule,
        TranslateTestingModule.withTranslations('de', {})
      ]
    });
    service = TestBed.inject(DemandMatchingService);
    httpController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get Matches', fakeAsync(() => {
    service.triggerMatching(1);
    let api = env.apiHost + env.api + env.endPoints.getMatchingCapacities.replace("{demandId}", "1");
    httpController.expectOne(api)
      .flush(CAPACITIES);
    service.getMatches().subscribe(r => {
      expect(r).toEqual(CAPACITIES);
      expect(r[0].id).toBe(CAPACITIES[0].id);
    });
    flush();
  }));
});
