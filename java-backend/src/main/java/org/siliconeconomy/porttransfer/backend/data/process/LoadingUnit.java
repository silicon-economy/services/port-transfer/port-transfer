// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package org.siliconeconomy.porttransfer.backend.data.process;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.PositiveOrZero;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.siliconeconomy.porttransfer.backend.data.BaseEntity;
import org.siliconeconomy.porttransfer.backend.data.master.Container;
import org.springframework.lang.NonNull;

/**
 * An object that needs to be transported from one place to another.
 * <p>
 * Contains information that may restrict how and by whom it can be transported
 *
 * @author F. König
 */
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor
@Getter
@ToString
@SuppressWarnings("java:S2160") // Entities intentionally do not override equals()
public class LoadingUnit extends BaseEntity {

    /**
     * The Container that needs to be moved
     */
    @NonNull
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Container container;

    /**
     * The weight of the contents of the container
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    @PositiveOrZero
    private int nettoWeight;

    /**
     * Information about the hazard status of the content. An {@link #isEmpty() empty} LoadingUnit
     * should always have {@code false}
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    private boolean hazard;

    /**
     * Informs that the LoadingUnit may need to be cooled during transport. An
     * {@link #isEmpty() empty} LoadingUnit should always have {@code false}
     */
    @NonNull
    @Column(nullable = false, updatable = false)
    private boolean refrigerated;

    /**
     * A LoadingUnit is empty if the content's {@link #getNettoWeight() weight} is 0
     *
     * @return whether the LoadingUnit is empty
     */
    public boolean isEmpty() {
        return nettoWeight == 0;
    }

    /**
     * The BruttoWeight is the sum of the {@link #getNettoWeight() content weight} and the
     * {@link Container#getTaraWeight() Container weight}
     *
     * @return the sum
     */
    public int getBruttoWeight() {
        return container.getTaraWeight() + getNettoWeight();
    }

}
