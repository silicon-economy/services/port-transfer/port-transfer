/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {FooterService} from '../../shared/services/state/footer.service';
import {ThemeService} from '../../shared/services/state/theme.service';
import {BlockchainModeService} from '../../shared/services/state/blockchain-mode.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss'],
  animations: [
    /** Animation that rotates the indicator arrow. */
    trigger('indicatorRotation', [
      state('collapsed, void', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
      transition('expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ]),
    /** Animation that expands and collapses the panel content. */
    trigger('listHeight', [
      state('collapsed, void', style({height: '0px', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition('expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ]),
  ]
})
export class SettingsPageComponent {

  constructor(public footerService: FooterService,
              public themeService: ThemeService,
              public blockchainService: BlockchainModeService) {
  }
}
