/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: 'a, button'
})
export class FocusBlurDirective {
  constructor(private element: ElementRef) {}

  @HostListener('click')
  onClick() {
    this.element.nativeElement.blur();
  }
}
